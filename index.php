<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Troque lá. Site de trocas online. Troque seu celular, smartphone, televisor, troque seus games e consoles. Troque aquele produto que você não quer mais por um produto de seu interesse. Encontre pessoas para realizar sua troca.">
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : 'Trocas online, Troque lá, Celulares, Games, Jogos'  }}" />
    <meta name="robots" content="index, follow">

    <link href="favicon.ico" rel="SHORTCUT ICON" type="image/x-icon">

    <title>Troque Lá - Economize trocando</title>

    <!-- Bootstrap Core CSS -->
    <!--link href="bootstrap/css/custom.css" rel="stylesheet"-->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">


    <!-- Plugin CSS -->
    <link rel="stylesheet" href="bootstrap/css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="bootstrap/css/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand page-scroll center-align" href="#page-top">Troque Lá</a>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Economize trocando</h1>
                <hr>
                <p>No Troque Lá você troca aquele produto que não quer com facilidade.</p>
            </div>
        </div>
    </header>

    <!-- jQuery -->
    <script src="bootstrap/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="bootstrap/js/jquery.easing.min.js"></script>
    <script src="bootstrap/js/jquery.fittext.js"></script>
    <script src="bootstrap/js/wow.min.js"></script>

    <!-- Custom Theme JavSacript -->
    <script src="bootstrap/js/creative.js"></script>


</body>
