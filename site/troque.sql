-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 07/10/2015 às 13:45
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `troque`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_config`
--

CREATE TABLE IF NOT EXISTS `admin_config` (
  `id_admin_config` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `other_configurations` text COLLATE utf8_unicode_ci,
  `imported_database` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_config_module`
--

CREATE TABLE IF NOT EXISTS `admin_config_module` (
  `id_admin_config_module` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_admin_menu` int(10) unsigned NOT NULL,
  `table_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `primary_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `menu_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon_menu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_config_module`),
  KEY `admin_config_module_id_admin_menu_foreign` (`id_admin_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_config_module_field`
--

CREATE TABLE IF NOT EXISTS `admin_config_module_field` (
  `id_admin_config_module_field` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `column` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `field_name_view` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_null` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_list` int(11) DEFAULT NULL,
  `show_form` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `id_admin_type_field` int(10) unsigned NOT NULL,
  `id_admin_config_module` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `relational_field` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_admin_config_module_field`),
  KEY `admin_config_module_field_id_admin_type_field_foreign` (`id_admin_type_field`),
  KEY `admin_config_module_field_id_admin_config_module_foreign` (`id_admin_config_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_menu`
--

CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id_admin_menu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon_menu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_permission`
--

CREATE TABLE IF NOT EXISTS `admin_permission` (
  `id_admin_permission` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_sub_menu_module`
--

CREATE TABLE IF NOT EXISTS `admin_sub_menu_module` (
  `id_admin_sub_menu_module` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_admin_config_module` int(10) unsigned NOT NULL,
  `sub_menu_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon_menu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `father` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_sub_menu_module`),
  KEY `admin_sub_menu_module_id_admin_config_module_foreign` (`id_admin_config_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_type_field`
--

CREATE TABLE IF NOT EXISTS `admin_type_field` (
  `id_admin_type_field` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tag_html` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_css` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_type_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `id_admin_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_admin_user_group` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_user`),
  KEY `admin_user_id_admin_user_group_foreign` (`id_admin_user_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_user_group`
--

CREATE TABLE IF NOT EXISTS `admin_user_group` (
  `id_admin_user_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_admin_user_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_user_group_permission`
--

CREATE TABLE IF NOT EXISTS `admin_user_group_permission` (
  `id_admin_user_group_permission` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_admin_user_group` int(10) unsigned NOT NULL,
  `id_admin_permission` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_admin_user_group_permission`),
  KEY `admin_user_group_permission_id_admin_user_group_foreign` (`id_admin_user_group`),
  KEY `admin_user_group_permission_id_admin_permission_foreign` (`id_admin_permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `categoria`, `ativo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Eletrônicos', 1, 'eletronicos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Games', 1, 'games', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Filmes e Músicas', 1, 'filmes-e-musicas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cidade`
--

CREATE TABLE IF NOT EXISTS `cidade` (
  `id_cidade` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cidade` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id_estado` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cidade`),
  KEY `cidade_id_estado_foreign` (`id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Fazendo dump de dados para tabela `cidade`
--

INSERT INTO `cidade` (`id_cidade`, `cidade`, `id_estado`) VALUES
(1, 'Campo Grande', 1),
(2, 'Rio de Janeiro', 2),
(3, 'São Gabriel do Oeste', 1),
(4, 'Coxim', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id_estado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sigla` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `estado`
--

INSERT INTO `estado` (`id_estado`, `estado`, `sigla`) VALUES
(1, 'MS', 'MS'),
(2, 'RJ', 'RJ');

-- --------------------------------------------------------

--
-- Estrutura para tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id_institucional` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `criado_em` datetime NOT NULL,
  `atualizado_em` datetime NOT NULL,
  PRIMARY KEY (`id_institucional`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `interesse_usuario`
--

CREATE TABLE IF NOT EXISTS `interesse_usuario` (
  `id_interesse_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_sub_categoria` int(10) unsigned NOT NULL,
  `id_usuario` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_interesse_usuario`),
  KEY `interesse_usuario_id_sub_categoria_foreign` (`id_sub_categoria`),
  KEY `interesse_usuario_id_usuario_foreign` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66 ;

--
-- Fazendo dump de dados para tabela `interesse_usuario`
--

INSERT INTO `interesse_usuario` (`id_interesse_usuario`, `id_sub_categoria`, `id_usuario`) VALUES
(63, 1, 2),
(64, 2, 2),
(65, 3, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_09_29_000751_create_institucional_table', 1),
('2015_09_29_001621_create_termo_uso_table', 1),
('2015_09_29_002200_create_social_table', 1),
('2015_09_29_231644_create_estado_table', 1),
('2015_09_29_231839_create_cidade_table', 1),
('2015_09_30_231445_create_usuario_table', 1),
('2015_10_03_065401_create_categoria_table', 2),
('2015_10_03_065803_create_sub_categoria_table', 2),
('2015_10_03_071610_add_sub_categoria_to_sub_categoria_table', 3),
('2015_07_27_174119_create_admin_user_group_table', 4),
('2015_07_28_014425_create_admin_config_table', 4),
('2015_07_29_172438_create_admin_user_table', 4),
('2015_07_29_175323_create_admin_permisson_table', 4),
('2015_07_30_025811_create_admin_group_permission_table', 4),
('2015_07_30_030331_create_admin_user_group_permission_table', 4),
('2015_08_01_023748_create_admin_menu_table', 4),
('2015_08_02_183536_create_admin_config_module_table', 4),
('2015_08_03_174318_create_admin_type_field_table', 4),
('2015_08_03_180633_create_admin_config_module_field_table', 4),
('2015_08_04_034952_create_admin_sub_menu_module_table', 4),
('2015_08_13_012110_alter_admin_config_module_fieldAddRelationalField', 4),
('2015_08_13_012146_alter_admin_config_module_fieldTableAddRelationalField', 4),
('2015_10_06_224435_create_intesse_usuario_table', 5),
('2015_10_07_020219_add_sexo_to_usuario_table', 6),
('2015_10_07_020408_add_data_nascimento_to_usuario_table', 6);

-- --------------------------------------------------------

--
-- Estrutura para tabela `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `id_social` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_social`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `sub_categoria`
--

CREATE TABLE IF NOT EXISTS `sub_categoria` (
  `id_sub_categoria` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_categoria` int(10) unsigned NOT NULL,
  `ativo` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sub_categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_sub_categoria`),
  KEY `sub_categoria_id_categoria_foreign` (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `sub_categoria`
--

INSERT INTO `sub_categoria` (`id_sub_categoria`, `id_categoria`, `ativo`, `slug`, `created_at`, `updated_at`, `sub_categoria`) VALUES
(1, 1, 1, 'notebooks', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Notebooks'),
(2, 2, 1, 'play-station', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Play Station'),
(3, 2, 1, 'x-box-360', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'X-Box 360');

-- --------------------------------------------------------

--
-- Estrutura para tabela `termo_uso`
--

CREATE TABLE IF NOT EXISTS `termo_uso` (
  `id_termo_uso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `termo` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `criado_em` datetime NOT NULL,
  `atualizado_em` datetime NOT NULL,
  PRIMARY KEY (`id_termo_uso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_cidade` int(10) unsigned DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `confirmou_email` int(11) DEFAULT NULL,
  `primeiro_acesso` int(11) DEFAULT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_facebook` int(11) DEFAULT NULL,
  `cep` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento` text COLLATE utf8_unicode_ci,
  `criado_em` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `atualizado_em` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sexo` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `usuario_id_cidade_foreign` (`id_cidade`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `nome`, `sobrenome`, `id_cidade`, `email`, `confirmou_email`, `primeiro_acesso`, `senha`, `id_facebook`, `cep`, `endereco`, `numero`, `bairro`, `complemento`, `criado_em`, `atualizado_em`, `sexo`, `data_nascimento`) VALUES
(1, NULL, 'Rafael', 'Franke', 2, 'rafa.franke14@hotmail.com', NULL, 0, '$2y$10$kqtjv7x9GiOrQENsn5.6t.B50u3rYAH48BH1rPZhR4WmGPMGh5Cyy', NULL, '20081-902', 'Rua Sacadura Cabral', 1111, 'Saúde', 'Apartamento', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00'),
(2, NULL, 'Rafael', 'Franke', 1, 'rafaelfrankean@gmail.com', NULL, 0, '$2y$10$WCLLrF/Dg6NRj9A0sdzPL.QPwvDAzHbuscwgTB83jK5cWqgtN5Xty', NULL, '79017-021', 'Rua Santo Inácio de Loiola', 641, 'Nova Lima', 'Residência', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Masculino', '0000-00-00');

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `admin_config_module`
--
ALTER TABLE `admin_config_module`
  ADD CONSTRAINT `admin_config_module_id_admin_menu_foreign` FOREIGN KEY (`id_admin_menu`) REFERENCES `admin_menu` (`id_admin_menu`);

--
-- Restrições para tabelas `admin_config_module_field`
--
ALTER TABLE `admin_config_module_field`
  ADD CONSTRAINT `admin_config_module_field_id_admin_config_module_foreign` FOREIGN KEY (`id_admin_config_module`) REFERENCES `admin_config_module` (`id_admin_config_module`),
  ADD CONSTRAINT `admin_config_module_field_id_admin_type_field_foreign` FOREIGN KEY (`id_admin_type_field`) REFERENCES `admin_type_field` (`id_admin_type_field`);

--
-- Restrições para tabelas `admin_sub_menu_module`
--
ALTER TABLE `admin_sub_menu_module`
  ADD CONSTRAINT `admin_sub_menu_module_id_admin_config_module_foreign` FOREIGN KEY (`id_admin_config_module`) REFERENCES `admin_config_module` (`id_admin_config_module`);

--
-- Restrições para tabelas `admin_user`
--
ALTER TABLE `admin_user`
  ADD CONSTRAINT `admin_user_id_admin_user_group_foreign` FOREIGN KEY (`id_admin_user_group`) REFERENCES `admin_user_group` (`id_admin_user_group`);

--
-- Restrições para tabelas `admin_user_group_permission`
--
ALTER TABLE `admin_user_group_permission`
  ADD CONSTRAINT `admin_user_group_permission_id_admin_permission_foreign` FOREIGN KEY (`id_admin_permission`) REFERENCES `admin_permission` (`id_admin_permission`),
  ADD CONSTRAINT `admin_user_group_permission_id_admin_user_group_foreign` FOREIGN KEY (`id_admin_user_group`) REFERENCES `admin_user_group` (`id_admin_user_group`);

--
-- Restrições para tabelas `cidade`
--
ALTER TABLE `cidade`
  ADD CONSTRAINT `cidade_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`);

--
-- Restrições para tabelas `interesse_usuario`
--
ALTER TABLE `interesse_usuario`
  ADD CONSTRAINT `interesse_usuario_id_sub_categoria_foreign` FOREIGN KEY (`id_sub_categoria`) REFERENCES `sub_categoria` (`id_sub_categoria`),
  ADD CONSTRAINT `interesse_usuario_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Restrições para tabelas `sub_categoria`
--
ALTER TABLE `sub_categoria`
  ADD CONSTRAINT `sub_categoria_id_categoria_foreign` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Restrições para tabelas `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_id_cidade_foreign` FOREIGN KEY (`id_cidade`) REFERENCES `cidade` (`id_cidade`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
