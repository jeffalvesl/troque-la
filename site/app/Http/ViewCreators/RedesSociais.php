<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use App\Repositories\Institucional\SocialRepository;

class RedesSociais
{
    public function __construct(SocialRepository $socialRepo)
    {
        $this->socialRepo = $socialRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $social = $this->socialRepo->makeModel()->whereIn('slug', ['facebook', 'twitter', 'instagram', 'google'])->get(   );
        $rede = $social->lists('url', 'slug');

        $view->with(compact('rede'));

        return $view;
    }
}