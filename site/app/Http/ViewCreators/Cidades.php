<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use App\Repositories\Localidade\CidadeRepository;

class Cidades
{
    public function __construct(CidadeRepository $cidadeRepo)
    {
        $this->cidadeRepo = $cidadeRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $cidades = $this->cidadeRepo->with(['estado'])->makeModel()->orderBy('cidade', 'ASC')->get();

        $view->with(compact('cidades'));

        return $view;
    }
}