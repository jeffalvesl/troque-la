<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use Auth;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;

class MenuUsuarioLogado
{
    public function __construct(NotificacaoUsuarioRepository $notificacaoUsuarioRepo)
    {
        $this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $notificacoes = $this->notificacaoUsuarioRepo->retornaNotificacao(Auth::user()->id_usuario, 3, false, false);

        $view->with(compact('notificacoes'));

        return $view;
    }
}