<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use App\Repositories\Produto\CategoriaRepository;

class Categorias
{
    public function __construct(CategoriaRepository $categoriaRepo)
    {
        $this->categoriaRepo = $categoriaRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $categorias = $this->categoriaRepo->with(['subCategorias.produtos'])
                    ->makeModel()
                    ->where('ativo', 1)
                    ->get();

        $view->with(compact('categorias'));

        return $view;
    }
}