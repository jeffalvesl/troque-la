<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use App\Repositories\Produto\ProdutoRepository;

class Novidades
{
    public function __construct(ProdutoRepository $produtoRepo)
    {
        $this->produtoRepo = $produtoRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $novidades = $this->produtoRepo->with(['imagens'])
                ->makeModel()
                ->orderBy('criado_em', 'DESC')
                ->where('publicado', 1)
                ->where('trocado', 0)
                ->take(5)
                ->get();

        $view->with(compact('novidades'));

        return $view;
    }
}