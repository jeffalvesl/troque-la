<?php namespace App\Http\ViewCreators;

use Illuminate\Contracts\View\View;
use App\Repositories\Produto\ProdutoRepository;

class Destaque
{
    public function __construct(ProdutoRepository $produtoRepo)
    {
        $this->produtoRepo = $produtoRepo;
    }

    /**
     * MÃ©todo executado quando a view estiver sendo criada.
     *
     * @param View $view
     *
     * @return View
     */
    public function create(View $view)
    {
        $destaques = $this->produtoRepo->with(['imagens'])->makeModel()->where('trocado', 0)->orderBy('views', 'desc')->take(6)->get();

        $view->with(compact('destaques'));

        return $view;
    }
}