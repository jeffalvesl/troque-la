<?php namespace App\Http\Controllers\Usuario;

use Input;
use Request;
use Auth;
use Hash;
use DB;
use App\Http\Controllers\BaseController;
use App\Services\Email\EmailService;
use App\Repositories\Usuario\RecuperarSenhaRepository;
use App\Repositories\Usuario\UsuarioRepository;


class RecuperarSenhaController extends BaseController
{

	public function __construct(EmailService $emailService, RecuperarSenhaRepository $recuperarSenhaRepo, UsuarioRepository $usuarioRepo)
	{
		$this->emailService = $emailService;
		$this->usuarioRepo = $usuarioRepo;
		$this->recuperarSenhaRepo = $recuperarSenhaRepo;
	}

	public function esqueci()
	{
		return view('login.esqueci');
	}


	public function postEsqueci()
	{
		if(!Request::ajax())
			abort(500);

		try {
			
			DB::beginTransaction();

			$usuario = $this->usuarioRepo->retornaUsuarioPorCampo('email', Input::get('email'), ['email', 'id_usuario']);
			$hash = Hash::make($usuario->email);

			$hashExiste = $this->recuperarSenhaRepo->findByField('id_usuario', $usuario->id_usuario)->first();

			if($hashExiste)
			{
				if($hashExiste->valida)
				{
					$hash = $hashExiste->hash;
				}
				else
				{
					$dadosHash = ['hash' => $hash, 'id_usuario' => $usuario->id_usuario];
					$this->recuperarSenhaRepo->create($dadosHash);
				}
			}
			else
			{
				$dadosHash = ['hash' => $hash, 'id_usuario' => $usuario->id_usuario];
				$this->recuperarSenhaRepo->create($dadosHash);
			}

			$this->emailService->send(['email' => Input::get('email'), 'assunto' => 'Recuperar senha', 'url' => route('definir.senha', $hash)], 'emails.esqueci_senha');

			DB::commit();
			return response()->json(['mensagem' => 'Enviamos as instruções de recuperação de senha para seu email.']);

		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['mensagem' => 'Não foi possível executar a ação. Atualize a página e tente novamente.']);
		}
	}


	public function definirNovaSenha($hash)
	{
		try {
			$hash = $this->recuperarSenhaRepo->findByField('hash', $hash)->first();
			if(!$hash->valida)
				abort(404);

			$usuario = $this->usuarioRepo->find($hash->id_usuario);

			return view('login.recuperar_senha', compact('hash', 'usuario'));

		} catch (Exception $e) {
			abort(404);
		}
	}

	public function postDefinirNovaSenha()
	{
		try {

			$hash = $this->recuperarSenhaRepo->findByField('hash', Input::get('hash'))->first();
			$usuario = $this->usuarioRepo->retornaUsuarioPorCampo('email', Input::get('email'), ['email', 'id_usuario']);

			if($hash->id_usuario != $usuario->id_usuario)
				abort(404);

			DB::beginTransaction();
			$this->recuperarSenhaRepo->update(['valida' => 0], $hash->id_recuperar_senha_hash);
			
			$novaSenha = Hash::make(Input::get('senha'));
			$dados = ['senha' => $novaSenha];
			$this->usuarioRepo->atualizarDados($dados, $usuario->id_usuario);

			DB::commit();

			return redirect()->route('login.login');

		} catch (Exception $e) {
			DB::rollback();
			abort(404);
		}
	}

}
