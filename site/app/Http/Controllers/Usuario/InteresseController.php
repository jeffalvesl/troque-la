<?php namespace App\Http\Controllers\Usuario;

use Input;
use Auth;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\InteresseUsuarioRepository;

class InteresseController extends BaseController
{

	public function __construct(InteresseUsuarioRepository $interesseUsuarioRepo)
	{
		$this->middleware('auth');
		$this->interesseUsuarioRepo = $interesseUsuarioRepo;
	}
	
	/**
	 * Retorna interesses de um usuário.
	 */
	public function retornaInteresses()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {

			$interesses = $this->interesseUsuarioRepo->retornaInteressesUsuario($termo);
			return response()->json(['interesses' => $interesses]);
		} catch (Exception $e) {
			
		}
	}

	/**
	 * Pesquisa interesses através de um termo.
	 */
	public function pesquisaInteresse()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {
			$interesses = $this->interesseUsuarioRepo->pesquisarInteresses(Input::get('termo'), Auth::user()->id_usuario);
			return response()->json(['interesses' => $interesses]);
		} catch (Exception $e) {
			
		}
	}
}
