<?php namespace App\Http\Controllers\Usuario;

use Input;
use Auth;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Repositories\Usuario\InteresseUsuarioRepository;
use App\Repositories\Produto\ProdutoRepository;
use App\Services\Produto\Produto;
use View;

class UsuarioController extends BaseController
{

	public $title = 'Minhas Informações';

	public function __construct(
		UsuarioRepository $usuarioRepo,
		InteresseUsuarioRepository $interesseUsuarioRepo,
		ProdutoRepository $produtoRepo,
		Produto $produtoServ
	)
	{
		$this->middleware('auth');
		$this->usuarioRepo = $usuarioRepo;
		$this->interesseUsuarioRepo = $interesseUsuarioRepo;
		$this->produtoRepo = $produtoRepo;
		$this->produtoServ = $produtoServ;
		parent::__construct();
	}

	/**
	 * View index de usuário.
	 * @return View.
	 */
	public function index()
	{
		$usuario = $this->usuarioRepo->getUsuario(Auth::user()->id_usuario, true, ['cidade', 'interesses.subCategoria.categoria']);

		$trocas = $this->produtoServ->trocas(['usuario', 'troca.proposta.usuario', 'propostas']);

		$trocasId = $trocas->lists('id_produto');

		$propostasAceitas = $this->produtoServ->trocas(['propostas', 'usuario'], true, 12, $trocasId);

		$produtos = $this->produtoRepo->retornaProdutos(Auth::user()->id_usuario);

		$interesses = $usuario->interesses;
		View::share('title', $this->title);

		return view('usuario.index', compact('usuario', 'interesses', 'trocas', 'produtos', 'propostasAceitas'));
	}

	/**
	 * Ativa uma conta do usuário.
	 * @return Bool true - false.
	 */
	public function ativarConta()
	{
		try {
			$this->usuarioRepo->atualizarDados(['ativo' => 1], Auth::user()->id_usuario);

			return redirect()->back();
		} catch (Exception $e) {
			Auth::logout();
		}
	}

	/**
	 * Cancela uma conta do usuário.
	 * @return view.
	 */
	public function cancelarConta()
	{
		return view('usuario.cancelar');
	}

	/**
	 * Post cancelar conta.
	 * @return Bool true - false.
	 */
	public function postCancelarConta()
	{
		$dados['ativo'] = 0;
		$dados['motivo_cancelamento'] = Input::get('motivo_cancelamento');

		try {

			$this->usuarioRepo->atualizarDados($dados, Auth::user()->id_usuario);
			return redirect()->route('logout.get');

		} catch (Exception $e) {
			return redirect()->route('logout.get');
		}
	}

	/**
	 * Atualiza dados de um usuário
	 * @return response json com true|false
	 */
	public function postAtualizaDados()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {
			$dados = Input::all();

			$atualizou = $this->usuarioRepo->atualizarDados($dados, Auth::user()->id_usuario);

			if($atualizou['atualizou'])
			{
				$redirecionar = is_null($atualizou['primeiro_acesso']) || $atualizou['primeiro_acesso'] == 1 ? route('produto_usuario.novo') : false; 

				if(is_null($atualizou['primeiro_acesso']) || $atualizou['primeiro_acesso'] == 1)
				{
					$this->usuarioRepo->atualizarDados(['primeiro_acesso' => 0], Auth::user()->id_usuario);
				}

				return response()->json(['atualizou' => true, 'classe' => 'sucesso', 'redirecionar' => $redirecionar, 'mensagem' => 'Dados atualizados com sucesso!']);				
			}
			else
			{
				return response()->json(['atualizou' => false, 'classe' => 'falha', 'mensagem' => 'Desculpe, não foi possível executar ação']);	
			}

		} catch (Exception $e) {
			return response()->json(['atualizou' => false, 'classe' => 'falha', 'mensagem' => 'Desculpe, não foi possível executar ação']);
		}
	}
}
