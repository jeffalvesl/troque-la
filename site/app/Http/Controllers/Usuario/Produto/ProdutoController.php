<?php namespace App\Http\Controllers\Usuario\Produto;

use Input;
use Auth;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Repositories\Usuario\InteresseUsuarioRepository;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Produto\CategoriaRepository;
use App\Repositories\Produto\ImagemProdutoRepository;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;
use App\Services\Arquivo\Arquivo;
use App\Services\Produto\Produto;
use View;

class ProdutoController extends BaseController
{

	public function __construct(
		UsuarioRepository $usuarioRepo,
		ProdutoRepository $produtoRepo,
		Produto $produtoServ,
		CategoriaRepository $categoriaRepo,
		ImagemProdutoRepository $imagemProdutoRepo,
		InteresseUsuarioRepository $interesseUsuarioRepo,
		NotificacaoUsuarioRepository $notificacaoUsuarioRepo
		)
	{
		$this->middleware('auth');
		$this->produtoRepo = $produtoRepo;
		$this->produtoServ = $produtoServ;
		$this->categoriaRepo = $categoriaRepo;
		$this->imagemProdutoRepo = $imagemProdutoRepo;
		$this->interesseUsuarioRepo = $interesseUsuarioRepo;
		$this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
		parent::__construct();
	}

	/**
	 * View index da listagem de produtos.
	 * @return View.
	 */
	public function index()
	{
		$produtos = $this->produtoRepo->retornaProdutos(Auth::user()->id_usuario);

		return view('usuario.produto.index', compact('produtos'));
	}

	/**
	 * Produtos trocados.
	 * @return view.
	 */
	public function trocados()
	{
		$trocas = $this->produtoServ->trocas(['usuario', 'troca.proposta.usuario']);
		$propostasAceitas = $this->produtoServ->trocas(['usuario', 'troca.proposta.usuario'], true, 12, $trocas->lists('id_produto'));

		$this->title = 'Trocas Realizadas';
		View::share('title', $this->title);

		return view('usuario.produto.trocas', compact('trocas', 'propostasAceitas'));
	}

	/**
	 * View para inserir novo produto.
	 */
	public function novoProduto()
	{
		$this->title = 'Inserir novo produto';
		View::share('title', $this->title);

		//Pega possíveis interesses do usuário logado.
		$usuarioInteresses = $this->interesseUsuarioRepo->retornaInteressesUsuario(Auth::user()->id_usuario);
		$possiveisInteresses = $this->produtoRepo->buscaProdutosPorInteresse($usuarioInteresses->lists('id_sub_categoria'));

		$categorias = $this->categoriaRepo->retornaCategorias(['subCategorias']);

		return view('usuario.produto.novo_produto', compact('categorias', 'possiveisInteresses'));
	}

	/**
	 * Função salva os dados de um novo produto ou da edição de um produto existente.
	 * @return response -> array com id do produto e também para qual página iremos depois dessa ação.
	 */
	public function salvar()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$dados = Input::all();
		$dados['slug'] = $this->createSlug($dados['produto']);

		//Verifica se existe um produto com esta slug. Se existir nós vamos criar uma nova slug com o mesmo nome acrescentando um número no final.
		// Ex: teste-produto-2
		$produtosSlug = $this->produtoRepo->findByField('slug', $dados['slug'])->all();
		if(count($produtosSlug) > 0)
		{
			$slugNUm = count($produtosSlug) + 1;
			$dados['slug'] = $dados['slug'] . '-' . $slugNUm;
		}

		try {
			//Se não existir um produto nós vamos cria-lo.
			if(!isset($dados['id_produto']))
			{
				$dados['aceita_proposta'] = 1;
				$dados['id_usuario'] = Auth::user()->id_usuario;
				$produto = $this->produtoRepo->criarNovoProduto($dados);
			}
			else
			{
				//Se existir produto nós apenas vamos editar.
				$idProduto = Input::get('id_produto');
				$dados['publicado'] = isset($dados['publicado']) ? 1 : 0;

				$produto = $this->produtoRepo->atualizar($dados, $idProduto);
			}

			return response()->json(['id_produto' => $produto->id_produto, 'url' => route('editar.produto', $dados['slug'])]);

		} catch (Exception $e) {
			return response()->json(['criou' => false, 'classe' => 'falha', 'mensagem' => 'Desculpe, não foi possível realizar a ação.']);
		}
	}

	/**
	 * Edita um produto do usuário.
	 * @param string $slug slug do produto.
	 * @return View
	 */
	public function editarProduto($slug)
	{
		$produto = $this->produtoRepo->retornaProduto(
			[
				'slug' => $slug
			],
			[
				'troca',
				'propostas',
				'subCategoria.categoria',
				'interesses'
			]
		);

		if(!$produto)
			abort(404);

		$disabled = '';
		if($produto->trocado)
		{
			$disabled = 'disabled';
		}

		$categorias = $this->categoriaRepo->retornaCategorias(['subCategorias']);

		$this->title = 'Editar - ' . $produto->produto;
		View::share('title', $this->title);

		return view('usuario.produto.editar_produto', compact('produto', 'categorias', 'disabled'));
	}


	/**
	 * Retorna view para adicionar imagens do produto.
	 * @param  string $slug produto.
	 * @return View.
	 */
	public function imagemProduto($slug)
	{
		$produto = $this->produtoRepo->retornaProduto(['slug' => $slug], ['usuario', 'imagens']);

		$this->title = 'Imagens - ' . $produto->produto;
		View::share('title', $this->title);

		return view('usuario.produto.imagens_produto', compact('produto'));
	}

	/**
	 * Exclui uma imagem do produto.
	 * @param int $idImagemProduto -> id da imagem para excluir.
	 */
	public function excluirImagemProduto($idImagemProduto)
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$imagem = $this->imagemProdutoRepo->findByField('id_imagem_produto', $idImagemProduto)->first();
		$idProduto = $imagem->id_produto;
		$excluiu = Arquivo::delete($imagem->imagem);

		if($excluiu)
		{
			$this->imagemProdutoRepo->delete($idImagemProduto);

			//verifica se não possui imagens o suficiente, se não tiver altera situação do produto para 0(não publicado)
			$imagens = $this->imagemProdutoRepo->findByField('id_produto', $idProduto)->all();
			if(count($imagens) < 4)
			{
				$this->produtoRepo->atualizar(['publicado' => 0], $idProduto);
			}

			return response()->json(['excluiu' => true]);
		}

		return response()->json(['excluiu' => false]);
	}

	/**
	 * Busca produtos de um usuário através de um termo digitado pelo usuário.
	 */
	public function buscarProdutosUsuario()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$termo = Input::get('termo');
		try {
			$produtos = $this->produtoRepo->buscarProdutosUsuario(Auth::user()->id_usuario, $termo);

			return response()->json(['produtos' => $produtos]);
		} catch (Exception $e) {

		}
	}

	/**
	 * Publica um produto.
	 */
	public function postPublicarProduto()
	{
		$idProduto = Input::get('id_produto');

		try {
			$produto = $this->produtoRepo->atualizar(['publicado' => 1], $idProduto);

			return redirect()->route('produtos.detalhe', $produto->slug);
		} catch (Exception $e) {
			return redirect()->back();
		}
	}

	/**
	 * Faz o upload de arquivo neste controller
	 */
	public function upload()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {
			$produto = $this->produtoRepo->retornaProduto(['id_produto' => Input::get('id_produto')]);
			$nomeArquivo = $produto->slug;

			$imagens_produto = [Input::file('arquivo')];

			foreach($imagens_produto as $key => $imagem)
			{
				$uploaded = Arquivo::upload($imagem, 'produtos/'.$produto->id_usuario, $nomeArquivo);
				if($uploaded)
				{
					$criouRegistro = $this->imagemProdutoRepo->criar(
						[
							'id_produto' => $produto->id_produto,
							'imagem' => $uploaded['arquivo'],
							'legenda' => $produto->produto,
							'miniatura' => $uploaded['miniatura']
						]
					);

					$imagensProduto = $this->imagemProdutoRepo->findByField('id_produto', $produto->id_produto)->all();

				}
			}

			return response()->json(['arquivo_salvou' => true, 'arquivo' => asset($uploaded['arquivo']), 'total' => count($imagensProduto)]);

		} catch (Exception $e) {
			return response()->json(['retorno' => false]);
		}
	}
}
