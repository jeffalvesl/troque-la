<?php namespace App\Http\Controllers\Usuario\Produto;

use Auth;
use View;
use Input;
use DB;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Produto\PropostaProdutoRepository;
use App\Repositories\Produto\ProdutoPropostaProdutoRepository;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;
use App\Repositories\Produto\ProdutoTrocaRepository;

class PropostaController extends BaseController
{

	public function __construct(
		ProdutoRepository $produtoRepo,
		PropostaProdutoRepository $propostaProdutoRepo,
		ProdutoPropostaProdutoRepository $produtoPropostaProdutoRepo,
		NotificacaoUsuarioRepository $notificacaoUsuarioRepo,
		ProdutoTrocaRepository $produtoTrocaRepo
		)
	{
		$this->middleware('auth');
		$this->produtoRepo = $produtoRepo;
		$this->propostaProdutoRepo = $propostaProdutoRepo;
		$this->produtoPropostaProdutoRepo = $produtoPropostaProdutoRepo;
		$this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
		$this->produtoTrocaRepo = $produtoTrocaRepo;
		parent::__construct();
	}


	public function index($slug)
	{
		$produto = $this->produtoRepo->retornaProduto(
			['slug' => $slug],
			['usuario', 'proposta.produtoProposta', 'imagens']
		);


		return view('propostas.index', compact('produto'));
	}

	public function propostaProduto($slug)
	{
		$produto = $this->produtoRepo->retornaProduto(
			['slug' => $slug],
			['usuario', 'proposta.produtoProposta', 'imagens']
		);


		return view('propostas.produto', compact('produto'));
	}

	/**
	 * Aceita uma proposta em produto e envia uma notificação para o usuário.
	 * Além disso também cria uma proposta com situação de aguardando confirmação de troca.
	 */
	public function postAceitarProposta()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {
			DB::beginTransaction();

			$dados = ['aceita_proposta' => 0];
			$produto = $this->produtoRepo->atualizar($dados, Input::get('id_produto'));

			$idPropostaProduto = Input::get('id_proposta_produto');
			$proposta = $this->propostaProdutoRepo->atualizar(['aceita' => 1], $idPropostaProduto);

			$this->notificacaoUsuarioRepo->criarNovaNotificacao(
				[
					'url_conteudo' => route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]),
					'id_usuario' => $proposta->id_usuario,
				],
				Auth::user()->nome . ' ' . Auth::user()->sobrenome,
				'aceitou_proposta'
			);

			$this->produtoTrocaRepo->novaTroca(
				[
					'id_produto' => $produto->id_produto,
					'id_proposta_produto' => $proposta->id_proposta_produto
				]
			);

			DB::commit();

			$enviou = true;

			return response()->json(['aceita' => true]);

		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['aceita' => false]);
		}
	}

}
