<?php namespace App\Http\Controllers\Usuario\Produto;

use Auth;
use View;
use Input;
use DB;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Produto\PropostaProdutoRepository;
use App\Repositories\Produto\ProdutoPropostaProdutoRepository;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;
use App\Repositories\Produto\ProdutoTrocaRepository;

class TrocaController extends BaseController
{

	public function __construct(
		ProdutoRepository $produtoRepo,
		PropostaProdutoRepository $propostaProdutoRepo,
		ProdutoPropostaProdutoRepository $produtoPropostaProdutoRepo,
		NotificacaoUsuarioRepository $notificacaoUsuarioRepo,
		ProdutoTrocaRepository $produtoTrocaRepo
		)
	{
		$this->middleware('auth');
		$this->produtoRepo = $produtoRepo;
		$this->propostaProdutoRepo = $propostaProdutoRepo;
		$this->produtoPropostaProdutoRepo = $produtoPropostaProdutoRepo;
		$this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
		$this->produtoTrocaRepo = $produtoTrocaRepo;
		parent::__construct();
	}


	/**
	 * Confirma a troca de um produto.
	 */
	public function postConfirmarTroca()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {

			DB::beginTransaction();

			$troca = $this->produtoTrocaRepo->atualizar([], Input::get('id_produto_troca'), 'realizada');

			$produtoProposta = $this->produtoPropostaProdutoRepo->with(['produto'])->findByField('id_proposta_produto', $troca->id_proposta_produto)->first();
			$this->produtoRepo->atualizar(['aceita_proposta' => 0, 'trocado' => 1], $produtoProposta->produto->id_produto);
			$this->produtoRepo->atualizar(['aceita_proposta' => 0, 'trocado' => 1], $produtoProposta->proposta->id_produto);

			$this->notificacaoUsuarioRepo->criarNovaNotificacao(
				[
					'url_conteudo' => route('mensagens_produto.index', [$produtoProposta->proposta->produto->slug, $produtoProposta->id_proposta_produto]),
					'id_usuario' => $produtoProposta->proposta->produto->id_usuario,
					'visualizada' => 0
				],
				Auth::user()->nome . ' ' . Auth::user()->sobrenome,
				'confirmar_troca'
			);

			DB::commit();

			return response()->json(['confirmou' => true]);
		} catch (Exception $e) {

			DB::rollback();
			return response()->json(['confirmou' => false]);
		}
	}

	/**
	 * Realiza a troca de um produto.
	 */
	public function postTrocaRealizada()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$dadosTroca = Input::all();

		try {
			DB::beginTransaction();
			$troca = $this->produtoTrocaRepo->atualizar([], Input::get('id_produto_troca'), 'realizada');
			
			$this->produtoRepo->atualizar([
				'trocado' => 1,
				'aceita_proposta' => 0
			], $troca->id_produto);

			$this->produtoRepo->atualizar([
				'trocado' => 1,
				'aceita_proposta' => 0
			], $troca->produto->id_produto);

			$this->notificacaoUsuarioRepo->criarNovaNotificacao(
				[
					'url_conteudo' => route('mensagens_produto.index', [$troca->proposta->produto->slug, $troca->id_proposta_produto]),
					'id_usuario' => $troca->proposta->id_usuario,
					'visualizada' => 0
				],
				Auth::user()->nome . ' ' . Auth::user()->sobrenome,
				'troca_realizada'
			);

			DB::commit();
			return response()->json(['trocado' => true]);
		} catch (Exception $e) {
			DB::rollback();

			return response()->json(['trocado' => false]);
		}
	}


	public function postCancelarTroca()
	{
		$dados = Input::all();

		try {
			
			DB::beginTransaction();

			$troca = $this->produtoTrocaRepo->atualizar([], Input::get('id_produto_troca'), 'cancelada');

			$this->produtoRepo->atualizar([
				'trocado' => 0,
				'aceita_proposta' => 1
			], $troca->id_produto);

			$this->produtoRepo->atualizar([
				'trocado' => 0,
				'aceita_proposta' => 1
			], $troca->proposta->produtoProposta->produto->id_produto);

			$this->notificacaoUsuarioRepo->criarNovaNotificacao(
				[
					'url_conteudo' => route('mensagens_produto.index', [$troca->proposta->produto->slug, $troca->id_proposta_produto]),
					'id_usuario' => $troca->proposta->id_usuario,
					'visualizada' => 0
				],
				Auth::user()->nome . ' ' . Auth::user()->sobrenome,
				'cancelada'
			);

			DB::commit();
			return response()->json(['cancelada' => true]);
		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['cancelada' => false]);
		}
	}

}

