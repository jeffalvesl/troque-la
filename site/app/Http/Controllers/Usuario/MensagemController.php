<?php namespace App\Http\Controllers\Usuario;

use Input;
use Auth;
use DB;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;
use App\Repositories\Produto\PropostaProdutoRepository;
use App\Repositories\Produto\MensagemPropostaUsuarioRepository;
use App\Repositories\Produto\ProdutoTrocaRepository;
use View;

class MensagemController extends BaseController
{

	public $title = 'Propostas';

	public function __construct(
		UsuarioRepository $usuarioRepo,
		ProdutoRepository $produtoRepo,
		NotificacaoUsuarioRepository $notificacaoUsuarioRepo,
		PropostaProdutoRepository $propostaProdutoRepo,
		MensagemPropostaUsuarioRepository $mensagemPropostaUsuarioRepo,
		ProdutoTrocaRepository $produtoTrocaRepo
	)
	{
		$this->middleware('auth');
		$this->usuarioRepo = $usuarioRepo;
		$this->produtoRepo = $produtoRepo;
		$this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
		$this->propostaProdutoRepo = $propostaProdutoRepo;
		$this->mensagemPropostaUsuarioRepo = $mensagemPropostaUsuarioRepo;
		$this->produtoTrocaRepo = $produtoTrocaRepo;
		parent::__construct();
	}

	/**
	 * View index de usuário.
	 * @return View.
	 */
	public function mensagensProduto($slug, $idProposta)
	{
		$produto = $this->produtoRepo->with([
				'interesses',
				'imagens',
				'propostas.mensagens.usuario',
				'troca.situacao'
			])->makeModel()
			->where(['slug' => $slug])
			->first();

		$proposta = $produto->propostas->filter(function($query) use ($idProposta){
			return $query->id_proposta_produto == $idProposta;
		})->first();


		if(!$proposta)
			abort(404);

		if($proposta->usuario->id_usuario !== Auth::user()->id_usuario && $produto->usuario->id_usuario !== Auth::user()->id_usuario)
			abort(404);

		$proposta->mensagens->sortByDesc(function($mensagem){
			return $mensagem->criado_em;
		});

		$this->mensagemPropostaUsuarioRepo->makeModel()->where(
			['id_proposta_produto' => $proposta->id_proposta_produto]
		)->whereNotIn('id_usuario', [Auth::user()->id_usuario])->update(['visualizada' => 1]);

		$produtoNofic = route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]);

		$this->title = 'Proposta - ' . $produto->produto;
		View::share('title', $this->title);

		DB::beginTransaction();

		$atualizou = $this->notificacaoUsuarioRepo->atualizar(
			[
				'url_conteudo' => $produtoNofic,
				'id_usuario' => Auth::user()->id_usuario
			],
			[
				'visualizada' => 1
			]
		);

		DB::commit();
		if(!$atualizou)
		{
			DB::rollback();
		}


		return view('usuario.mensagens.index', compact('produto', 'proposta'));
	}


	/**
	 * Envia mensagens para uma proposta.
	 */
	public function postEnviaMensagemProposta()
	{
		if(!Request::ajax())
			abort(404);

		$dados = Input::all();
		$enviou = false;

		try {
			DB::beginTransaction();

			$idPropostaProduto = Input::get('id_proposta_produto');
			$produto = $this->produtoRepo->retornaProduto(
				[
					'id_produto' => Input::get('id_produto')
				],

				[
					'propostas'
				],
				'propostas',
				function($query) use ($idPropostaProduto){
					$query->where('id_proposta_produto', $idPropostaProduto);
				}
			);

			$mensagem = $this->mensagemPropostaUsuarioRepo->create(
				[
					'mensagem' => Input::get('mensagem'),
					'id_proposta_produto' => Input::get('id_proposta_produto'),
					'id_usuario' => Auth::user()->id_usuario
				]
			);

			$produto = $this->produtoRepo->find(Input::get('id_produto'));
			$proposta = $this->propostaProdutoRepo->find(Input::get('id_proposta_produto'));

			// Se o usuário logado for o mesmo da proposta vamos enviar uma mensagem para o dono do produto
			$idUsuarioDestNotificacao = $proposta->usuario->id_usuario;
			if($idUsuarioDestNotificacao == Auth::user()->id_usuario)
			{
				$idUsuarioDestNotificacao = $produto->usuario->id_usuario;
			}

			$this->notificacaoUsuarioRepo->criarNovaNotificacao(
				[
					'url_conteudo' => route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]),
					'id_usuario' => $idUsuarioDestNotificacao,
				],
				Auth::user()->nome . ' ' . Auth::user()->sobrenome,
				'mensagem'
			);

			DB::commit();

			$enviou = true;

			//Enviar email para o usuário do produto em questão.
			/*$enviou = $this->notificacaoUsuarioRepo->enviarEmail(
				[
					'dono_proposta' => $proposta->usuario->nome .' ' .$proposta->usuario->sobrenome,
					'produto' => $produto->produto,
					'dono_produto' => $produto->usuario->nome . ' ' . $produto->usuario->sobrenome,
					'email' => $proposta->usuario->email,
					'usuario_remetente' => Auth::user()->nome . ' ' . Auth::user()->sobrenome,
					'email_remetente' => Auth::user()->email,
					'assunto' => 'Mensagem sobre proposta ',
					'url_mensagem' => route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto])
				],
				'emails.mensagem_proposta'
			);*/

			return response()->json(['enviou' => true]);
		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['enviou' => $enviou]);
		}
	}


	/**
	 * Verifica se este usuário possui notificações.
	 * @return response json.
	 */
	public function verificaNotificacao()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$notificacoes = $this->notificacaoUsuarioRepo->retornaNotificacao(Auth::user()->id_usuario, 3, true, 0);

		return response()->json(['notificacoes' => $notificacoes]);
	}

	public function verificaMensagemProposta()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$mensagem = $mensagem = $this->mensagemPropostaUsuarioRepo->makeModel()
			->where(['id_proposta_produto' => Input::get('id_proposta_produto'), 'visualizada' => 0])
			->whereNotIn('id_usuario', [Auth::user()->id_usuario])
			->first();

		if($mensagem)
			return response()->json(['temMensagem' => true]);

		return response()->json(['temMensagem' => false]);
	}

}
