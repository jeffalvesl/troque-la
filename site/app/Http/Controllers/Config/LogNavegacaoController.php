<?php namespace App\Http\Controllers\Config;

use Input;
use Auth;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Config\LogNavegacaoRepository;

class LogNavegacaoController extends BaseController
{

	public function __construct(LogNavegacaoRepository $logNavRepo)
	{
		$this->middleware('auth');
		$this->logNavRepo = $logNavRepo;
	}


	public function log()
	{
		if(!Request::ajax())
			abort(404);
	}

}
