<?php namespace App\Http\Controllers\Localidade;

use Input;
use Auth;
use Cookie;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Localidade\EstadoRepository;
use App\Repositories\Localidade\CidadeRepository;
use Illuminate\Http\Response;

class LocalidadeController extends BaseController
{

	public function __construct(EstadoRepository $estadoRepo, CidadeRepository $cidadeRepo, Response $response)
	{
		$this->middleware('auth', ['except' => ['setarCidade']]);
		$this->estadoRepo = $estadoRepo;
		$this->cidadeRepo = $cidadeRepo;
		$this->response = $response;
	}

	/**
	 * Consulta uma cidade existente no banco de dados.
	 * @return response json.
	 */
	public function consultarLocalidade()
	{
		if(!Request::ajax())
		{
			abort(404);
		}
		
		$estado = $this->estadoRepo->estadoExiste(Input::get('sigla'));
		$cidade = $this->cidadeRepo->cidadeExiste($estado->id_estado, Input::get('cidade'));

		return response()->json(['id_cidade' => $cidade->id_cidade, 'id_estado' => $estado->id_estado]);
	}


	public function setarCidade()
	{
		$cidade = $this->cidadeRepo->find(Input::get('id_cidade'));
		$dadosCookie = ['cidade' => $cidade->cidade, 'id_cidade' => $cidade->id_cidade];

		$cookie = cookie()->forever('cidade', $dadosCookie);

		return response()->json(['setou' => true])->withCookie($cookie);
	}
		
}
