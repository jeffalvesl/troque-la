<?php namespace App\Http\Controllers\Institucional;

use Auth;
use DB;
use Request;
use Input;
use App\Http\Controllers\BaseController;
use App\Services\Arquivo\FileUpload;
use App\Repositories\Institucional\ErroRepository;

class ErroController extends BaseController
{


	public function __construct(ErroRepository $erroRepo, FileUpload $fileUpload)
	{
		$this->middleware('auth');
		$this->erroRepo = $erroRepo;
		$this->fileUpload = $fileUpload;
	}


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('erros.index');
	}


	public function salvar()
	{
		$dados = Input::all();

		$file = Request::file('print_tela');
		$file = $this->fileUpload->upload($file, 'erros/'.Auth::user()->id_usuario, 'erro-'.Auth::user()->id_usuario);
		
		$dados['print_tela'] = $file['arquivo'];

		try {
			DB::beginTransaction();

			$dados['id_usuario'] = Auth::user()->id_usuario;
			
			$this->erroRepo->create($dados);
			DB::commit();

			return redirect()->route('home.index');
		} catch (Exception $e) {
			
			DB::rollback();

			return redirect()->back();
		}
	}

}
