<?php namespace App\Http\Controllers\Institucional;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Repositories\Institucional\ContatoRepository;
use App\Repositories\Usuario\UsuarioRepository;
use View;
use Input;

class ContatoController extends BaseController
{
	public $pagina = 'contato';

	public function __construct(ContatoRepository $contatoRepo, UsuarioRepository $usuarioRepo) 
	{
		$this->contatoRepo = $contatoRepo;
		$this->usuarioRepo = $usuarioRepo;
	}

	public function postContato()
	{
		$dados = Input::all();
		try {
			$usuario = $this->usuarioRepo->findByField('email', $dados['email'])->first();

			if($usuario)
			{
				$dados['id_usuario'] = $usuario->id_usuario;
			}

			$this->contatoRepo->create($dados);

			return response()->json(['enviou' => true]);
		} catch (Exception $e) {
			return response()->json(['enviou' => false]);
		}
	}

}
