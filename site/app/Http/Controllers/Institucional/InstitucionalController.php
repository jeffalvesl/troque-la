<?php namespace App\Http\Controllers\Institucional;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Repositories\Institucional\InstitucionalRepository;
use App\Repositories\Institucional\AjudaRepository;
use View;

class InstitucionalController extends BaseController
{
	public $pagina = 'sobrePage';

	public function __construct(InstitucionalRepository $institucionalRepo, AjudaRepository $ajudaRepo)
	{
		$this->institucionalRepo = $institucionalRepo; 
		$this->ajudaRepo = $ajudaRepo; 
	}

	/**
	 * View index deste controller.
	 */
	public function index()
	{
		$this->title = 'Sobre';
		View::share('title', $this->title);

		$sobre = $this->institucionalRepo->retornaInstitucional('sobre');
		View::share('description', substr($sobre->texto, 0, 160));

		$idPagina = $this->pagina;

		$ajuda = $this->ajudaRepo->makeModel()->where('ativo', 1)->orderBy('criado_em', 'desc')->get();

		return view('sobre.index', compact('sobre', 'idPagina', 'ajuda'));
	}

	/**
	 * Página de privacidade.
	 */
	public function privacidade()
	{
		$this->title = 'Política de Privacidade';
		$privacidade = $this->institucionalRepo->retornaInstitucional('privacidade');
		View::share('description', substr($privacidade->texto, 0, 160));

		View::share('title', $this->title);

		return view('privacidade.index', compact('privacidade'));
	}


	public function guia()
	{
		$this->title = 'Guia Prático ao usuário do Troque lá';

		View::share('title', $this->title);
		View::share('description', 'Conheça o modo de usar o troque.la');

		return view('guia.index');

	}

}
