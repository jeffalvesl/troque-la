<?php namespace App\Http\Controllers\Institucional;

use Input;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Services\Login\Login;
use Laravel\Socialite\Contracts\Factory as Socialite;
use View;
use Auth;
use Request;

class CadastroController extends BaseController
{

	public function __construct(UsuarioRepository $usuarioRepo, Socialite $socialite, Login $loginService)
	{
		$this->usuarioRepo = $usuarioRepo;
		$this->socialite = $socialite;
		$this->loginService = $loginService;
	}
	
	/**
	 * View index de cadastros
	 * @return View.
	 */
	public function index()
	{
		$this->title = 'Cadastre-se';
		View::share('title', $this->title);
		View::share('description', 'Cadastre-se no troque lá e comece a se trocar seus produtos');
		
		return view('cadastro.index');
	}

	/**
	 * Faz o cadastro de um novo usuário.
	 * @return redirect para página de completar cadastro do usuário.
	 */
	public function postCadastrar()
	{
		try {
			$dados = Input::all();
			
			$credenciais['email'] = Input::get('email');
			$credenciais['password'] = Input::get('password');
			
			$cadastrou = $this->usuarioRepo->cadastrar($dados, $credenciais);

			if(!is_array($cadastrou) && $cadastrou)
			{
				return redirect()->route('usuario.index');
			}

			return redirect()->back()->withInput();

		} catch (Exception $e) {
			
		}
	}

	/**
	 * Solicita os dados para uma network
	 * @param string $network - facebook.
	 */
	public function redirectToProvider($network)
	{
    	return $this->socialite->with($network)
    		->fields(['first_name', 'last_name', 'email', 'gender', 'birthday'])
    		->scopes([
    			'email', 'user_birthday'
    		])
    		->redirect();
	}

	/**
	 * Recebe dados da network.
	 */
	public function handleProviderCallback()
	{

		try {

	    	$usuario = $this->socialite->with('facebook')->user();
	    	$usuarioExist = $this->usuarioRepo->findByField('email', $usuario->user['email'])->first();

	    	if($usuarioExist)
	    	{
	    		$logou = $this->loginService->loginWithId($usuarioExist->id_usuario); 
	    		$view = 'usuario.index';

	    		if(!Auth::user()->primeiro_acesso)
	    		{
	    			$view = 'home.index';
	    		}
	    		return redirect()->route($view);
	    	}
	    	
	    	$dados = [
	    		'usuario' => $usuario->getName(),
	    		'nome' => $usuario->user['first_name'],
	    		'sobrenome' => $usuario->user['last_name'],
	    		'email' => $usuario->user['email'],
	    		'id_facebook' => $usuario->user['id'],
	    		'primeiro_acesso' => 1
	    	];

	    	return view('cadastro.cadastro_social', compact('dados'));

		} catch (Exception $e) {
			return redirect()->route('cadastro.index');
		}

	}

	/**
	 * Confirm email of the user.
	 * @param string $hash.
	 */
	public function confirmEmail($hash)
	{
		$confirmed = $this->userRepo->confirmEmail($hash);
		if(!$confirmed)
			return redirect()->route('login.index');

		return redirect()->route('home.index');
	}


	/**
	 * Valida a existencia de um email no banco de dados.
	 */
	public function verificaEmail()
	{
		if(!Request::ajax())
		{
			abort(404);
		}
		$email = Input::get('email');

		try {
			
			$existe = $this->usuarioRepo->findByField('email', $email)->first();
		
			if(!$existe)
				return response()->json(true);

			return response()->json(false);

		} catch (Exception $e) {
			
		}
	}

}
