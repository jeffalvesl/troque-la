<?php namespace App\Http\Controllers\Institucional;

use App\Http\Controllers\Controller;
use App\Repositories\Institucional\TermoRepository;
use App\Http\Controllers\BaseController;
use View;

class TermoController extends BaseController
{


	public function __construct(TermoRepository $termoRepo)
	{
		$this->termoRepo = $termoRepo; 
	}

	/**
	 * View index deste controler.
	 */
	public function index()
	{
		$this->title = 'Termos de Uso';
		View::share('title', $this->title);

		$termo = $this->termoRepo->getTermo();
		View::share('description', substr($termo->termo, 0, 160));
		
		return view('termos.index', compact('termo'));
	}

}
