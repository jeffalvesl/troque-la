<?php namespace App\Http\Controllers\Auth;

use Input;
use Request;
use Auth;
use App\Services\Login\Login;
use App\Http\Controllers\BaseController;
use App\Services\Email\EmailService;
use App\Repositories\Usuario\RecuperarSenhaRepository;


class LoginController extends BaseController
{

	public function __construct(Login $userLogin, EmailService $emailService, RecuperarSenhaRepository $recuperarSenhaRepo)
	{
		$this->userLogin = $userLogin;
		$this->emailService = $emailService;
		$this->recuperarSenhaRepo = $recuperarSenhaRepo;
	}

	/**
	 * View index deste controller.
	 */
	public function index()
	{
		if(Auth::user())
			return redirect()->route('home.index');

		return view('login.index');
	}

	/**
	 * View entrar.
	 */
	public function entrar()
	{
		if(Auth::user())
			return redirect()->route('home.index');

		return view('login.login');
	}

	/**
	 * Post para autenticação.
	 */
	public function auth()
	{
		try {
			$manterLogado = Input::get('manter_logado');
			$this->userLogin->login(
				[
					'email' => Input::get('email'),
					'password' => Input::get('password')
				],
				$manterLogado
			);

			$route = '';
			if(Auth::user() && Auth::user()->primeiro_acesso)
			{
				$route = 'usuario.index';
			}

			if(!empty($route))
				return redirect()->route($route);

			return redirect()->back();

		} catch (Exception $e) {
			return redirect()->back();
		}
	}

	/**
	 * Verifica credenciais de um usuário antes de logar.
	 */
	public function verificaCredenciais()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {
			$manterLogado = 0;
			$logou = $this->userLogin->login(
				[
					'email' => Input::get('email'),
					'password' => Input::get('password')
				],
				$manterLogado
			);

			if($logou)
			{
				return response()->json(true);
			}

			return response()->json(false);
		} catch (Exception $e) {
			return response()->json(true);
		}
	}

	/**
	 * Sair.
	 */
	public function logout()
	{
		Auth::logout();
		return redirect()->back();
	}

}
