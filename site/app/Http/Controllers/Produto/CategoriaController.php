<?php namespace App\Http\Controllers\Produto;

use Input;
use Auth;
use Request;
use View;
use URL;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Repositories\Produto\CategoriaRepository;
use App\Services\Config\LogNavegacao;

class CategoriaController extends BaseController
{

	public function __construct(UsuarioRepository $usuarioRepo, CategoriaRepository $categoriaRepo, LogNavegacao $log)
	{
		$this->usuarioRepo = $usuarioRepo;
		$this->categoriaRepo = $categoriaRepo;
		$this->log = $log;
	}

	/**
	 * View index deste controller.
	 * @return View.
	 */
	public function index()
	{
		$categorias = $this->categoriaRepo->retornaCategorias(['produtos', 'subCategorias']);

		$this->title = 'Categorias';
		View::share('title', $this->title);

		return view('categorias.index', compact('categorias'));
	}


	public function categoria($slug)
	{
		$categoria = $this->categoriaRepo->with(['produtos.usuario'])
					->makeModel()
					->where('slug', $slug)
					->first();

		if(Auth::user())
		{
			$this->log->registrar(
				[
					'log_navegacao' => 'Visitou a categoria ' . $categoria->categoria,
					'url_conteudo' => URL::current(),
					'tipo' => 'visita',
					'titulo_pagina' => $categoria->categoria,
					'url_anterior' => URL::previous(),
					'email' => Auth::user()->email,
					'usuario' => Auth::user()->nome . ' ' . Auth::user()->sobrenome,
					'id_usuario' => Auth::user()->id_usuario
				]
			);
		}

		$produtos = $categoria->produtos->filter(function($query){
			return $query->publicado = 1;
		});

		if($produtos)
			$produtos = $categoria->produtos()->paginate(20);

		$this->title = $categoria->categoria;
		View::share('title', $this->title);

		return view('produtos.categorias.index', compact('categoria', 'produtos'));

	}

	/**
	 * Retorna todas as categorias.
	 * @return response json.
	 */
	public function retornarCategorias()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		$categorias = $this->categoriaRepo->pesquisarCategoria(Input::get('termo'));

		return response()->json($categorias);
	}

}
