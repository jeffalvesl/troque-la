<?php namespace App\Http\Controllers\Produto;

use Auth;
use URL;
use View;
use Input;
use Session;
use DB;
use Cookie;
use Request;
use App\Http\Controllers\BaseController;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Produto\PropostaProdutoRepository;
use App\Repositories\Produto\ProdutoPropostaProdutoRepository;
use App\Repositories\Usuario\NotificacaoUsuarioRepository;
use App\Repositories\Usuario\ProdutoInteresseUsuarioRepository;
use App\Services\Produto\Produto;
use App\Services\Config\LogNavegacao;

class ProdutoController extends BaseController
{

	public function __construct(
		ProdutoRepository $produtoRepo,
		PropostaProdutoRepository $propostaProdutoRepo,
		ProdutoPropostaProdutoRepository $produtoPropostaProdutoRepo,
		NotificacaoUsuarioRepository $notificacaoUsuarioRepo,
		Produto $produtoServ,
		LogNavegacao $log,
		ProdutoInteresseUsuarioRepository $produtoInteresseUserRepo
		)
	{
		$this->produtoRepo = $produtoRepo;
		$this->propostaProdutoRepo = $propostaProdutoRepo;
		$this->produtoPropostaProdutoRepo = $produtoPropostaProdutoRepo;
		$this->notificacaoUsuarioRepo = $notificacaoUsuarioRepo;
		$this->produtoServ = $produtoServ;
		$this->log = $log;
		$this->produtoInteresseUserRepo = $produtoInteresseUserRepo;
		parent::__construct();
	}


	/**
	 * View index.
	 *
	 * @return View
	 */
	public function index()
	{
		$usuario = isset(Auth::user()->id_usuario) ? Auth::user()->id_usuario : '';
		$recomendados = $this->produtoRepo->buscaProdutosInteresseProduto($usuario, 10);

		$novidades = $this->produtoRepo->retornaNovidades();

		$idNovidades = $novidades->lists('id_produto');

		foreach($recomendados as $recomendado)
		{
			$idNovidades[] = $recomendado->id_produto;
		}

		$maisProdutos = $this->produtoRepo
						->makeModel()
						->where('publicado', 1)
						->cidade()
						->whereNotIn('id_produto', $idNovidades)
						->paginate(12);


		$view = 'produtos.index';
		if(!is_null(Input::get('page')))
		{
			$view = 'produtos.todos_produtos';
			$this->title = 'Todos os Produtos';
		}

		View::share('title', $this->title);

		return view($view, compact('novidades', 'maisProdutos', 'recomendados'));
	}

	/**
	 * Detalhe do produto.
	 * @param string $slug - Slug do produto.
	 */
	public function detalheProduto($slug)
	{
		$with = ['imagens', 'interesses', 'usuario.cidade.estado'];
		if(Auth::user())
		{
			$with[] = 'propostas.usuario';
		}

		$produto = $this->produtoRepo->retornaProduto(
			[
				'slug' => $slug,
				'publicado' => 1
			],

			$with
		);

		if(!$produto)
			abort(404);

		$imagemProduto = $produto->imagens[0]->imagem;

		$produtosUsuarioLogado = array();
		if(Auth::user())
		{
			$produtosUsuarioLogado = $this->produtoRepo->retornaProdutosPorCondicao(
				[
					'publicado' => 1,
					'trocado' => 0,
					'id_usuario' => Auth::user()->id_usuario
				]
			);

			$this->log->registrar(
				[
					'log_navegacao' => 'Visitou o produto ' . $produto->produto,
					'url_conteudo' => URL::current(),
					'tipo' => 'visita',
					'titulo_pagina' => $produto->produto,
					'url_anterior' => URL::previous(),
					'email' => Auth::user()->email,
					'usuario' => Auth::user()->nome . ' ' . Auth::user()->sobrenome,
					'id_usuario' => Auth::user()->id_usuario
				]
			);
		}


		if(count($produto->propostas) OR isset($produto->propostas))
			$pessoasInteressadas = $produto->propostas->lists('id_usuario');
		else
			$pessoasInteressadas = array();


		$this->produtoServ->atualizaViews($produto);

		$relacionados = $this->produtoRepo->retornaProdutosPorCondicao(
			[
				'publicado' => 1,
				'id_sub_categoria' => $produto->id_sub_categoria
			],

			false
		);

		$novidades = $this->produtoRepo->retornaProdutosPorCondicao(
			[
				'publicado' => 1,
				'trocado' => 0
			],
			false,
			6
		);

		$this->title = $produto->produto;
		$this->description = $produto->descricao_produto;
		View::share('title', $this->title);
		View::share('description', substr($this->description, 0, 160));

		return view('produtos.detalhe', compact('produto', 'relacionados', 'novidades', 'pessoasInteressadas', 'produtosUsuarioLogado', 'imagemProduto'));
	}


	public function buscar()
	{
		$termo = Input::get('termo');

		$maisProdutos = $this->produtoRepo->pesquisarProdutos($termo);
		if(!$maisProdutos)
			abort(404);


		$this->title = $termo;
		View::share('title', $termo);

		return view('produtos.todos_produtos', compact('maisProdutos'));
	}

	/**
	 * Salva uma proposta do produto.
	 */
	public function postPropostaProduto()
	{
		if(!Request::ajax())
		{
			abort(404);
		}

		try {

			//Inicia ação no banco de dados.
			DB::beginTransaction();
			$proposta = $this->propostaProdutoRepo->create(
				[
					'proposta' => Input::get('proposta'),
					'id_usuario' => Auth::user()->id_usuario,
					'id_produto' => Input::get('id_produto')
				]
			);

			if($proposta)
			{
				// Cria o produto referente a proposta
				$this->produtoPropostaProdutoRepo->create(['id_proposta_produto' => $proposta->id_proposta_produto, 'id_produto' => Input::get('produto_proposta_produto')]);
				$produto = $this->produtoRepo->with(['usuario'])->find(Input::get('id_produto'));

				//Cria notificação para o usuário dono do produto em questão.
				$this->notificacaoUsuarioRepo->criarNovaNotificacao([
						'url_conteudo' => route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]),
						'id_usuario' => $produto->id_usuario,
						'visualizada' => 0
					],
					Auth::user()->nome . ' ' . Auth::user()->sobrenome,
					'proposta'
				);

				DB::commit();

				//Enviar email para o usuário do produto em questão.
				/*$enviou = $this->notificacaoUsuarioRepo->enviarEmail(
					[
						'produto' => $produto->produto,
						'dono_produto' => $produto->usuario->nome . ' ' .$produto->usuario->sobrenome,
						'email' => $produto->usuario->email,
						'usuario_remetente' => Auth::user()->nome . ' ' . Auth::user()->sobrenome,
						'email_remetente' => Auth::user()->email,
						'assunto' => 'Proposta de troca. Produto: ' . $produto->produto,
						'url_mensagem' => route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto])
					]
				);*/


				return response()->json(['salvou' => true]);
			}

			return response()->json(['salvou' => true]);
		} catch (Exception $e) {
			DB::rollback();
			return response()->json(['salvou' => false]);
		}
	}

	public function produtosInteresse()
	{
		if(!Auth::user())
			die('faça login e confira o que preparamos pra você');

		$produtos = Auth::user()->produtoInteresse;
		$produtos = $this->produtoRepo->makeModel()->whereIn('id_produto', $produtos->lists('id_produto'))->get();

		$this->title = 'Produtos com base nos seus interesses.';
		View::share('title', $this->title);

		return view('produtos.todos_produtos', compact('produtos'));
	}

}
