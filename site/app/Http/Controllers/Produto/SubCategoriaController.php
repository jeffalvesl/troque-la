<?php namespace App\Http\Controllers\Produto;

use Input;
use Auth;
use Request;
use View;
use URL;
use App\Http\Controllers\BaseController;
use App\Repositories\Usuario\UsuarioRepository;
use App\Repositories\Produto\SubCategoriaRepository;
use App\Services\Config\LogNavegacao;

class SubCategoriaController extends BaseController
{

	public function __construct(UsuarioRepository $usuarioRepo, SubCategoriaRepository $subCategoriaRepo, LogNavegacao $log)
	{
		$this->usuarioRepo = $usuarioRepo;
		$this->subCategoriaRepo = $subCategoriaRepo;
		$this->log = $log;
	}

	/**
	 * View index deste controller.
	 * @return View.
	 */
	public function index($categoria, $subCategoria)
	{
		$subCategoria = $this->subCategoriaRepo->with(['categoria', 'produtos'])
						->makeModel()
						->where('slug', $subCategoria)
						->first();

		$produtos = $subCategoria->produtos->filter(function($query){
			return $query->publicado == 1;
		});

		if(!$subCategoria)
			abort(404);

		if(Auth::user())
		{
			$this->log->registrar(
				[
					'log_navegacao' => 'Visitou a Sub Categoria ' . $subCategoria->sub_categoria,
					'url_conteudo' => URL::current(),
					'tipo' => 'visita',
					'titulo_pagina' => $subCategoria->sub_categoria,
					'url_anterior' => URL::previous(),
					'email' => Auth::user()->email,
					'usuario' => Auth::user()->nome . ' ' . Auth::user()->sobrenome,
					'id_usuario' => Auth::user()->id_usuario
				]
			);
		}

		$this->title = $subCategoria->sub_categoria;
		View::share('title', $this->title);

		return view('produtos.categorias.subcategorias.index', compact('produtos', 'subCategoria'));
	}

}
