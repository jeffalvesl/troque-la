<?php namespace App\Http\Controllers;

use View;
use Auth;

class BaseController extends Controller
{

	public $title;
	public $description;

	public function __construct()
	{

    }

	public function createSlug($titulo)
	{
		$text = $this->tiraLetrascomAcento($titulo);

		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		return $text;
	}


	public function tiraLetrascomAcento($texto)
	{
		$letrasAcento = array('á','à','â','ã','ª','Á','À','Â','Ã', 'é','è','ê','É','È','Ê','í','ì','î','Í','Ì','Î','ò','ó','ô', 'õ','º','Ó','Ò','Ô','Õ','ú','ù','û','Ú','Ù','Û','ç','Ç','Ñ','ñ');
		$letrasSemAcento = array('a','a','a','a','a','A','A','A','A','e','e','e','E','E','E','i','i','i','I','I','I','o','o','o','o','o','O','O','O','O','u','u','u','U','U','U','c','C','N','n');

		return str_replace($letrasAcento, $letrasSemAcento, $texto);
	}
}
