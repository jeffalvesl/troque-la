<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\BaseController;
use App\Repositories\Institucional\InstitucionalRepository;
class HomeController extends BaseController
{


	public function __construct(InstitucionalRepository $institucionalRepo)
	{
		$this->institucionalRepo = $institucionalRepo;
	}


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sobre = $this->institucionalRepo->retornaInstitucional('sobre');

		return view('home.index', compact('sobre'));
	}

}
