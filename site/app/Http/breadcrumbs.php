<?php

use App\Models\Produto\Produto;


Breadcrumbs::register('home.index', function($breadcrumbs)
{
    $breadcrumbs->push('Produtos', route('home.index'));
});

Breadcrumbs::register('produtos.detalhe', function($breadcrumbs, $produto)
{
    $breadcrumbs->push('Produtos', route('home.index'));
	$breadcrumbs->push($produto->subCategoria->categoria->categoria, route('produto_categoria.get', [$produto->subCategoria->categoria->slug]));
	$breadcrumbs->push($produto->subCategoria->sub_categoria, route('subcategoria_produtos.get', [$produto->subCategoria->categoria->slug, $produto->subCategoria->slug]));
	$breadcrumbs->push($produto->produto, route('produtos.detalhe', [$produto->slug]));
});

Breadcrumbs::register('mensagens_produto.index', function($breadcrumbs, $produto)
{
    $breadcrumbs->push('Produtos', route('home.index'));
	$breadcrumbs->push('Mensagens', route('mensagens_produto.index', [$produto->slug]));
});

Breadcrumbs::register('produto_categoria.get', function($breadcrumbs, $categoria)
{
    $breadcrumbs->push('Produtos', route('home.index'));
	$breadcrumbs->push($categoria->categoria, route('produto_categoria.get', $categoria->slug));
});

Breadcrumbs::register('subcategoria_produtos.get', function($breadcrumbs, $subCategoria)
{
    $breadcrumbs->push('Produtos', route('home.index'));
	$breadcrumbs->push($subCategoria->categoria->categoria, route('produto_categoria.get', $subCategoria->categoria->slug));
	$breadcrumbs->push($subCategoria->sub_categoria, route('subcategoria_produtos.get', [$subCategoria->categoria->slug, $subCategoria->slug]));
});


// Home > About
Breadcrumbs::register('about', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About', route('about'));
});

/**
 * Breadcrumbs do ambiente do usuário.
 */



Breadcrumbs::register('usuario.index', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
});

Breadcrumbs::register('produto_usuario.novo', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Adicionar Produto' , route('produto_usuario.novo'));
});

Breadcrumbs::register('editar.produto', function($breadcrumbs, $produto)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Produtos postados' , route('produto_usuario.index'));
    $breadcrumbs->push($produto->produto, route('editar.produto', [$produto->slug]));
});

Breadcrumbs::register('produto_usuario.index', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Produtos Postados' , route('produto_usuario.index'));
});

Breadcrumbs::register('minhas_trocas.index', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Minhas Trocas' , route('minhas_trocas.index'));
});

Breadcrumbs::register('reportar_erro.index', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Reportar Erro' , route('reportar_erro.index'));
});

Breadcrumbs::register('cancelar.conta', function($breadcrumbs)
{
    $breadcrumbs->push(Auth::user()->nome . ' ' .Auth::user()->sobrenome , route('usuario.index'));
    $breadcrumbs->push('Cancelar Conta' , route('cancelar.conta'));
});
