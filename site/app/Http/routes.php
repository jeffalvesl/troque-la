<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home.index', 'uses' => 'Produto\ProdutoController@index']);

Route::get('/login', function(){
    return redirect()->route('home.index');
});

Route::get('/produtos', function(){
    return redirect()->route('home.index');
});

Route::get('cadastre-se', ['as' => 'cadastro.index', 'uses' => 'Institucional\CadastroController@index']);
Route::post('cadastrar', ['as' => 'cadastro.salvar', 'uses' => 'Institucional\CadastroController@postCadastrar']);
Route::post('verifica-email', ['as' => 'verifica_email.post', 'uses' => 'Institucional\CadastroController@verificaEmail']);
Route::get('cadastre-se/{provider?}', ['as' => 'cadastro.social', 'uses' => 'Institucional\CadastroController@redirectToProvider']);
Route::get('callback', ['as' => 'callback.social', 'uses' => 'Institucional\CadastroController@handleProviderCallback']);


/**
 * Institucional
 */
Route::group(['prefix' => 'institucional', 'namespace' => 'Institucional'], function() {
    Route::get('termos-de-uso', ['as' => 'termo.index', 'uses' => 'TermoController@index']);
    Route::get('sobre', ['as' => 'sobre.index', 'uses' => 'InstitucionalController@index']);
    Route::get('privacidade', ['as' => 'privacidade.index', 'uses' => 'InstitucionalController@privacidade']);
    Route::get('guia', ['as' => 'guia.index', 'uses' => 'InstitucionalController@guia']);

    Route::post('salvar-contato', ['as' => 'contato.post', 'uses' => 'ContatoController@postContato']);

    Route::get('reportar-erro', ['as' => 'reportar_erro.index', 'uses' => 'ErroController@index']);
    Route::post('reportar-erro/salvar', ['as' => 'reportar_erro.post', 'uses' => 'ErroController@salvar']);
});


/**
 * Institucional
 */
Route::group(['prefix' => 'usuario', 'namespace' => 'Usuario'], function() {
    Route::get('minhas-informacoes', ['as' => 'usuario.index', 'uses' => 'UsuarioController@index']);
    Route::post('atualizar-informacoes', ['as' => 'atualizar_informacoes.post', 'uses' => 'UsuarioController@postAtualizaDados']);
    Route::post('interesses-usuario', ['as' => 'interesse_usuario.post', 'uses' => 'InteresseController@retornaInteresses']);
    Route::get('interesses-usuario-troca', ['as' => 'interesse_usuario_troca.get', 'uses' => 'InteresseController@pesquisaInteresse']);

    Route::get('mensagens', ['as' => 'mensagens.index', 'uses' => 'MensagemController@index']);
    Route::get('mensagens/{slug}/{id_proposta}', ['as' => 'mensagens_produto.index', 'uses' => 'MensagemController@mensagensProduto']);
    Route::post('verifica-notificacao', ['as' => 'verifica.notificacao', 'uses' => 'MensagemController@verificaNotificacao']);
    Route::post('enviar-mensagem', ['as' => 'enviar_mensagem.post', 'uses' => 'MensagemController@postEnviaMensagemProposta']);
    Route::post('responder-mensagem', ['as' => 'responder_mensagem.post', 'uses' => 'MensagemController@responderMensagem']);
    Route::post('verifica-mensagem-proposta', ['as' => 'verifica.mensagemProposta', 'uses' => 'MensagemController@verificaMensagemProposta']);

    Route::get('cancelar-conta', ['as' => 'cancelar.conta', 'uses' => 'UsuarioController@cancelarConta']);
    Route::get('ativar-conta', ['as' => 'ativar.conta', 'uses' => 'UsuarioController@ativarConta']);
    Route::post('cancelando', ['as' => 'cancelar.post', 'uses' => 'UsuarioController@postCancelarConta']);

    Route::get('esqueci-senha', ['as' => 'login.esqueci', 'uses' => 'RecuperarSenhaController@esqueci']);
    Route::post('esqueci-senha/enviar', ['as' => 'post.esqueci', 'uses' => 'RecuperarSenhaController@postEsqueci']);

    Route::get('definir-nova-senha/{hash}', ['as' => 'definir.senha', 'uses' => 'RecuperarSenhaController@definirNovaSenha']);
    Route::post('definir-nova-senha/salvar', ['as' => 'definir_senha.post', 'uses' => 'RecuperarSenhaController@postDefinirNovaSenha']);

});

/**
 * Produtos do usuário logado.
 */
Route::group(['prefix' => 'usuario/produto', 'namespace' => 'Usuario\Produto'], function() {
    Route::get('/produtos-postados', ['as' => 'produto_usuario.index', 'uses' => 'ProdutoController@index']);
    Route::get('/minhas-trocas', ['as' => 'minhas_trocas.index', 'uses' => 'ProdutoController@trocados']);
    Route::get('novo-produto', ['as' => 'produto_usuario.novo', 'uses' => 'ProdutoController@novoProduto']);

    Route::get('editar-produto/{slug}', ['as' => 'editar.produto', 'uses' => 'ProdutoController@editarProduto']);

    Route::post('post-editar-produto', ['as' => 'editar.produto_post', 'uses' => 'ProdutoController@postEditarProduto']);

    Route::get('imagens-produto/{slug}', ['as' => 'imagens_produto.index', 'uses' => 'ProdutoController@imagemProduto']);

    Route::get('excluir-imagem/{id}', ['as' => 'excluir_imagem.produto', 'uses' => 'ProdutoController@excluirImagemProduto']);
    Route::post('salvar', ['as' => 'produto_usuario.salvar', 'uses' => 'ProdutoController@salvar']);

    Route::post('salvar-produto', ['as' => 'produto_usuario.novo_post', 'uses' => 'ProdutoController@postNovoProduto']);
    Route::post('salvar-imagens', ['as' => 'upload.imagem_produto', 'uses' => 'ProdutoController@upload']);
    Route::post('publicar-produto', ['as' => 'publicar_produto.post', 'uses' => 'ProdutoController@postPublicarProduto']);
    Route::post('buscar-produtos-usuario', ['as' => 'buscar_produtos.post', 'uses' => 'ProdutoController@buscarProdutosUsuario']);

    Route::get('/propostas', ['as' => 'propostas.index', 'uses' => 'PropostaController@index']);
    Route::get('propostas/{produto}', ['as' => 'propostas.produto', 'uses' => 'PropostaController@propostaProduto']);

    Route::post('aceitar-proposta', ['as' => 'aceitar_proposta.post', 'uses' => 'PropostaController@postAceitarProposta']);
    Route::post('confirmar-troca', ['as' => 'confirmar_troca.post', 'uses' => 'TrocaController@postConfirmarTroca']);
    Route::post('troca-realizada', ['as' => 'troca_realizada.post', 'uses' => 'TrocaController@postTrocaRealizada']);
    Route::post('cancelar-troca', ['as' => 'cancelar_troca.post', 'uses' => 'TrocaController@postCancelarTroca']);
});

/**
 * Localidade cidade estado
 */
Route::group(['prefix' => 'localidade', 'namespace' => 'Localidade'], function() {
    Route::post('consultar', ['as' => 'consultar_localidade.post', 'uses' => 'LocalidadeController@consultarLocalidade']);
    Route::post('trocar-cidade', ['as' => 'trocar.cidade', 'uses' => 'LocalidadeController@setarCidade']);
});

/**
 * Produtos em geral.
 */
Route::group(['prefix' => 'produtos', 'namespace' => 'Produto'], function() {
    //Route::get('/', ['as' => 'produtos.index', 'uses' => 'ProdutoController@index']);

    Route::get('/detalhe/{slug}', ['as' => 'produtos.detalhe', 'uses' => 'ProdutoController@detalheProduto']);
    Route::get('buscar', ['as' => 'buscar.produto', 'uses' => 'ProdutoController@buscar']);
    Route::get('produtos-de-interesse', ['as' => 'produtos.interesse', 'uses' => 'ProdutoController@produtosInteresse']);
    Route::get('buscar/?termo={termo}', ['as' => 'buscar.produto_termo', 'uses' => 'ProdutoController@buscar']);
    Route::get('/categorias', ['as' => 'categorias.index', 'uses' => 'CategoriaController@index']);
    Route::get('/{slug}', ['as' => 'produto_categoria.get', 'uses' => 'CategoriaController@categoria']);
    Route::get('{categoria}/{subcategoria}', ['as' => 'subcategoria_produtos.get', 'uses' => 'SubCategoriaController@index']);

    Route::post('salvar-proposta-produto', ['as' => 'proposta_produto.post', 'uses' => 'ProdutoController@postPropostaProduto']);

    Route::post('retornar-categorias', ['as' => 'retornar.categorias', 'uses' => 'CategoriaController@retornarCategorias']);
});


/**
 * Logar, lagout, renovar sessão.
 */
Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {
    Route::get('/', ['as' => 'login.index', 'uses' => 'LoginController@index']);
    Route::get('entrar', ['as' => 'login.login', 'uses' => 'LoginController@entrar']);
    Route::post('logando', ['as' => 'logar.post', 'uses' => 'LoginController@auth']);
    Route::post('verificar-credenciais', ['as' => 'verifica_credenciais.post', 'uses' => 'LoginController@verificaCredenciais']);
    Route::get('sair', ['as' => 'logout.get', 'uses' => 'LoginController@logout']);
});
