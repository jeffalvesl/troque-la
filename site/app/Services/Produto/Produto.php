<?php namespace App\Services\Produto;

use App\Repositories\Produto\ProdutoRepository;
use Auth;

class Produto
{

	public function __construct(ProdutoRepository $produtoRepo)
	{
		$this->produtoRepo = $produtoRepo;
	}

	/**
	 * Atualiza as views de um produto.
	 * @param  Produto $produto produto que está em questão.
	 * @return Bool true-false.
	 */
	public function atualizaViews($produto)
	{
		if(!Auth::user() OR Auth::user() && $produto->usuario->id_usuario != Auth::user()->id_usuario)
		{
			return $this->produtoRepo->atualizar(['views' => $produto->views + 1], $produto->id_produto);
		}
	}

	/**
	 * Retorna trocas feitas por um usuário. As propostas aceitas, e logo trocadas, também caeem nessa função.
	 * @param  array   $with             array com as relações.
	 * @param  boolean $propostasAceitas se são propostas aceitas = true se não = fase.
	 * @param  integer $qnt              quantidade de registros.
	 * @return Model Produto.
	 */
	public function trocas(array $with, $propostasAceitas = false, $qnt = 12, $whereNotIn = null)
	{
		$trocas = $this->produtoRepo->with($with)->makeModel()->where('trocado', 1)->where('id_usuario', Auth::user()->id_usuario);
		if(is_array($whereNotIn))
			$trocas->whereNotIn('id_produto', $whereNotIn);

		$trocas = $trocas->take($qnt)->get();

		return $trocas;
	}
}