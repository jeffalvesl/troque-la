<?php namespace App\Services\Cron;

use App\Repositories\Config\LogNavegacaoRepository;
use App\Repositories\Produto\ProdutoRepository;
use App\Repositories\Usuario\ProdutoInteresseUsuarioRepository;
use Auth;

class ProdutoInteresseUsuario
{

	public function __construct(LogNavegacaoRepository $logNavRepo, ProdutoRepository $produtoRepo, ProdutoInteresseUsuarioRepository $produtoInteresseUserRepo)
	{
		$this->logNavRepo = $logNavRepo;
		$this->produtoRepo = $produtoRepo;
		$this->produtoInteresseUserRepo = $produtoInteresseUserRepo;
	}

	public function executar()
	{
		$this->rotina();
	}

	/**
	 * Rotina de gerar produtos para cada usuário.
	 */
	private function rotina()
	{
		$logs = $this->getDados();
		$logs = $this->filtrar($logs);

		$this->buscarSugestoesProdutos($logs);
	}

	private function buscarSugestoesProdutos($dados)
	{
		foreach($dados as $key => $infoLog)
		{
			foreach($infoLog as $log)
			{
				$produtos = $this->produtoRepo->makeModel();
				$produtos->where('publicado', 1);
				$produtos->where('trocado', 0);
				$produtos->where('produto', 'LIKE', '%' . $log['titulo_pagina'] . '%');
				$produtos->orWhere('descricao_produto', 'LIKE', '%' . $log['titulo_pagina'] . '%');

				$produtos = $produtos->get();
				$produtos = $produtos->filter(function($query) use($log){
					return $query->id_usuario != $log['id_usuario'];
				});

				$idsProdutos = $produtos->lists('id_produto');

				$idsExistentes = $this->produtoInteresseUserRepo->makeModel()
					->whereIn('id_produto', $idsProdutos)
					->where('id_usuario', $log['id_usuario'])
					->get();
				$idsExistentes = $idsExistentes->lists('id_produto');

				foreach($idsProdutos as $id)
				{
					if(!in_array($id, $idsExistentes))
					{
						$this->registrarDados(
							[
								'id_produto' => $id,
								'id_log_navegacao' => $log['id_log_navegacao'],
								'id_usuario' => $log['id_usuario']
							]
						);
					}
				}
			}


		}
	}

	private function registrarDados(array $dados)
	{
		return $this->produtoInteresseUserRepo->create($dados);
	}


	/**
	 * Pega todos os logs dos usuários.
	 */
	private function getDados()
	{
		return $this->logNavRepo->makeModel()->orderBy('criado_em', 'DESC')->get();
	}

	/**
	 * Filtra os logs e pega campos especificos.
	 * @param  array $logs todos os logs.
	 * @return array.
	 */
	private function filtrar($logs)
	{
		$dados = array();
		foreach($logs as $log)
		{
			$dados[$log->id_usuario][] = [
				'id_log_navegacao' => $log->id_log_navegacao,
				'titulo_pagina' => $log->titulo_pagina,
				'id_usuario' => $log->id_usuario,
				'email' => $log->email,
				'usuario' => $log->usuario
			];
		}

		return $dados;
	}
}