<?php namespace App\Services\Config;

use App\Repositories\Config\LogNavegacaoRepository;
use Auth;

class LogNavegacao
{

	public function __construct(LogNavegacaoRepository $logNavRepo)
	{
		$this->logNavRepo = $logNavRepo;
	}

	public function registrar(array $dados)
	{
		return $this->logNavRepo->log($dados);
	}
}