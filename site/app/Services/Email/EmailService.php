<?php namespace App\Services\Email;

use Hash;
use Mail;

class EmailService
{


	public function send(array $dados, $view)
	{
		Mail::send($view, ['dados' => $dados], function($message) use ($dados)
		{
			$message->from(env('MAIL_USERNAME'), 'Troque lá');
		    $message->to($dados['email'])->subject($dados['assunto']);
		});
	}

}