<?php namespace App\Services\Arquivo;

use Intervention\Image\Facades\Image;

class FileUpload
{

	private $path = 'upload/';



	public function upload($file, $dir, $fileName, $type = 'image')
	{
		$namePath = $this->path.$dir.'/'.$fileName.'/';

		if(!is_dir($namePath))
		{
			mkdir($namePath, 0777, true);
		}

		if($type = 'image')
		{
			$dataImage = $this->image($file, $fileName);
			$dataImage['file_to_upload']->save($namePath.$dataImage['file_name']);
		}

		return ['arquivo' => $namePath.$dataImage['file_name']];
	}


	private function image($file, $fileName)
	{
		$numberFile = $this->numberFile();
		$extension = $this->getExtension($file);
		
		$fileName = $fileName.'-'.$numberFile.'.'.$extension;

		$fileUpload = Image::make($file->getRealPath());

		return ['file_name' => $fileName, 'file_to_upload' => $fileUpload];
	}

	private function getExtension($file)
	{
		return $file->getClientOriginalExtension();
	}


	private function numberFile()
	{
		return rand(11111,99999);
	}


	private function getDimensions($file)
	{
		list($width, $height) = getimagesize($file);

		return ['width' => $width, 'height' => $height];
	}


	public static function delete($image)
	{
		return unlink($image);
	}



	public static function uploadCrop($file, $width, $height, $x, $y)
	{
		$destinationPath = 'uploads'; // upload path

		$extension = $file->getClientOriginalExtension();
		$fileName = rand(11111,99999).'.'.$extension;

		Image::make($file->getRealPath())->fit($width, $height)->crop($width, $height)->save('uploads/'.$fileName);

		return ['file_name' => $fileName];
	}



	public static function uploadTemp($file, $destinationPath = null)
	{
		$destinationPath = is_null($destinationPath) ? 'temp' : $destinationPath; // upload path

		$extension = $file->getClientOriginalExtension();
		$fileName = rand(11111,99999).'.'.$extension;

		Image::make($file->getRealPath())->fit(800, 600)->save('uploads/'.$destinationPath.'/'.$fileName);

		return ['file_name' => $fileName];
	}

}