<?php namespace App\Services\Arquivo;

use Intervention\Image\Facades\Image;

class Arquivo
{

	private static $path = 'upload/';

	public static function upload($file, $dir, $fileName)
	{
		$namePath = self::$path.$dir.'/'.$fileName.'/';
		$namePathMinia = self::$path.$dir.'/'.$fileName.'/thumbs/';

		if(!is_dir($namePath))
		{
			mkdir($namePath, 0777, true);
		}

		if(!is_dir($namePathMinia))
		{
			mkdir($namePathMinia, 0777, true);
		}

		$extension = $file->getClientOriginalExtension();

		$numberFile = rand(11111,99999);
		
		$fileName = $fileName.'-'.$numberFile.'.'.$extension;
		
		list($width, $height) = getimagesize($file);
		$fileUpload = Image::make($file->getRealPath());
		$miniatura = Image::make($file->getRealPath())->fit(224, 135)->resize(224, 135, function($constraint){
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$fileUpload->save($namePath.$fileName);
		$miniatura->save($namePathMinia.$fileName);


		return ['arquivo' => $namePath.$fileName, 'miniatura' => $namePathMinia.$fileName];
	}


	public static function delete($image)
	{
		return unlink($image);
	}



	public static function uploadCrop($file, $width, $height, $x, $y)
	{
		$destinationPath = 'uploads'; // upload path

		$extension = $file->getClientOriginalExtension();
		$fileName = rand(11111,99999).'.'.$extension;

		Image::make($file->getRealPath())->fit($width, $height)->crop($width, $height)->save('uploads/'.$fileName);

		return ['file_name' => $fileName];
	}



	public static function uploadTemp($file, $destinationPath = null)
	{
		$destinationPath = is_null($destinationPath) ? 'temp' : $destinationPath; // upload path

		$extension = $file->getClientOriginalExtension();
		$fileName = rand(11111,99999).'.'.$extension;

		Image::make($file->getRealPath())->fit(800, 600)->save('uploads/'.$destinationPath.'/'.$fileName);

		return ['file_name' => $fileName];
	}

}