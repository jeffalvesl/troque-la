<?php namespace App\Services\Cache;

use Cache;
use Session;
use Carbon\Carbon;
use App\Repositories\Usuario\UsuarioRepository;

/**
 * Class create cache for the user.
 * @class UserCreateCache
 */
class UserCache
{
	/**
	 * @var UserRepository
	 */
	protected $usuarioRepo;

	/**
	 * @var MenuRepository.
	 */
	protected $menuRepo;

	/**
	 * @var Cache
	 */
	protected $cache;

	public function __construct(UsuarioRepository $usuarioRepo)
	{
		$this->usuarioRepo = $usuarioRepo;
	}

	/**
	 * Create cache for an user with menus and modules.
	 * @param  Array $dataUser Data of the user.
	 * @return bool           true.
	 */
	public function createCache($dataUser)
	{
		return true;
	}
}