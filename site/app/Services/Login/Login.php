<?php namespace App\Services\Login;

use Illuminate\Config\Repository;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Events\Dispatcher;
use App\Services\Cache\UserCache;

/**
 * Class responsible for login and logout of users
 * @author  <Rafael Franke> rafaelfrankean@gmail.com
 */
class Login
{
	/**
	 * @var UserRespository
	 */
	protected $userRepo;

	/**
	 * @var Dispatcher
	 */
	protected $eventDispatcher;

	/**
	 * @var Guard
	 */
	protected $auth;

	/**
	 * @var Respository
	 */
	protected $config;

	/**
	 * @var UserCreateCache
	 */
	protected $userCache;



	public function __construct(Dispatcher $eventDispatcher, Repository $config, Guard $auth)
	{
		$this->eventDispatcher = $eventDispatcher;
		$this->config = $config;
		$this->auth = $auth;
	}

	/**
	 * Method to do login of an user.
	 * @param  array  $credentials  Crenditials of an user like email and password.
	 * @param  boolean $rememberToken boolean to know if user want remember your password.
	 * @return boolean                 true|false.
	 */
	public function login($credentials, $rememberToken = false)
	{
		$logged = $this->auth->attempt($credentials, (bool)$rememberToken);

		if($logged)
		{
			$this->eventDispatcher->fire('usuario.login', $this->auth->user());
		}

		return $logged;
	}

	/**
	 * Faz login de um usuário com id.
	 * @param int $id id do usuário
	 * @param boolean $rememberToken
	 * @return bool true|false.
	 */
	public function loginWithId($id, $rememberToken = false)
	{
		$logged = $this->auth->loginUsingId($id, (bool)$rememberToken);

		if($logged)
		{
			$this->eventDispatcher->fire('usuario.login', $this->auth->user());
		}

		return $logged;
	}


}