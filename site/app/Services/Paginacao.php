<?php namespace App\Services;

use Illuminate\Pagination\BootstrapThreePresenter;
use URL;
use Input;
class Paginacao extends BootstrapThreePresenter
{
    public function getActivePageWrapper($text)
    {
        return '<li class="active"><span>' . $text . '</span></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><a href="#">' . $text . '</a></li>';
    }

    public function getAvailablePageWrapper($url, $page, $rel = null)
    {
        $url = str_replace('/?', '?', $url);

        #dd(Input::all());
        if(!is_null(Input::get('termo')))
        {
            foreach(Input::all() as $key => $param)
            {
                if($key == 'termo')
                {
                    $url = $url . '&' . $key.'='.$param;
                }
            }
        }

        return '<li><a href="' . $url . '">' . $page . '</a></li>';
    }

    public function render()
    {
        if ($this->hasPages()) {
            return sprintf(
                '<ul class="pagination pagination-sm no-margin pull-right">%s %s %s</ul>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            );
        }

        return '';
    }
}