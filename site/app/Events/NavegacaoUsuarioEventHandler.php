<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Auth;

class NavegacaoUsuarioEventHandler extends Event {

	#use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	public function onProdutosDetalhe($event)
	{
		dd(Auth::user());
	}

	public function onProdutosIndex($event)
	{
		dd(Auth::user());
	}


	public function subscribe($events)
	{
		$events->listen('produtos.detalhe', 'App\Events\NavegacaoUsuarioEventHandler@onProdutosDetalhe');
		$events->listen('produtos.index', 'App\Events\NavegacaoUsuarioEventHandler@onProdutosIndex');
	}


}
