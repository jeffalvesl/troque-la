<?php

use Illuminate\Pagination\LengthAwarePaginator;

function paginar(LengthAwarePaginator $dados)
{	
    return with(new App\Services\Paginacao($dados))->render();
}