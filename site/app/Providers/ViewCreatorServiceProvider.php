<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewCreatorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make('view')->creator('partials.menu_categorias', 'App\Http\ViewCreators\Categorias');
        $this->app->make('view')->creator('partials.menu_usuario_logado', 'App\Http\ViewCreators\MenuUsuarioLogado');
        $this->app->make('view')->creator('partials.destaques_menu', 'App\Http\ViewCreators\Destaque');
        $this->app->make('view')->creator('partials.social', 'App\Http\ViewCreators\RedesSociais');
        $this->app->make('view')->creator('partials.cidades', 'App\Http\ViewCreators\Cidades');
        $this->app->make('view')->creator('partials.novidades', 'App\Http\ViewCreators\Novidades');
    }


    public function register()
    {

    }

}