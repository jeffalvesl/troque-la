<?php namespace App\Handlers\Events;

use App\Events\EventNavegacaoUsuario;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class GravaLogNavegacao implements ShouldBeQueued {

	use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  EventNavegacaoUsuario  $event
	 * @return void
	 */
	public function handle(NavegacaoUsuarioEventHandler $event)
	{
		dd($event);
	}

}
