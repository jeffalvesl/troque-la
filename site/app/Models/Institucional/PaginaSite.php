<?php namespace App\Models\Institucional;

use Eloquent;

class PaginaSite extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pagina_site';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['pagina', 'rota_laravel', 'slug'];


}
