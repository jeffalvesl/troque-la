<?php namespace App\Models\Institucional;

use Eloquent;

class Institucional extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'institucional';

	protected $primaryKey = 'id_institucional';

}
