<?php namespace App\Models\Institucional;

use App\Models\BaseModel;

class Contato extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contato';


	protected $primaryKey = 'id_institucional';

	protected $fillable = [
		'nome',
		'email',
		'mensagem',
		'id_usuario',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;

}
