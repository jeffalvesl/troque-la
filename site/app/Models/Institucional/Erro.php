<?php namespace App\Models\Institucional;

use App\Models\BaseModel;

class Erro extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'erro';


	protected $primaryKey = 'id_erro';


	protected $fillable = [
		'pagina',
		'print_tela',
		'descricao_erro',
		'id_usuario',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;

}
