<?php namespace App\Models\Institucional;

use Eloquent;

class Social extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'social';

	protected $primaryKey = 'id_social';

}
