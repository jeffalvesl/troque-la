<?php namespace App\Models\Institucional;

use App\Models\BaseModel;

class Ajuda extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ajuda';


	protected $primaryKey = 'id_ajuda';

	protected $fillable = [
		'pergunta',
		'resposta',
		'slug',
		'criado_em',
		'atualizado_em',
		'ativo'
	];

	public $timestamps = true;

}
