<?php namespace App\Models\Institucional;

use Eloquent;

class Termo extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'termo_uso';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['titulo', 'subtitulo', 'termo', 'slug'];


}
