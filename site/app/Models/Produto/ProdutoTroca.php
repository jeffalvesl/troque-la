<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class ProdutoTroca extends BaseModel
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'produto_troca';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_produto_troca';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'criado_em',
        'atualizado_em',
        'id_produto',
        'id_proposta_produto',
        'id_situacao'
    ];

    public $timestamps = true;


    public function situacao()
    {
        return $this->belongsTo('App\Models\Situacao', 'id_situacao');
    }

    public function produto()
    {
        return $this->hasMany('App\Models\Produto\Produto', 'id_produto');
    }

    public function proposta()
    {
        return $this->belongsTo('App\Models\Produto\PropostaProduto', 'id_proposta_produto');
    }

}
