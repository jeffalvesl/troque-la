<?php namespace App\Models\Produto;

use Eloquent;
use Cache;

class Categoria extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categoria';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_categoria';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'categoria',
		'slug',
		'ativo'
	];


	public $timestamps = false;

	/**
	 * Sub categorias desta categoria.
	 */
	public function subCategorias()
	{
		return $this->hasMany('App\Models\Produto\SubCategoria', 'id_categoria');
	}


	public function produtos()
	{
		return $this->hasMany("App\Models\Produto\Produto", 'id_categoria');
	}
}
