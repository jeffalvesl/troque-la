<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class ProdutoPropostaProduto extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produto_proposta_produto';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_produto_proposta_produto';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id_proposta_produto',
		'id_produto',
		'visualizada'
	];

	public $timestamps = false;



	public function produto()
	{
		return $this->belongsTo('App\Models\Produto\Produto', 'id_produto');
	}


	public function proposta()
	{
		return $this->belongsTo('App\Models\Produto\PropostaProduto', 'id_proposta_produto');
	}

}
