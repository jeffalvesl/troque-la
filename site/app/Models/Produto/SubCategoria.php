<?php namespace App\Models\Produto;

use Eloquent;
use Cache;

class SubCategoria extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sub_categoria';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_sub_categoria';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'sub_categoria',
		'id_categoria',
		'slug',
		'ativo'
	];


	public $timestamps = false;


	public function categoria()
	{
		return $this->belongsTo('App\Models\Produto\Categoria', 'id_categoria');
	}


	public function produtos()
	{
		return $this->hasMany('App\Models\Produto\Produto', 'id_sub_categoria');
	}

}
