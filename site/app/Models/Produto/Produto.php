<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use App\Models\BaseModel;
use Cookie;

class Produto extends BaseModel
{

	/**
	 * @var string
	 */
	protected $table = 'produto';

    /**
     * @var string
     */
    protected $primaryKey = 'id_produto';

	/**
	 * @var array
	 */
	protected $fillable = [
		'id_categoria',
		'id_sub_categoria',
		'produto',
		'descricao_produto',
		'data_aquisicao',
		'garantia',
		'tempo_garantia',
		'garantia_extendida',
		'garantia_extendida_tempo',
		'aceita_proposta',
		'trocado',
		'publicado',
		'id_usuario',
		'slug',
		'views'
	];


	public $timestamps = true;

	/**
	 * @param  attribute $dataAquisicao data de aquisição produto.
	 * @return array.
	 */
	public function getDataAquisicaoAttribute($dataAquisicao)
    {
        if($dataAquisicao == '0000-00-00')
        {
            return '';
        }

        $dataAquisicao = str_replace('-', '/', $dataAquisicao);
        $dataAquisicao = date('d/m/Y', strtotime($dataAquisicao));
        $this->attributes['data_aquisicao'] = $dataAquisicao;

        return $this->attributes['data_aquisicao'];
    }

    /**
     * seta data aquisição.
     */
    public function setDataAquisicaoAttribute($dataAquisicao)
    {
    	$this->attributes['data_aquisicao'] = str_replace('/', '-', $dataAquisicao);
        $this->attributes['data_aquisicao'] = date('Y-m-d', strtotime($this->attributes['data_aquisicao']));
    }

    /**
     * Faz uma query buscando produtos de uma cidade.
     * @param  Query $query.
     */
    public function scopeCidade($query)
    {
    	$cidade = Cookie::get('cidade');
    	if(!is_null($cidade))
    	{
    		$query->whereHas('usuario', function($q) use ($cidade){
    			$q->where('id_cidade', $cidade['id_cidade']);
    		});
    	}
    }

    public function scopeProposta($query, $idPropostaProduto)
    {
    	$query->whereHas('propostas', function($q) use ($idPropostaProduto){
    		$q->where('id_proposta_produto', '=', $idPropostaProduto);
    	});
    }

    public function scopeUsuario($query, $idUsuario)
    {
    	$query->whereHas('usuario', function($q) use($idUsuario){
    		$q->where('id_usuario', $idUsuario);
    	});
    }

    /**
     * @return hasMany.
     */
    public function interesses()
    {
    	return $this->hasMany('App\Models\Produto\ProdutoInteresse', 'id_produto');
    }

    /**
     * @return belongsTo.
     */
    public function categoria()
    {
		return $this->belongsTo('App\Models\Produto\Produto', 'id_categoria');
    }

    /**
     * @return belongsTo.
     */
	public function subCategoria()
	{
		return $this->belongsTo('App\Models\Produto\SubCategoria', 'id_sub_categoria');
	}

	/**
	 * @return belongsTo.
	 */
	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario\Usuario', 'id_usuario');
	}

	/**
	 * @return hasMany.
	 */
	public function imagens()
	{
		return $this->hasMany('App\Models\Produto\ImagemProduto', 'id_produto');
	}

	/**
	 * @return hasMany.
	 */
	public function propostas()
	{
		return $this->hasMany('App\Models\Produto\PropostaProduto', 'id_produto');
	}

	/**
	 * @return hasOne.
	 */
	public function troca()
	{
		return $this->hasOne('App\Models\Produto\ProdutoTroca', 'id_produto');
	}


}
