<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use Carbon\Carbon;
use App\Models\BaseModel;

class MensagemPropostaUsuario extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mensagem_proposta_usuario';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_mensagem_proposta_usuario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'mensagem',
		'visualizada',
		'criado_em',
		'atualizado_em',
		'id_proposta_produto',
		'id_usuario'
	];

	public $timestamps = true;


	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario\Usuario', 'id_usuario');
	}

	public function getCriadoEmAttribute()
	{
		$data = '';
		if(date('Y-m-d', strtotime($this->attributes['criado_em'])) == date('Y-m-d'))
		{
			$data = 'Hoje às ' . Carbon::createFromTimestamp(strtotime($this->attributes['criado_em']))->format('H:i');
		}
		else
		{
			$data = 'Em ' . Carbon::createFromTimestamp(strtotime($this->attributes['criado_em']))->format('d/m/Y') . ' às '. Carbon::createFromTimestamp(strtotime($this->attributes['criado_em']))->format('H:i');
		}

		return $data;
	}
}
