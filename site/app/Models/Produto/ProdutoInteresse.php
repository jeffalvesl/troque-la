<?php namespace App\Models\Produto;

use Eloquent;

class ProdutoInteresse extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produto_interesse';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_produto_interesse';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'produto_interesse',
		'descricao_produto',
		'id_produto'
	];

    public $timestamps = false;


    public function produto()
    {
        return $this->belongsTo('App\Models\Produto\Produto', 'id_produto');
    }

}
