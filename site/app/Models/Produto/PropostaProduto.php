<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class PropostaProduto extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'proposta_produto';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_proposta_produto';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'proposta',
		'aceita',
		'visualizada',
		'id_produto',
		'id_usuario',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;


	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario\Usuario', 'id_usuario');
	}

	public function mensagens()
	{
		return $this->hasMany('App\Models\Produto\MensagemPropostaUsuario', 'id_proposta_produto');
	}

	public function troca()
	{
		return $this->hasOne('App\Models\Produto\ProdutoTroca', 'id_proposta_produto');
	}

	public function produtoProposta()
	{
		return $this->hasOne('App\Models\Produto\ProdutoPropostaProduto', 'id_proposta_produto');
	}

	public function produto()
	{
		return $this->belongsTo('App\Models\Produto\Produto', 'id_produto');
	}


}
