<?php namespace App\Models\Produto;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class ImagemProduto extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'imagem_produto';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_imagem_produto';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id_produto',
		'imagem',
		'legenda',
		'miniatura'
	];


	public $timestamps = false;

}
