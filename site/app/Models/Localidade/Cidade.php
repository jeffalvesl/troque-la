<?php namespace App\Models\Localidade;

use Eloquent;
use Cache;

class Cidade extends Eloquent
{

	/**
	 * Tabela deste model.
	 *
	 * @var string
	 */
	protected $table = 'cidade';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_cidade';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'cidade',
		'id_estado'
	];


	public $timestamps = false;


	public function estado()
	{
		return $this->belongsTo('App\Models\Localidade\Estado', 'id_estado');
	}

}
