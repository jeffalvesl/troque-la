<?php namespace App\Models\Localidade;

use Eloquent;
use Cache;

class Estado extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'estado';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_estado';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'estado',
		'sigla'
	];


	public $timestamps = false;

}
