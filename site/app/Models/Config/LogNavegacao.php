<?php namespace App\Models\Config;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class LogNavegacao extends BaseModel
{

	/**
	 * Tabela deste model.
	 *
	 * @var string
	 */
	protected $table = 'log_navegacao';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_log_navegacao';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'log_navegacao',
		'url_conteudo',
		'tipo',
		'titulo_pagina',
		'url_anterior',
		'email',
		'usuario',
		'id_usuario',
		'criado_em',
		'atualizado_em'
	];


	public $timestamps = true;


	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario\Usuario', 'id_usuario');
	}

}
