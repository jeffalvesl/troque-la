<?php namespace App\Models\Config;

use App\Models\BaseModel;

class LogAcessoSite extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'log_acesso_site';


	protected $primaryKey = 'id_log_acesso_site';

	protected $fillable = [
		'url_referencia',
		'token',
		'session',
		'cadastrou',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;

}
