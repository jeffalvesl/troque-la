<?php namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Cache;

class BaseModel extends Eloquent
{

	const CREATED_AT = 'criado_em';

	const UPDATED_AT = 'atualizado_em';




	public function getCriadoEmAttribute()
	{
		return Carbon::createFromTimestamp(strtotime($this->attributes['criado_em']))->format('d/m/Y');
	}

	public function getAtualizadoEmAttribute()
	{
		return Carbon::createFromTimestamp(strtotime($this->attributes['atualizado_em']))->format('d/m/Y');
	}
}
