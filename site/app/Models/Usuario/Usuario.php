<?php namespace App\Models\Usuario;

use Eloquent;
use Cache;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Usuario extends Eloquent implements AuthenticatableContract, CanResetPasswordContract
{

	use Authenticatable, CanResetPassword;

	/**
	 * @var string
	 */
	protected $table = 'usuario';

    /**
     * @var string
     */
    protected $primaryKey = 'id_usuario';

	/**
	 * @var array
	 */
	protected $fillable = [
		'usuario',
		'nome',
		'sobrenome',
		'id_cidade',
		'email',
        'confirmou_email',
		'primeiro_acesso',
		'senha',
		'id_facebook',
		'cep',
		'endereco',
        'numero',
		'bairro',
		'complemento',
        'sexo',
        'data_nascimento',
        'telefone',
        'celular',
        'ativo',
        'motivo_cancelamento'
	];

	protected $hidden = ['senha'];

	public $timestamps = false;




    /**
     * Formata data de nascimento de um usuário.
     * @param  array $dataNascimento.
     * @return array
     */
    public function getDataNascimentoAttribute($dataNascimento)
    {
        if($this->attributes['data_nascimento'] == '0000-00-00')
        {
            return '';
        }

        $this->attributes['data_nascimento'] = str_replace('-', '/', $dataNascimento);
        $this->attributes['data_nascimento'] = date('d/m/Y', strtotime($this->attributes['data_nascimento']));

        return $this->attributes['data_nascimento'];
    }

    /**
     * Formata data de nascimento para salvar no banco.
     */
    public function setDataNascimentoAttribute($dataNascimento)
    {
        $dataNascimento = str_replace('/', '-', $dataNascimento);
        $dataNascimento = date('Y-m-d', strtotime($dataNascimento));
        $this->attributes['data_nascimento'] = $dataNascimento;
    }

    /**
     * @return belongsTo.
     */
    public function cidade()
    {
        return $this->belongsTo('App\Models\Localidade\Cidade', 'id_cidade');
    }

    /**
     * @return hasMany.
     */
    public function interesses()
    {
        return $this->hasMany('App\Models\Usuario\InteresseUsuario', 'id_usuario');
    }

    /**
     * @return hasMany.
     */
    public function produtoInteresse()
    {
        return $this->hasMany('App\Models\Usuario\ProdutoInteresseUsuario', 'id_usuario')->where('visualizado', 0);
    }

    /**
     * @return hasMany.
     */
    public function produtos()
    {
        return $this->hasMany('App\Models\Produto\Produto', 'id_usuario');
    }

    /**
     * Retorna o identificador Ãnico deste usuÃ¡rio.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Retorna a senha deste usuÃ¡rio.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->attributes['senha'];
    }

    /**
     * Retorna o e-mail para o caso de ter esquecido a senha poder recuperÃ¡-la.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->attributes['email'];
    }

    /**
     * Retorna o nome do campo que serÃ¡ utilizado para o "Manter conectado" quando o usuÃ¡rio fizer login.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'logado_token';
    }


}
