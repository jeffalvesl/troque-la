<?php namespace App\Models\Usuario;

use Eloquent;
use Cache;
use App\Models\BaseModel;

class NotificacaoUsuario extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notificacao_usuario';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_notificacao_usuario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'notificacao_usuario',
		'url_conteudo',
		'visualizada',
		'criado_em',
		'atualizado_em',
		'id_usuario'
	];

	public $timestamps = true;



}
