<?php namespace App\Models\Usuario;

use Eloquent;

class InteresseUsuario extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'interesse_usuario';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id_interesse_usuario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id_sub_categoria',
		'id_usuario'
	];

    public $timestamps = false;


    public function subCategoria()
    {
        return $this->belongsTo('App\Models\Produto\SubCategoria', 'id_sub_categoria');
    }

}
