<?php namespace App\Models\Usuario;

use App\Models\BaseModel;

class RecuperarSenhaHash extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'recuperar_senha_hash';


	protected $primaryKey = 'id_recuperar_senha_hash';


	protected $fillable = [
		'hash',
		'valida',
		'id_usuario',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;

	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario\Usuario', 'id_usuario');
	}

}
