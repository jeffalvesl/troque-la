<?php namespace App\Models\Usuario;

use App\Models\BaseModel;

class ProdutoInteresseUsuario extends BaseModel
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produto_interesse_usuario';


	protected $primaryKey = 'id_produto_interesse_usuario';

	protected $fillable = [
		'id_produto',
		'id_log_navegacao',
		'id_usuario',
		'visualizado',
		'criado_em',
		'atualizado_em'
	];

	public $timestamps = true;



	public function produto()
	{
		return $this->belongsTo('App\Models\Produto\Produto', 'id_produto');
	}

}
