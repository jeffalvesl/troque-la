<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Repositories\Produto\ProdutoRepository;

class Teste extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'teste';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Teste de comando';


	public function __construct(ProdutoRepository $produtoRepo)
	{
		parent::__construct();
		$this->produtoRepo = $produtoRepo;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$produtos = $this->produtoRepo->find(1);

	}

}
