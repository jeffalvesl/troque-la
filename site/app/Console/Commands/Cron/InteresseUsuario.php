<?php namespace App\Console\Commands\Cron;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Repositories\Produto\ProdutoRepository;
use App\Services\Cron\ProdutoInteresseUsuario;

class InteresseUsuario extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'interesse_usuario';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Separa produtos de acordo com a navegação do usuário';


	public function __construct(ProdutoInteresseUsuario $produtoInteresseUsuario)
	{
		parent::__construct();
		$this->produtoInteresseUsuario = $produtoInteresseUsuario;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->produtoInteresseUsuario->executar();
	}

}
