<?php namespace App\Repositories\Localidade;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* @package EstadoRepository.
*/
class EstadoRepository extends BaseRepository
{
	
	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Localidade\Estado';
	}

	/**
	 * Verifica se um estado existe.
	 * @param  string $estado sigla do estado.
	 * @return retorna o estado existente ou cria um novo e retorna o mesmo.
	 */
	public function estadoExiste($estado)
	{
		$estadoExiste = $this->findByField('sigla', $estado)->first();
		if(!$estadoExiste)
		{
			return $this->novoEstado(['estado' => $estado, 'sigla' => $estado]);	
		}

		return $estadoExiste;
	}

	/**
	 * Cria um novo estado.
	 * @param  array  $estado dados do estado como nome e URF
	 * @return O novo estado adicionado.
	 */
	public function novoEstado(array $estado)
	{
		return $this->create($estado);
	}
}