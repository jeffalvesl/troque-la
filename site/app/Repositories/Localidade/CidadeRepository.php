<?php namespace App\Repositories\Localidade;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* 
*/
class CidadeRepository extends BaseRepository
{
	
	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Localidade\Cidade';
	}

	
	/**
	 * Verifica se uma cidade existe no banco de dados.
	 * @param  int $estado id do estado.
	 * @param  string $cidade cidade daquele estado. 
	 * @return retorna os dados da cidade existente.
	 */
	public function cidadeExiste($estado, $cidade)
	{
		$cidadeExiste = $this->makeModel()->where('id_estado', $estado)->where('cidade', $cidade)->first();
		if(!$cidadeExiste)
		{
			return $this->novaCidade(['id_estado' => $estado, 'cidade' => $cidade]);	
		}

		return $cidadeExiste;
	}

	/**
	 * Adiciona uma nova cidade.
	 * @param  array  $cidade dados da cidade e o estado
	 * @return bool         true|false
	 */
	public function novaCidade(array $cidade)
	{
		return $this->create($cidade);
	}

}