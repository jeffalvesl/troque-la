<?php namespace App\Repositories\Institucional;

use Prettus\Repository\Eloquent\BaseRepository;

/**
* @package TermoRepository.
*/
class TermoRepository extends BaseRepository
{
	
	public function model()
	{
		return 'App\Models\Institucional\Termo';
	}

	/**
	 * Retorna o termo de uso do site.
	 */
	public function getTermo()
	{
		return $this->findByField('slug', 'termos-de-uso')->first();
	}
}