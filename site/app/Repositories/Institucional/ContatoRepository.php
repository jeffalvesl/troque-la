<?php namespace App\Repositories\Institucional;

use Prettus\Repository\Eloquent\BaseRepository;

/**
* @package ContatoRepository
*/
class ContatoRepository extends BaseRepository
{
	
	public function model()
	{
		return 'App\Models\Institucional\Contato';
	}

}