<?php namespace App\Repositories\Institucional;

use Prettus\Repository\Eloquent\BaseRepository;

/**
* Classe responsável por controlar o institucional do site.
* @package InstitucionalRepository
*/
class InstitucionalRepository extends BaseRepository
{
	
	public function model()
	{
		return 'App\Models\Institucional\Institucional';
	}

	/**
	 * Retorna institucional pela slug.
	 * @param string $slug - Slug da página[termos, sobre, contato]
	 * @return Institucional.
	 */
	public function retornaInstitucional($slug)
	{
		return $this->findByField('slug', $slug)->first();
	}
}