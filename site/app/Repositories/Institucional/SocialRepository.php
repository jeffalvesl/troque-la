<?php namespace App\Repositories\Institucional;

use Prettus\Repository\Eloquent\BaseRepository;

/**
* Classe responsável por controlar o institucional do site.
* @package InstitucionalRepository
*/
class SocialRepository extends BaseRepository
{

	public function model()
	{
		return 'App\Models\Institucional\Social';
	}


	public function retornarPorCondicao(array $condicao)
	{
		if(count($condicao) == 0)
		{
			return $this->makeModel()->get();
		}

		$social = $this->makeModel();
		$social = $social->where($condicao)->get();

		return $social;
	}
}