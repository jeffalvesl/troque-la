<?php namespace App\Repositories\Config;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* @package LogNavegacaoRepository.
*/
class LogNavegacaoRepository extends BaseRepository
{
	protected $tipo = [
		0 => 'visualizacao',
		1 => 'proposta'
	];

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Config\LogNavegacao';
	}

	/**
	 * Cria um novo log de nevagação.
	 * @param  array  $dados dados do log.
	 * @param  string $tipo  tipo do log.
	 * @return bool true
	 *
	 */
	public function log(array $dados)
	{
		return $this->create($dados);
	}


}