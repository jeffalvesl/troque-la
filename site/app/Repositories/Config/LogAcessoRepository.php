<?php namespace App\Repositories\Config;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* @package CategoriaRepository.
*/
class LogAcessoRepository extends BaseRepository
{

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Config\LogAcessoSite';
	}

}