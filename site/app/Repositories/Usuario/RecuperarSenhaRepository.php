<?php namespace App\Repositories\Usuario;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* @package CategoriaRepository.
*/
class RecuperarSenhaRepository extends BaseRepository
{
	
	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Usuario\RecuperarSenhaHash';
	}

}