<?php namespace App\Repositories\Usuario;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

class ProdutoInteresseUsuarioRepository extends BaseRepository
{

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Usuario\ProdutoInteresseUsuario';
	}


	public function produtos(array $condicao, array $with)
	{
		$produtos = $this->with($with)->makeModel()->where($condicao)->get();

		return $produtos;
	}

}