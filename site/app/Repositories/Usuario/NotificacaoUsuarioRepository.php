<?php namespace App\Repositories\Usuario;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Services\Email\EmailService;

/**
* @package ProdutoRepository.
*/
class NotificacaoUsuarioRepository extends BaseRepository
{
	public $mensagem = array(
		'proposta' => '%s fez uma proposta por seu produto.',
		'mensagem' => 'Você recebeu uma mensagem de %s',
		'resposta' => '%s respondeu sua mensagem sobre ',
		'aceitou_proposta' => '%s aceitou sua proposta em um produto',
		'confirmar_troca' => '%s confirmou a troca de produtos com você.',
		'troca_realizada_1' => '%s confirmou recebimento do produto',
		'troca_realizada_2' => '%s confirmou recebimento do produto',
		'cancelada' => '%s cancelou a troca de produtos. Entre em contato para mais informações'
	);

	public function __construct(Application $app, EmailService $emailService)
	{
		parent::__construct($app);
		$this->emailService = $emailService;
	}

	/**
	 * Model deste repository.
	 */
	public function model()
	{
		return 'App\Models\Usuario\NotificacaoUsuario';
	}

	/**
	 * Cria uma nova notificação para um usuário.
	 * @param  array  $notificacao - dados da noficicação.
	 * @param  int $usuario     - usuário destino da notificação.
	 * @param  string $tipo     - tipo da notificação - mensagem, resposta, proposta
	 * @return bool true|false.
	 */
	public function criarNovaNotificacao(array $notificacao, $usuario, $tipo)
	{
		$notificacao['notificacao_usuario'] = sprintf($this->mensagem[$tipo], $usuario);

		return $this->create($notificacao);
	}

	/**
	 * Atualiza dados da notificação através de uma condição.
	 * @param  array  $condicoes condições para atualizar um registro.
	 * @param  array  $dados     novos dados do registro.
	 * @return Boolean true - false.
	 */
	public function atualizar(array $condicoes, array $dados)
	{
		$atualizou = $this->makeModel()->where($condicoes)->update($dados);

		return $atualizou;
	}

	/**
	 * Envia email para usuário dono do produto que foi feita a proposta.
	 * @param  array  $dados dados do email
	 * @return bool true|false.
	 */
	public function enviarEmail(array $dados, $template = 'emails.proposta_troca')
	{
		return $this->emailService->send($dados, $template);
	}

	/**
	 * Retornas as notificações de um usuário.
	 * @param  int  $idUsuario   - usuário logado.
	 * @param  integer $qnt         quantidade de notificações.
	 * @param  boolean $tratarDados - se for ajax essa variável será verdadeira
	 * @param  boolean $visualizada - com todas as visualizadas.
	 * @return array|collection.
	 */
	public function retornaNotificacao($idUsuario, $qnt = 6, $tratarDados = false, $visualizada = false)
	{
		$notificacoes = $this->makeModel()->where('id_usuario', $idUsuario);
		if($visualizada == 1)
		{
			$notificacoes->where('visualizada', 1);
		}
		elseif($visualizada == 0)
		{
			$notificacoes->where('visualizada', 0);
		}

		$notificacoes = $notificacoes->take($qnt)->get();

		if($tratarDados)
		{
			$notificacoes = $this->tratarDados($notificacoes);
		}

		return $notificacoes;
	}

	/**
	 * Trata os dados da notificação.
	 * @param  array $dados - notificações achadas no banco de dados.
	 * @return array com todas as notificações.
	 */
	private function tratarDados($dados)
	{
		$dadosRetorno = [];
		$total = 0;
		foreach($dados as $dado)
		{
			$dadosRetorno[] = [
				'id_notificacao_usuario' => $dado->id_notificacao_usuario,
				'url_conteudo' => $dado->url_conteudo,
				'notificacao_usuario' => $dado->notificacao_usuario,
				'data_hora' => $dado->criado_em
			];
			$total++;
		}

		$dadosRetorno['total'] = $total;
		return $dadosRetorno;
	}

}