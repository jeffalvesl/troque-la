<?php namespace App\Repositories\Usuario;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Usuario\InteresseUsuarioRepository;
use App\Services\Login\Login;
use Hash;
use Carbon\Carbon;

/**
* Classe por controlar toda a lógica referente ao usuário.
* @package UsuarioRepository.
*/
class UsuarioRepository extends BaseRepository
{

	public function __construct(Login $loginService, Application $app, InteresseUsuarioRepository $interesseUsuarioRepo)
	{
		parent::__construct($app);
		$this->loginService = $loginService;
		$this->interesseUsuarioRepo = $interesseUsuarioRepo;
	}

	public function model()
	{
		return 'App\Models\Usuario\Usuario';
	}

	/**
	 * Cadastra um novo usuário.
	 * @param  array  $dados       dados do novo usuário.
	 * @param  array $credenciais  credenciais
	 * @return bool|array              Array com dados caso erro ou true caso seja cadastrado e logado.
	 */
	public function cadastrar(array $dados, $credenciais)
	{
		$dados['senha'] = Hash::make($dados['password']);
		$dados['primeiro_acesso'] = 1;

		$cadastrou = $this->create($dados);

		$logou = $this->loginService->loginWithId($cadastrou->id_usuario); 

		if($logou)
		{
			return true;
		}

		return ['dados' => $dados, 'retorno' => false];
	}

	/**
	 * Retorna informações de um usuário.
	 * @param  int  $idUsuario id do usuário
	 * @param  boolean $logado    se é os dados do usuário logado ou nao.
	 * @return Usuario
	 */
	public function getUsuario($idUsuario, $logado = false, array $with)
	{
		if($logado)
		{
			if(count($with) > 0)
			{
				return $this->with($with)->findByField('id_usuario', $idUsuario)->first();
			}

			return $this->findByField('id_usuario', $idUsuario)->first();
		}
	}

	/**
	 * Atualiza dados do usuário.
	 * @param  array  $dados     dados do usuário.
	 * @param  int $idUsuario id do usuário a ser atualizado
	 * @return bool|array Se atualizou retorna true se é primeiro acesso | false se deu erro.
	 */
	public function atualizarDados(array $dados, $idUsuario)
	{
		$atualizou = $this->update($dados, $idUsuario);
		if(isset($dados['interesses']))
		{
			$this->interesseUsuarioRepo->atualizaInteresses($dados['interesses'], $idUsuario);
		}

		if($atualizou)
		{
			return ['atualizou' => true, 'primeiro_acesso' => $atualizou->primeiro_acesso];
		}

		return false;
	}


	public function retornaUsuarioPorCampo($campo, $valor, $colunas = array('*'))
	{
		return $this->findByField($campo, $valor, $colunas)->first();
	}

}