<?php namespace App\Repositories\Usuario;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

/**
* 
*/
class InteresseUsuarioRepository extends BaseRepository
{
	
	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Usuario\InteresseUsuario';
	}

	/**
	 * Retorna os interesses do usuário.
	 * @param  id usuario $idUsuario interesses deste usuário. 
	 * @return interesses.
	 */
	public function retornaInteressesUsuario($idUsuario)
	{
		$interesses = $this->with(['subCategoria.categoria'])->makeModel()->where('id_usuario', $idUsuario)->get();

		return $interesses;
	}


	/**
	 * Atualiza os interesses de um usuário.
	 * @param  array  $interesses novos interesses
	 * @param  int $idUsuario  usuário no qual criaremos os interesses.
	 * @return bool true.
	 */
	public function atualizaInteresses(array $interesses, $idUsuario)
	{
		$this->excluiInteresses($idUsuario);

		foreach($interesses as $interesse)
		{
			$interesseUsuario = ['id_sub_categoria' => (int)$interesse, 'id_usuario' => (int)$idUsuario];
			$this->create($interesseUsuario);
		}
		

		return true;
	}

	/**
	 * Exclui os interesses de um usuário.
	 * @param  int $idUsuario id do usuário.
	 * @return bool true|false.
	 */
	private function excluiInteresses($idUsuario)
	{
		return $this->makeModel()->where('id_usuario', $idUsuario)->delete();
	}

	/**
	 * Pesquisa interesses para um usuário.
	 * @param  string $termo   - Termo que o usuário pesquisar.
	 * @param  int $idUsuario - interesses que não são deste id_usuario.
	 * @return array            interesses encontrados.
	 */
	public function pesquisarInteresses($termo, $idUsuario = null)
	{
		$interesses = $this->with(['subCategoria.produtos'])->makeModel()->whereHas('subCategoria', function($query) use ($termo, $idUsuario){
			$query->whereHas('produtos', function($q) use ($termo, $idUsuario){
				$q->where('produto', 'LIKE', '%'.$termo.'%');
				$q->whereNotIn('id_usuario', [$idUsuario]);
			});
		})->get();

		$dados = [];

		foreach($interesses as $key => $interesse)
		{
			if(count($interesse->subCategoria) > 0)
			{
				$dados[$key] = ['id' => $interesse->subCategoria->id_sub_categoria, 'text' => $interesse->subCategoria->sub_categoria];
				foreach($interesse->subCategoria->produtos as $produto)
				{
					$dados[$key]['children'][] = ['id' => $produto->id_produto, 'text' => $produto->produto];
				}
			}
		}

		return $dados;
	}


}