<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

/**
* @package ProdutoTrocaRepository.
*/
class ProdutoTrocaRepository extends BaseRepository
{
	/**
	 * Situações possíveis.
	 */
	public $situacao = array(
		'aguardando-confirmacao' => 1,
		'troca-em-andamento' => 2,
		'realizada' => 3 ,
		'cancelada' => 4,
		'aguardando-recebimento' => 5
	);

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	/**
	 * Model deste repository.
	 */
	public function model()
	{
		return 'App\Models\Produto\ProdutoTroca';
	}

	/**
	 * Um produto ele será considerado trocado depois que o ambos usuários receberem o mesmo.
	 * Enquanto isso, o produto estará em situação 1, 2, 3 ou 4
	 * @param array $dados - dados da troca.
	 */
	public function novaTroca(array $dados)
	{
		$dados['id_situacao'] = $this->situacao['aguardando-confirmacao'];

		return $this->create($dados);
	}


	public function atualizar(array $dados, $idProdutoTroca, $situacao = null)
	{
		$dados['id_situacao'] = $this->situacao[$situacao];

		return $this->update($dados, $idProdutoTroca);
	}

}