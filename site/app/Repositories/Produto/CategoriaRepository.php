<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Hash;

/**
* @package CategoriaRepository.
*/
class CategoriaRepository extends BaseRepository
{

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Produto\Categoria';
	}

	/**
	 * Retorna categorias e caso o $with seja não nulo ele trará também as subcategorias.
	 * @param  array $with relações do model.
	 * @return categorias com ou sem subcategorias.
	 */
	public function retornaCategorias($with = null)
	{
		if(is_null($with))
		{
			return $this->findByField('ativo', 1)->all();
		}

		if(in_array('produtos', $with))
		{
			return $this->with($with)->makeModel()
					->whereHas('produtos', function($query){
						$query->where('trocado', 0);
						$query->where('publicado', 1);
					})->get();
		}

		return $this->with($with)->all();
	}

	/**
	 * Faz pesquisas de uma categoria informada pelo usuário..
	 * @param  string $termo categoria procurada pelo usuário.
	 * @return todas as categorias encontradas.
	 */
	public function pesquisarCategoria($termo)
	{
		$categorias = $this->with(['subCategorias'])->makeModel()
		->where('categoria', 'LIKE', '%'.$termo.'%')
		->whereHas('subCategorias', function($query) use ($termo){
			$query->orWhere('sub_categoria', 'LIKE', '%' . $termo . '%');
		})
		->get();

		$dados = [];

		foreach($categorias as $key => $categoria)
		{
			if(count($categoria->subCategorias) > 0)
			{
				$dados[$key] = ['id' => $categoria->id_categoria, 'text' => $categoria->categoria];
				foreach($categoria->subCategorias as $subCategoria)
				{
					$dados[$key]['children'][] = ['id' => $subCategoria->id_sub_categoria, 'text' => $subCategoria->sub_categoria];
				}
			}
		}

		return $dados;
	}

	/**
	 * Coloca todos os dados em um array.
	 *
	 * @param array $categorias
	 * @return array.
	 */
	public function tratarDados($categorias)
	{
		$dados = [];
		foreach($categorias as $categoria)
		{
			if(count($categoria->subCategorias) > 0)
			{
				foreach($categoria->subCategorias as $subCategoria)
				{
					$dados[$subCategoria->id_sub_categoria] = $subCategoria->sub_categoria;
				}
			}
		}

		return $dados;
	}

}