<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Services\Email\EmailService;

/**
* @package PropostaProdutoRepository.
*/
class PropostaProdutoRepository extends BaseRepository
{

	public function __construct(Application $app, EmailService $emailService)
	{
		parent::__construct($app);
		$this->emailService = $emailService;
	}

	/**
	 * Model deste repository.
	 */
	public function model()
	{
		return 'App\Models\Produto\PropostaProduto';
	}

	/**
	 * Retorna todoas as propostas de um produto e um usuário.
	 * @param  int $idProduto - produto em questão.
	 * @param  int $idUsuario - usuário logado.
	 * @return return collection.
	 */
	public function retornaPropostas($idProduto, $idUsuario)
	{
		return $this->findWhere(['id_produto' => $idProduto, 'id_usuario' => $idUsuario])->first();
	}


	public function propostas(array $condicoes, $with = null)
	{
		$propostas = $this->makeModel();
		if(!is_null($with))
			$propostas->with($with);

		$propostas = $propostas->where($condicoes)->get();

		return $propostas;
	}

	/**
	 * Atualiza proposta.
	 * @param  array  $dados             - dados da proposta.
	 * @param  id_proposta $idPropostaProduto - id_proposta
	 * @return bool true|false.
	 */
	public function atualizar(array $dados, $idPropostaProduto)
	{
		return $this->update($dados, $idPropostaProduto);
	}

	/**
	 * Envia um email para o usuário.
	 * @param  array  $dados [description]
	 * @return [type]        [description]
	 */
	public function enviarEmailUsuarioDestino(array $dados)
	{
		$this->emailService->send($dados, 'emails.proposta_troca');
	}

}