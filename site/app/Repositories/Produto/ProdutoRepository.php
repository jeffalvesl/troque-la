<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Produto\SubCategoriaRepository;
use App\Repositories\Produto\ProdutoInteresseRepository;
use App\Repositories\Produto\CategoriaRepository;
use App\Repositories\Usuario\InteresseUsuarioRepository;
use DB;

/**
* @package ProdutoRepository.
*/
class ProdutoRepository extends BaseRepository
{

	public function __construct(
		Application $app,
		SubCategoriaRepository $subCategoriaRepo,
		ProdutoInteresseRepository $produtoInteresseRepo,
		InteresseUsuarioRepository $interesseUsuarioRepo
	)
	{
		parent::__construct($app);
		$this->subCategoriaRepo = $subCategoriaRepo;
		$this->produtoInteresseRepo = $produtoInteresseRepo;
		$this->interesseUsuarioRepo = $interesseUsuarioRepo;
	}

	/**
	 * Model deste repository.
	 */
	public function model()
	{
		return 'App\Models\Produto\Produto';
	}

	/**
	 * Retorna produtos de um usuário.
	 */
	public function retornaProdutos($usuario = null, $trocado = null)
	{
		if(!is_null($usuario))
		{
			$produtos = $this->with(['usuario', 'imagens', 'interesses', 'troca', 'propostas'])
								->makeModel()
								->where('id_usuario', $usuario);

			if(!is_null($trocado))
			{
				return $produtos->where('trocado', $trocado)->get();
			}

			return $produtos->orderBy('criado_em', 'desc')->get();
		}

		return $this->makeModel()->orderBy('criado_em', 'DESC')->get();
	}


	public function buscarProdutosUsuario($idUsuario, $termo = null)
	{
		$produtos = $this->with(['subCategoria'])->makeModel()->where('id_usuario', $idUsuario);
		if(!is_null($termo))
		{
			$produtos = $produtos->where('produto', 'LIKE', '%'.$termo.'%');
		}

		$produtos = $produtos->get();

		$dados = [];

		foreach($produtos as $key => $produto)
		{

			$dados[$key] = ['id' => $produto->subCategoria->id_sub_categoria, 'text' => $produto->subCategoria->sub_categoria];	
			$dados[$key]['children'][] = ['id' => $produto->id_produto, 'text' => $produto->produto];
		}

		return $dados;
	}

	public function retornaNovidades($cidade = null)
	{
		$novidades = $this->with(['imagens', 'usuario'])
					->makeModel()
					->cidade()
					->where('publicado', 1)
					->where('trocado', 0);

		if(!is_null($cidade))
		{
			$novidades->whereHas('usuario', function($query) use ($cidade){
				$query->where('id_cidade', $cidade);
			});
		}

		return $novidades->take(6)->orderBy('criado_em', 'DESC')->get();
	}

	/**
	 * Retorna todos os produtos para uma condição.
	 * @param  array   $condicoes     condições para o retorno dos produtos.
	 * @param  boolean $pegaPorCidade se for pegar por cidade nós passaremos true.
	 * @param  integer $qnt           quantidade de registros.
	 * @param  string  $orderBy       ordenação por desc ou asc
	 * @return Produto.
	 */
	public function retornaProdutosPorCondicao(array $condicoes, $pegaPorCidade = true, $qnt = 9, $orderBy = 'desc')
	{
		$produtos = $this->makeModel();

		if($pegaPorCidade)
			$produtos->cidade();

		$produtos = $produtos->where($condicoes)->take($qnt)->orderBy('criado_em', $orderBy)->get();

		return $produtos;
	}

	/**
	 * Cria um novo produto para o usuário.
	 * @param array $dados - Dados do novo produto.
	 * @return Model Produto.
	 */
	public function criarNovoProduto(array $dados)
	{

		$categoria = $this->subCategoriaRepo->with(['categoria'])->find($dados['id_sub_categoria'])->first();
		$dados['id_categoria'] = $categoria->id_categoria;

		$produto = $this->create($dados);
		if($produto)
		{
			if(isset($dados['produtos_interesse']))
			{
				$this->cadastraInteresseTroca($dados['produtos_interesse'], $produto->id_produto);
			}

			return $produto;
		}

		return ['dados' => $produto, 'criou' => false];
	}

	/**
	 * Cadastra produto de interesses para troca.
	 * @param  array  $interesses - interesses de trocas.
	 * @param  int $idProduto  - id do produto.
	 */
	private function cadastraInteresseTroca(array $interesses, $idProduto)
	{
		$this->produtoInteresseRepo->makeModel()->where('id_produto', $idProduto)->delete();

		foreach($interesses as $interesse)
		{
			$produtoInteresse = $this->findByField('id_produto', $interesse)->first();
			if($produtoInteresse)
			{
				$this->produtoInteresseRepo->create([
					'produto_interesse' => $produtoInteresse->produto,
					'descricao_produto' => $produtoInteresse->produto,
					'id_produto' => $idProduto
				]);
			}
			else
			{
				$this->produtoInteresseRepo->create([
					'produto_interesse' => $interesse,
					'descricao_produto' => $interesse,
					'id_produto' => $idProduto
				]);
			}
		}
	}

	/**
	 * Atualiza um produto.
	 * @param array $dados - Dados do produto.
	 * @param int $idProduto - id do produto a ser atualizado.
	 * @return Model|Array Model de produto se atualizado ou array se ouve erro.
	 */
	public function atualizar(array $dados, $idProduto)
	{
		if(isset($dados['id_sub_categoria']))
		{
			$categoria = $this->subCategoriaRepo->with(['categoria'])->find($dados['id_sub_categoria'])->first();
			$dados['id_categoria'] = $categoria->id_categoria;
		}

		$produto = $this->update($dados, $idProduto);
		if($produto)
		{
			if(isset($dados['produtos_interesse']))
			{

				$this->cadastraInteresseTroca($dados['produtos_interesse'], $produto->id_produto);
			}

			return $produto;
		}

		return ['atualizou' => false];

	}

	/**
	 * Retorna um produto através de uma condição.
	 * @param  array  $condicoes condições para pegar o produto.
	 * @param  array $with      relações que este produto tem.
	 * @return Produto.
	 */
	public function retornaProduto(array $condicoes, $with = null, $whereHas = null, $function = null)
	{
		$produto = $this;
		if(!is_null($with))
			$produto = $produto->with($with);

		$produto = $produto->makeModel();

		if(!is_null($whereHas))
		{
			$produto->whereHas($whereHas, $function);
		}

		$produto = $produto->where($condicoes)->first();

		return $produto;
	}

	/**
	 * Retorna um produto pela slug.
	 * @param string $slug - slug do produto solicitado.
	 * @param array $with - array com as relações do model.
	 * @return Collection.
	 */
	public function retornaProdutoPorSlug($slug, $with = null, $publicado = 0)
	{
		$produto = $this;
		if(!is_null($with))
			$produto = $this->with($with);

		$produto->makeModel();

		if($publicado)
		{
			return $this
			->findWhere(
				[
					'publicado' => $publicado,
					'slug' => $slug
				])
			->first();
		}

		if(in_array('propostas', $with))
		{

		}

		if(!is_null($with))
		{
			return $this->with($with)
				->where(
					[
						'slug' => $slug
					])
				->first();
		}

		return $this->findByField('slug', $slug)->first();
	}

	/**
	 * Busca os possíveis interesses de um usuário, baseado nos interesses de seu perfil.
	 * @param  array $interesses - interesses do seu perfil.
	 * @return array
	 */
	public function buscaProdutosPorInteresse($interesses)
	{
		$produtos = $this->makeModel()->whereIn('id_sub_categoria', $interesses)->cidade()->take(10)->get();

		return $produtos;
	}

	/**
	 * Busca produtos que possivelmente podem interessar o usuário logado.
	 * @param int $idUsuario - Usuário logado.
	 * @param int $limite de produtos.
	 */
	public function buscaProdutosInteresseProduto($idUsuario, $limite = 10, $cidade = null)
	{
		$interesses = $this->interesseUsuarioRepo->retornaInteressesUsuario($idUsuario);
		$interesses = $interesses->lists('id_sub_categoria');

		$produtos = $this->with(['imagens', 'usuario'])->makeModel()->whereIn('id_sub_categoria', $interesses)
								->cidade()
								->where('trocado', 0)
								->where('publicado', 1);

		if(!is_null($produtos))
		{
			$produtos->whereHas('usuario', function($query) use($cidade){
				$query->where('id_cidade', $cidade['id_cidade']);
			});
		}


		return $produtos->take(10)->get();
	}


	public function pesquisarProdutos($termo)
	{
		$produtos = $this->with(['imagens', 'subCategoria.categoria', 'interesses'])
						->makeModel()
						->where('produto', 'like', '%' . $termo . '%')
						->where('trocado', 0)
						->orWhereHas('interesses', function($query) use($termo){
							$query->where('descricao_produto', 'LIKE', '%' . $termo . '%');
						})
						->orWhereHas('subCategoria', function($query) use($termo){
							$query->where('sub_categoria', 'LIKE', '%' . $termo . '%');

							$query->orWhereHas('categoria', function($q) use($termo){
								$q->where('categoria', 'LIKE', '%'. $termo .'%');
							});
						})
						->orderBy('criado_em', 'desc')
						->paginate(20);

		return $produtos;
	}


}