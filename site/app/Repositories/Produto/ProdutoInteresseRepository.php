<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

/**
*
*/
class ProdutoInteresseRepository extends BaseRepository
{

	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	public function model()
	{
		return 'App\Models\Produto\ProdutoInteresse';
	}

}