<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;

/**
* @package MensagemPropostaUsuarioRepository.
*/
class MensagemPropostaUsuarioRepository extends BaseRepository
{
	
	public function __construct(Application $app)
	{
		parent::__construct($app);
	}

	/**
	 * Model deste repository.
	 */
	public function model()
	{
		return 'App\Models\Produto\MensagemPropostaUsuario';
	}

	
	
}