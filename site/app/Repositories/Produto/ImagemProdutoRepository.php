<?php namespace App\Repositories\Produto;

use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Produto\ProdutoRepository;


/**
* @package CategoriaRepository.
*/
class ImagemProdutoRepository extends BaseRepository
{
	
	public function __construct(Application $app, ProdutoRepository $produtoRepo)
	{
		parent::__construct($app);
		$this->produtoRepo = $produtoRepo;
	}


	public function model()
	{
		return 'App\Models\Produto\ImagemProduto';
	}

	/**
	 * Cria uma nova imagem para um produto.
	 */
	public function criar(array $dados)
	{
		return $this->create($dados);
	}

	
}