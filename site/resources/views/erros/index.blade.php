@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/fileinput/css/fileinput.min.css') }}">
	@stop
		<!--CONTENT-->
	<div class="container" id="conteudo">
		
		@include('elements.usuario.menu_usuario')
		<div class="container-fluid">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{!! Breadcrumbs::render('reportar_erro.index') !!}
						</div>
					</div>
					<form class="formErro form-horizontal" role="form" method="POST" action="{{ route('reportar_erro.post') }}" enctype="multipart/form-data">

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h2 class="font text-center"><i class="fa fa-bug"></i> Ops! Algo saiu errado?</h2>
									<p class="font text-center">Relate o erro para que possamos sempre prestar um serviço de qualidade.</p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
									<label class="control-label" for="pagina">Página do erro*:</label>
									<div class="label"></div>
									<input type="text" class="form-control" id="pagina" name="pagina" placeholder="http://troque.la/">
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
									<label class="control-label" for="arquivo">Print da tela*:</label>
									<div class="label"></div>
									<input id="input-2" type="file" class="file" name="print_tela">
									<small>Tire um print da tela com erro para que possamos identificá-lo mais rapidamente</small>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
									<label class="control-label" for="arquivo">O que o levou até este erro*:</label>
									<div class="label"></div>
									<textarea class="form-control" rows="5" id="comment" name="descricao_erro" placeholder="Por favor, descreva o passo-a-passo que você seguiu até chegar a este erro. Isso nos ajuda a identificar, entender e solucionar o problema mais rapidamente."></textarea>
								</div>
							</div>
						</div>

						<div class="row">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<button type="submit" class="btn btn-danger btEnviar"><i class="fa fa-paper-plane-o"></i> Enviar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="text-center">Com base em seus interesses</h3>
							</div>
						</div>
					</div>

					<div class="trocas">
				    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    			<p>São de teu interesse.</a></p>
			    		</div>
			    	</div>
				</div>
			</div>-->

		</div>

		@section('script')
			<script type="text/javascript" src="{{ asset('assets/fileinput/js/fileinput.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/institucional/erro.js')}}"></script>
		@stop

	@stop