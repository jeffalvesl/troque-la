<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" href="bootstrap/imgs/troquela.png">

		<title>Cadastre-se | Troque Lá</title>

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!--<script src="bootstrap/js/ie-emulation-modes-warning.js"></script>-->


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="pageCadastro">

		<div class="container-fluid">
			<div class="row">
			    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<form role="form" class="formCadastro" action="{{ route('cadastro.salvar') }}" method="POST">
						<h1 class="font row-centered">Cadastre-se</h1>
						<hr class="colorgraph">
						<div class="bottom text-center">
		                    <h4 class="font">
		                        <a href="{{ route('login.login') }}"><b>Já Possui cadastro? Faça login</b></a>
		                    </h4>
		                </div>
		                <div class="row">
							<div class="col-xs-12 col-md-12"><a href="{{ route('cadastro.social', 'facebook') }}" class="btn btn-primary btn-block btn-lg">Cadastrar com Facebook</a></div>
						</div>
		                <hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div class="label"></div>
			                        <input type="text" name="nome" id="nome" class="form-control input-lg" value="{{ old('nome') }}" placeholder="Nome" tabindex="1">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div class="label"></div>
									<input type="text" name="sobrenome" id="sobrenome" class="form-control input-lg" value="{{ old('sobrenome') }}" placeholder="Sobrenome" tabindex="2">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-12">
								<div class="form-group">
									<div class="label"></div>
									<input type="email" name="email" data-route="{{ route('verifica_email.post') }}" id="email" class="form-control input-lg" value="{{ old('email') }}" placeholder="Email" tabindex="4">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div class="label"></div>
									<input type="password" name="password" id="senha" class="form-control input-lg" value="{{ old('password') }}" placeholder="Senha" tabindex="5">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div class="label"></div>
									<input type="password" name="confirmacao_senha" id="confirmacao_senha" class="form-control input-lg" value="{{ old('confirmacao_senha') }}" placeholder="Confirme a senha" tabindex="6">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								 Clicando em <strong class="label label-success">Cadastrar</strong>, você concorda com os <a href="#" data-toggle="modal" data-target="#t_and_c_m">Termos e Condições</a> deste site, incluindo nosso uso de Cookies.
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-md-12"><input type="submit" value="Cadastrar" class="btn btn-success btn-block btn-lg" tabindex="7"></div>
						</div>
					</form>
				</div>
			</div>


			<!-- Modal -->
			<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel">Termos e Condições Gerais de Uso</h4>
						</div>
						<div class="modal-body">
							<h3>Sumário de Contrato do Troque.Lá</h3>


                                <div class="termo termoTexto col-sm-12 col-centered">


                                    <h3>Ao usuário,</h3>

                                    <h4>Olá! Obrigado por tirar um pouco do seu tempo para ler nossos Termos.</h4>

                                    <h4>As informações aqui prestadas o ajudarão no uso correto de nossa plataforma. Assim você fica sabendo qual o seu dever perante ao site e nós lhe diremos nossos direitos de forma que nunca tenhamos problemas. Afinal, creio que não queremos isso. Então relaxe os olhos e boa leitura!</h4>

                                    <p>O <b>Troque.Lá</b> não é fornecedor, distribuidor ou dono de qualquer produto aqui anunciado. Nós somos uma prestadora de serviço de trocas pertencente à JF Company LDTA. inscrita no CNPJ/MF de número __________________. Nós apenas liberamos um espaço para que seja fácil a troca de produtos entre clientes, produtos os quais eles são proprietários.</p>
                                    <p>Os usuários poderão anunciar apenas produtos que tenham consigo, sendo que o mesmo deverá possuir foto real, título e descrição, bem como informações adicionais como tempo de uso e/ou tempo restante de garantia, salve quando não houver.</p>
                                    <p>Para utilizar dos serviços do <b>Troque.lá</b>, o usuário deverá aceitar, expressamente, a Política de Privacidade, que contém informações completa sobre coleta, uso, armazenamento, tratamento e proteção de dados pessoais dos usuários e visitantes do SITE.</p>
                                    <p>O Usuário deverá criar um cadastro único regido por nome de usuário e senha que são pessoais e intransferíveis. O <b>Troque.Lá</b> não se responsabiliza pelo uso e/ou divulgação desses dados por terceiros. Tampouco pediremos que sejam informado por quaisquer meios eletrônicos, exceto no próprio SITE.</p>
                                    <p>Em razão da Legislação em vigor e/ou dos Termos e Condições, o <b>Troque.Lá</b> poderá, a qualquer momento, excluir publicações que estejam em desacordo com tais. Sendo também o Usuário passível de advertência, suspensão temporária ou definitiva da conta.</p>
                                    <p>O Usuário, antes de decidir sobre a troca, deverá se atentar aos detalhes da mesma, detalhes estes como localização do outro Usuário com o qual deseja realizar a troca, condições do produto e disponibilidade.</p>

                                    <h2>Termos e Condições Gerais de Uso do Site</h2>

                                    <p>Qualquer pessoa, doravante chamada de Usuário, que pretenda utilizar dos serviços prestados pelo SITE, deverá aceitar o seguinte Termo e todas as demais políticas e princípios que o regem.</p>

                                    <p>A aceitação destes Termos e Condições Gerais de Uso é absolutamente indispensável à utilização do SITE e serviços do <b>Troque.Lá</b>.</p>

                                    <p>O Usuário deverá ler e entender tais Termos e Condições Gerais de Uso, assim como documentos referidos, antes de cadastrar-se para uso do SITE e seus serviços.</p>

                                    <p>Em caso de dúvida acesse Ajuda ou Contato.</p>

                                    <h3>1 - Objeto</h3>

                                    <p>Os serviços objeto dos presentes Termos e Condições Gerais de Uso consistem em:</p>
                                    <p>(i) ofertar e hospedar espaços no SITE para que os usuários anunciem para troca seus próprios produtos;</p>
                                    <p>(ii) viabilizar o contato direto entre Usuários anunciantes e interessados em realizar a troca reciproca de produtos, por meio de divulgação dos dados de contato de uma parte à outra. O <b>Troque.Lá</b>, portanto, possibilita aos Usuários uma plataforma para contatarem e negociarem diretamente entre si, sem qualquer intervenção do <b>Troque.Lá</b>, na negociação ou na concretização dos negócios. Desta forma, ressalta-se que o <b>Troque.Lá</b> não fornecesse quaisquer produtos anunciados pelos Usuários do SITE.</p>

                                    <h3>2 - Capacitados para cadastro</h3>

                                    <p>O serviço do <b>Troque.Lá</b> está disponível para pessoas físicas que tenham capacidade legal para tal fim. Em caso de Pessoa Jurídica, apenas será aceito mediante contato prévio com a Administração do <b>Troque.Lá</b> para fins de ações comerciais.</p>

                                    <p>É proibido o cadastro de Usuários que não tenham capacidade civil, bem como de Usuários que tenham sido suspensos do <b>Troque.Lá</b> temporária ou definitivamente. Em caso de Pessoa Jurídica, expressamente proibido qualquer tipo de publicação senão por meio de representante legal mediante contato prévio com a Administração do <b>Troque.Lá</b>.</p>

                                    <h3>3 - Cadastro</h3>

                                    <p>O cadastramento apenas será efetivado quando o Usuário preencher todos os campo obrigatórios do cadastro, com informações verdadeiras. O Usuário declara e assume o compromisso de atualizar os dados cadastrais sempre que for necessário.</p>

                                    <p>Ao se cadastrar no <b>Troque.Lá</b>, o Usuário poderá utilizar de todos os serviços por nós disponibilizados, declarando, para tanto, ter lido, compreendido e aceitado o respectivo Termos de Condição de Uso ao concluir o cadastro.
                                    O Usuário terá acesso a sua conta através de seu e-mail ou login e senha, e compromete-se a não informar tais dados a terceiros, responsabilizando-se integralmente pelo uso que deles seja feito.</p>

                                    <p>O <b>Troque.Lá</b> não se responsabiliza pela correção dos Dados Pessoais inseridos por seus Usuários. Os Usuários garantem e respondem, em qualquer caso, civil e criminalmente, pela veracidade, exatidão e autenticidade dos Dados Pessoais cadastrados.</p>

                                    <p>O login que o Usuário utilizará no <b>Troque.Lá</b> não poderá guardar semelhança com o nome <b>Troque.Lá</b>, tampouco poderá ser utilizado qualquer apelido que insinue ou sugira que o produto anunciado pertença ou tenha qualquer relação com o <b>Troque.Lá</b>. Também serão excluídos contas nas quais o login seja considerado ofensivo, bem como os que contenham Dados Pessoais do Usuário além daqueles já divulgados pelo nosso sistema ou alguma URL que não do próprio SITE.</p>

                                    <p>O <b>Troque.Lá</b> se reserva o direito de recusar qualquer solicitação de cadastro e de suspender um cadastro previamente aceito, que esteja em desacordo com as políticas e regras dos presentes Termos e Condições Gerais.</p>

                                    <p>O <b>Troque.Lá</b> poderá, a seu exclusivo critério, realizar as buscas que julgar necessárias para apurar dados incorretos ou inverídicos, bem como solicitar dados adicionais e documentos que estime serem pertinentes a fim de conferir os Dados Pessoais informados.</p>

                                    <p>Caso o <b>Troque.Lá</b> decida checar a veracidade dos dados cadastrais de um Usuário e se constate haver entre eles dados incorretos ou inverídicos, ou ainda caso o Usuário se negue a enviar os documentos requeridos, o <b>Troque.Lá</b> poderá suspender temporariamente ou definitivamente a conta, sem prejuízo de outras medidas que entender necessárias e oportunas.</p>

                                    <p>Havendo a aplicação de quaisquer das sanções acima referidas, automaticamente serão cancelados os anúncios do respectivo Usuário, não lhe assistindo, por essa razão qualquer indenização ou ressarcimento.</p>

                                    <p>O Usuário será o único responsável pelas operações efetuadas emsua conta,uma vez que o acesso só será possível mediante a inclusão de senha pessoal e instransferível.</p>

                                    <p>Em nenhuma hipótese será permitida a criaçãodenovos cadastros por pessoas cujos cadastros originais tenham sido suspensos temporária ou definitivamente por infrações às políticas do <b>Troque.Lá</b>.</p>

                                    <h3>4 - Modificações dos Termos e Condições Gerais de Uso</h3>

                                    <p>O <b>Troque.Lá</b> poderá alterar, a qualquer tempo, estes Termos e Condições Gerais, visando seu aprimoramento e melhoria dos serviços prestados. Quando, e se houver, alguma mudança nos Termos atuais, informaremos o Usuário. Caso o Usuário esteja em desacordo com os termos alterados, ele deverá nos comunicar por e-mail em até 7 (sete) dias corridos após a publicação. Nesse caso,o vínculo contratual deixará de existir. Não havendo manifestação no prazo estipulado, entender-se-á que o Usuário aceitou os novos Termos e Condições Gerais de Uso e o contrato continuará vinculando as partes.</p>

                                    <p>As alterações não vigorarão em relação às negociações e anúncios já iniciados antes de sua publicação, permanecendo, nestes casos, vigente a redação anterior.</p>

                                    <h3>5 - Produtos anunciados</h3>

                                    <h4>5.1 - Anúncios</h4>

                                    <p>O Usuário poderá anunciar a troca de produtos em suas respectivas categorias e subcategorias. Os anúncios serão padronizados com título, foto, descrição, produtos os quais o Usuário gostaria em troca, data de criação (caso seja produto artesanal desenvolvido pelo próprio usuário) ou compra do produto e tempo restante de garantia legal mais o tempo de garantia estendida, caso haja. Presumir-se-á que, mediante a inclusão do anúncio no <b>Troque.Lá</b>, o Usuário declara possuir o direito perante o produto em questão.</p>

                                    <p>É expressamente proibido o Anúncio de produtos com troca 100% (cem por cento) em valor monetário, o que será qualificado como venda, o que não é o intuito do SITE, ficando o Usuário passível de banimento do serviço.</p>

                                    <p>Poderá usar valor monetário como forma de complemento de troca em propostas.</p>

                                    <h4>5.2 Inclusão de imagens</h4>

                                    <p>O usuário deverá incluir o mínimo de 1 (uma) foto e o máximo de 4 (quatro) fotos que sejam exatamente correpondente ao produto anunciado, sendo expressamente proibido o uso de imagens encontradas na internet que não remetem ao estado atual do produto.</p>

                                    <p>O <b>Troque.Lá</b> poderá retirar do SITE anúncios que tenham imagens em desacordo com o Termo e Condições Gerais de Uso e demais políticas.</p>

                                    <h4>5.3 Produtos Proibidos</h4>

                                    <p>Poderão ser anunciados aqueles produtos que não estejam em desacordo com os Termos e Condições Gerais, demais políticas do <b>Troque.Lá</b>, ou pela lei vigente.</p>

                                    <p>As políticas concernentes a produtos cujo anúncio é expressamente proibido no <b>Troque.Lá</b> encontram-se anexas a este documento.

                                    <h3>6 - Privacidade da Informação</h3>

                                    <p>Toda informação ou Dado Pessoal do Usuário é armazenado em servidores ou meios magnéticos de alta segurança. Salvo com relação às informações que são publicadas no SITE, o <b>Troque.Lá</b> adotará todas as medidas possíveis para manter a confidencialidade e a segurança das informações sigilosas, porém não se responsabilizará por parte de terceiros que utilizem as redes públicas ou a internet, subvertendo os sistemas de segurança oara acessar as informações de Usuários.</p>

                                    <p>O Usuário expressamente autoriza que suas informações e dados pessoais sejam compartilhados pelo <b>Troque.Lá</b> com as demais empresas integrantes do grupo econômico, parceitos comerciais, membros do Programa de Proteção à Propriedade Intelectual, autoridades e pessoas físicas ou jurídicas, mediante mandado judicial, que aleguem ter sido lesadas por Usuários.</p>

                                    <p>Em caso de dúvidas sobre a proteção a Dados Pessoais ou para obter mais informações sobre e os casos nos quais poderá ser quebrado o sigilo de que trata esta cláusula, por favor, consultar a página de Privacidade e Confidencialidade.</p>

                                    <h3>7 - Obrigação dos Usuários</h3>

                                    <p>Os Usuários interessados devem ter a capacidade legal para troca de produtos. O Usuário anunciante deverá entrar em contato com o Usuário interessado para responder a proposta pendente.</p>

                                    <p> Usuário anunciante se vê no direito de aceitar a proposta ofertada ou não.</p>

                                    <p> Usuário anunciante deverá, em cumprimento à legislação brasileira vigente, além de demonstrar informações claras e ostensivas a respeito de quaisquer restrições à aquisição do produto ou serviço, apontar sempre em seus anúncios as características essenciais do produto ou serviço, incluindo os riscos à saúde e à segurança dos consumidores.</p>

                                    <p>Quando uma negociação se concretizar, o Usuário anunciante e o Usuário interessado deverão firmar em suas respectivas área de Usuário informar que os produtos foram trocados para que saiam da lista de produtos disponíveis para troca.</p>

                                    <p>Em virtude do <b>Troque.Lá</b> não figurar como parte nas negociações de troca, a responsabilidade por todas as obrigações decorrentes de qualquer natureza será exclusivamente dos Usuários. Assim, ambos os Usuários declaram e reconhecem que na hipótese do <b>Troque.Lá</b> vir a ser demandada judicialmente ou tenha contra ela uma reclamação dos órgãos de proteção ao consumidos, os valores relativos às condenações, acordos, despesas processuais e honorários advocatícios dispendidos pela empresa serão de responsabilidade do Usuário que deu causa.</p>

                                    <h3>8 - Violação no sistema ou da base de dados</h3>

                                    <p>Não é permitida a utilização de nenhum dispositivo, software ou outro recurso que venha a interferir nas atividades e operações do <b>Troque.Lá</b>, bem como nos anúncios, descrições, contas ou seus bancos de dados. Qualquer intromissão, tentativa ou atividade que viole ou contrarie as leis de direito de propriedade intelectual e/ou as proibições estipuladas nestes Termos e Condições Gerais de Uso, tornarão o responsável passível das ações legais pertinentes, bem como das sanções aqui previstas, sendo ainda responsável pelas indenizações por eventuais danos causados.</p>

                                    <h3>9 - Sanções</h3>

                                    <p>em prejuízo de outras medidas cabíveis, o <b>Troque.Lá</b> poderá advertir, suspender, temporária ou definitivamente, a conta de um Usuário ou cancelar os seus anúncios a qualquer tempo, iniciando as ações legais cabíveis e/ou suspendendo a prestação de seus serviços se: a) o Usuário não cumprir qualquer dispositivo destes Termos e Condições Gerais de Uso e demais políticas do <b>Troque.Lá</b>; b) se descumprir com seus deveres de Usuário; c) se praticar atos fraudulentos ou dolosos; d) se não puder ser verificada a identidade do Usuário ou se qualquer informação fornecida por ele estiver incorreta; e) se o <b>Troque.Lá</b> entender que os anúncios ou qualquer atitude do Usuário tenham causado algum dano a terceiros ou ao próprio <b>Troque.Lá</b> ou tenham a potencialidade de assim o fazer. Nos casos suspensão da conta do usuário, todos os anúncios ativos e/ou ofertas realizadas serão automaticamente cancelados.</p>

                                    <p>O <b>Troque.Lá</b> reserva-se o direito de, a qualquer momento e a seu exclusivo critério, solicitar o envio de documentação pessoal e/ou exigir que um Usuário torne-se ‘Certificado’.</p>

                                    <h3>10 - Responsabilidades</h3>

                                    <p>O <b>Troque.Lá</b> não se responsabiliza por vícios ou defeitos técnicos e/ou operacionais oriundos do sistema do Usuário ou de terceiros.</p>

                                    <p>O <b>Troque.Lá</b> não é o proprietário dos produtos anunciados pelos Usuários no SITE, não guarda a posse desses itens e não realiza as ofertas de troca, tampouco intervém na forma de entrega dos produtos.</p>
                                    <p>O <b>Troque.Lá</b> não será responsável pelo efetivo cumprimento das obrigações assumidas pelos Usuários. O Usuário reconhece e aceita ao realizar negociações com outros Usuários ou terceiros o faz por sua conta e risco, reconhecendo o <b>Troque.Lá</b> como mero fornecedor de serviços de disponibilização de espaço virtual para anúncios de produtos ofertados por terceiros. Em nenhum caso o <b>Troque.Lá</b> será responsável por qualquer dano e/ou prejuízo que o Usuário possa sofrer devido às negociações realizadas ou não realizadas por meio do SITE decorrentes da conduta de outros usuários.</p>

                                    <p>O <b>Troque.Lá</b> recomenda que toda negociação seja realizada com cautela e bom senso. O Usuário deverá sopesar os riscos da negociação, levando em consideração que pode estar, eventualmente, lidando com pessoas valendo-se de identidade falsa.</p>

                                    <p>Nos casos em que um ou mais Usuários ou algum terceiro inicie qualquer tipo de reclamação ou ação legal contra outro ou mais Usuários, todos e cada um dos Usuários envolvidos nas reclamações ou ações eximem de toda responsabilidade o <b>Troque.Lá</b> e os seus diretores, gerentes, empregados, agentes, operários, representantes e procuradores, observado, ainda, o estipulado na cláusula 7.</p>

                                    <h3>11 - Alcance dos serviços</h3>

                                    <p>Estes Termos e Condições Gerais de Uso não geram nenhum contrato de sociedade, mandato, franquia ou relação de trabalho entre <b>Troque.Lá</b> e o Usuário. O Usuário manifesta ciência de que o <b>Troque.Lá</b> não é parte de nenhuma negociação realizada entre Usuários, nem possui controle algum sobre a existência, qualidade, segurança ou legalidade dos produtos ou serviços anunciados pelos Usuários, sobre a veracidade ou exatidão dos anúncios elaborados pelos usuários e sobre a capacidade dos Usuários para negociar. O <b>Troque.Lá</b> não pode assegurar o êxito de qualquer negociação realizada entre Usuários, tampouco verificar a identidade ou dos Dados Pessoais dos usuários.</p>

                                    <p>O <b>Troque.Lá</b> não garante a veracidade da publicação de terceiros que apareça em seu site e não será responsável pela correspondência ou contratos que o Usuário realize com terceiros.</p>

                                    <h3>12 - Falhas no sistema</h3>

                                    <p>O <b>Troque.Lá</b> não se responsabiliza por qualquer dano, prejuízo ou perda sofridos pelo Usuário em razão de falhas na internet, no sistema ou no servidor utilizado pelo usuário, decorrentes de condutas de terceiros, caso fortuito ou força maior. O <b>Troque.Lá</b> também não será responsável por qualquer vírus que possa atacar o equipamento do usuário em decorrência do acesso, utilização ou navegação na internet ou como consequência da transferência de dados.</p>

                                    <h3>13 - Propriedade Intelectual e links</h3>

                                    <p>O uso comercial da expressão "<b>Troque.Lá</b>" como marca, nome empresarial ou nome de domínio, bem como os conteúdos das telas relativas aos serviços do <b>Troque.Lá</b> assim como os programas, look and feel do SITE, bancos de dados, redes, arquivos que permitem ao Usuário acessar e usar a sua Conta são propriedade do <b>Troque.Lá</b> e estão protegidos pelas leis e tratados internacionais de direito autoral, marcas, patentes, modelos e desenhos industriais. O uso indevido e a reprodução total ou parcial dos referidos conteúdos são proibidos, salvo a autorização prévia e expressa por escrito do <b>Troque.Lá</b>.</p>

                                    <p>O SITE pode manter links com outros sites, o que não significa que esses sites sejam de propriedade ou operados pelo <b>Troque.Lá</b>. Não possuindo controle sobre esses sites de terceiros, razão pela qual o <b>Troque.Lá</b> não será responsável pelos seus conteúdos, práticas e serviços ofertados. A presença de links para outros sites não implica relação de sociedade, de supervisão, de cumplicidade ou solidariedade do <b>Troque.Lá</b> para com esses sites e seus conteúdos.</p>

                                    <p>O Usuário declara e garante que é titular, possui a devida autorização do(s) titular(es) de direito(s) de propriedade intelectual ou que, de outra forma, pode anunciar no <b>Troque.Lá</b>, oferecer produtos e/ou serviços anunciados ou declarar-se loja oficial de determinada marca, sendo o único responsável pelo conteúdo das suas publicações.</p>

                                    <p>O Usuário assume total responsabilidade por todos os prejuízos, diretos e indiretos, inclusive indenizações, lucros cessantes, honorários advocatícios e demais encargos judiciais e extrajudiciais que o <b>Troque.Lá</b> seja obrigado a incorrer em virtude de ato ou omissão do Usuário.</p>

                                    <p>Se o <b>Troque.Lá</b> receber alguma reclamação ou questionamento de terceiros (por exemplo, dos titulares de marca), o <b>Troque.Lá</b> poderá remover o seu anúncio e aplicar as sanções cabíveis.</p>

                                    <h3>14 - Legislação aplicável e Foro de eleição</h3>

                                    <p>Todos os itens destes Termos e condições gerais de uso são regidos pelas leis vigentes na República Federativa do Brasil. Para todos os assuntos referentes à interpretação, ao cumprimento ou qualquer outro questionamento relacionado a estes Termos e condições gerais de uso, as partes concordam em se submeter ao Foro da Cidade de Campo Grande-MS.</p>


                                </div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Eu Concordo</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		</div>
		
		<div class="container">
			<div class="rodape">
				<div class="menuRodape col-sm-12 col-xs-12 col-lg-12">
					<ul class="col-sm-12 col-xs-12 col-lg-12">
						<li role="presentation"><a class="page-scroll" href="{{ route('home.index') }}">Início</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('termo.index') }}">Termos</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('guia.index') }}">Como funciona</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('privacidade.index') }}">Privacidade</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}">Sobre</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#help">Ajuda</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#contato">Contato</a></li>
					</ul>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rodapeSocial">
	                <div class="row">
	                    <div class="socialItens socialItensInicio">
	                        <ul class="social-network social-circle">
	                            <li>
	                                <a href="#" class="icoFacebook" title="Facebook">
	                                    <i class="fa fa-facebook"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoTwitter" title="Twitter">
	                                    <i class="fa fa-twitter"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoGoogle" title="Google +">
	                                    <i class="fa fa-google-plus"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoInstagram" title="Intagram">
	                                    <i class="fa fa-instagram"></i>
	                                </a>
	                            </li>
	                        </ul>                
	                    </div>
	                </div>
	            </div>

				<div class="col-sm-12 col-xs-12 col-lg-12 menuRodape">
					<small>
							2015© Troque Lá plataforma de trocas onlines. Todos os direitos reservados a J.F Company.
					</small>
				</div>
			</div>
		</div>
		</div>
		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('assets/js/usuario/cadastro.js')}}"></script>

	</body>
</html>