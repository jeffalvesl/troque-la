@extends('layout.app')
	@section('conteudo')
		<!--FIM DO BUSCADOR-->
		<div class="container" id="conteudo">
			<!--CATEGORIA-->
			<div class="page-header">
				<h1 class="font">{{ $title }}</h1>
			</div>
			
		</div>
		<div class="container">
			<div class="row">
				<ul class="mega-dropdown-menu row">
					@foreach($categorias as $categoria)
						@if(count($categoria->produtos) > 0)
							<li class="col-sm-3">
								<ul>
									<a href="{{ route('produto_categoria.get', $categoria->slug) }}"><li class="dropdown-header">{{ $categoria->categoria }}</li></a>
									@foreach($categoria->subCategorias as $subCategoria)
										@if(count($subCategoria->produtos) > 0)
											<li><a href="{{ route('subcategoria_produtos.get', [$categoria->slug, $subCategoria->slug]) }}">{{ $subCategoria->sub_categoria }}</a></li>
										@endif
									@endforeach
								</ul>
							</li>
						@endif
					@endforeach
				</ul>
			</div>
		</div>
@stop