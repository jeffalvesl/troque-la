@extends('layout.app')

	@section('conteudo')

		<!--CONTENT-->
	<div class="container" id="conteudo">
		<!--NOVIDADES-->
		@include('elements.usuario.menu_usuario')

		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-12">
			<div class="formUsuarioAmbiente">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{!! Breadcrumbs::render('cancelar.conta') !!}
					</div>
				</div>
				<form class="form-horizontal" role="form" method="POST" action="{{ route('cancelar.post') }}">

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h1 class="font text-center">
									<i class="fa fa-frown-o"></i>
								</h1>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h4 class="font text-center">
									
									<strong>{{ Auth::user()->nome }}, lembramos que a qualquer momento você pode voltar e reativar a sua conta</strong>.
									É uma pena te ver partir. Queremos te ouvir e saber em que podemos melhorar para que você possa continuar utilizando do nosso serviço.
									Caso queira realmente nos deixar, preencha o formulário abaixo:
								</h4>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<label class="control-label" for="email">Email:</label>
								<input type="text" name="email" class="form-control" id="email" value="{{ Auth::user()->email }}" disabled>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</div>
						
							
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> 
								<label class="control-label" for="senha">Senha*:</label>
								<input type="password" class="form-control" id="senha" placeholder="senha">
							</div>
						</div>
					</div>


					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
								<label class="control-label" for="motivo_cancelamento">Informe o motivo do cancelamento*:</label>
								<textarea class="form-control" rows="5" id="comment" name="motivo_cancelamento"></textarea>
								<small>{{ Auth::user()->nome }}, você tem certeza de que deseja cancelar sua conta? Você pode voltar a qualquer momento e reativá-la.</small>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group"> 
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
								<button type="submit" class="btn btn-danger">
									<i class="fa fa-unlock-alt"></i> Cancelar conta
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop