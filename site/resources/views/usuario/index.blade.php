@extends('layout.app')
	@section('conteudo')
		@section('style')
			<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
			<link rel="stylesheet" href="{{ asset('assets/css/datepicker/bootstrap-datepicker.min.css') }}" />
		@stop
		<!--END MENU-->


		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>


		<div class="container" id="conteudo">
			@include('elements.usuario.menu_usuario')

			<!-- MEUS DADOS -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{!! Breadcrumbs::render('usuario.index') !!}
						</div>
					</div>
					<form class="form-horizontal formUsuario" action="{{ route('atualizar_informacoes.post') }}" method="POST">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h3 class="font text-center"><i class="fa fa-info-circle"></i> Minhas Informações</h3>
								</div>

								<div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    			<h4>
					    				<i class="fa fa-smile-o"></i>
					    				 {{ Auth::user()->nome . ' ' . Auth::user()->sobrenome }}, seja Bem-vindo! Obrigado, por usar o Troque Lá.
				    				</h4>

				    				<h4>
				    					@if(count($trocas) <= 0 || count($produtos) <= 0)
						    				<a href="{{ route('home.index') }}">
						    				 	Que tal realizar algumas trocas? Clique aqui e comece já!
						    				</a>
					    				@endif
				    				</h4>
					    		</div>
							</div>
						</div>

						<div class="container-fluid">
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<label class="control-label" for="nome">Nome:</label>
										<div class="label"></div>
										<input type="text" name="nome" class="form-control" id="nome" value="{{ $usuario->nome }}" placeholder="Nome">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<label class="control-label" for="sobrenome">Sobrenome:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="sobrenome" id="sobrenome" value="{{ $usuario->sobrenome }}" placeholder="Sobrenome">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="data_nascimento">Nascimento:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="data_nascimento" id="data_nascimento" value="{{ $usuario->data_nascimento }}" placeholder="20/09/1985">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="telefone">Telefone:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="telefone" id="telefone" value="{{ $usuario->telefone }}" placeholder="(00) 9999-9999">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="celular">Celular:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="celular" id="celular" value="{{ $usuario->celular }}" placeholder="(00) 9999-9999">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="interesse">Sexo:</label>
										<div class="label"></div>
										<select name="sexo" class="sexo form-control" id="sexo">
											<option></option>
											<option value="Masculino" {{ $usuario->sexo == 'Masculino' ? 'selected' : '' }}>Masculino</option>
											<option value="Feminino" {{ $usuario->sexo == 'Feminino' ? 'selected' : '' }}>Feminino</option>
											<option value="Outros" {{ $usuario->sexo == 'Feminino' ? 'Outros' : '' }}>Outros</option>
										</select>
									</div>

									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
										<label class="control-label" for="email">Email:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="email" id="email" value="{{ $usuario->email }}" placeholder="Email">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="cep">CEP:</label>
										<div class="label"></div>
										<input type="text" class="form-control" id="cep" name="cep" data-url="{{ route('consultar_localidade.post') }}" id="cep" value="{{ $usuario->cep }}" placeholder="78089-098">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<label class="control-label" for="endereco">Endereço:</label>
										<input type="text" readonly class="form-control" id="endereco" name="endereco" value="{{ old('endereco', $usuario->endereco) }}" placeholder="Rua, Avenida..">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-2">
										<label class="control-label" for="bairro">Bairro:</label>
										<input type="text" readonly class="form-control" name="bairro" id="bairro" value="{{ $usuario->bairro }}" placeholder="Bairro">
									</div>

									<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
										<label class="control-label col-sm-2" for="numero">Número:</label>
										<input type="text" class="form-control" name="numero" id="numero" value="{{ old('numero', $usuario->numero) }}" placeholder="123">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<label class="control-label" for="complemento">Complemento:</label>
										<input type="text" class="form-control" name="complemento" id="complemento" value="{{ $usuario->complemento }}" placeholder="Bloco, apartamento, fundos, casa...">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
										<label class="control-label" for="cidade">Cidade:</label>
										<input type="text" readonly class="form-control" id="cidade" value="{{ $usuario->cidade ? $usuario->cidade->cidade : '' }}" placeholder="Cidade">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
										<label class="control-label" for="uf">Estado:</label>
										<input type="text" readonly class="form-control" id="uf" value="{{ isset($usuario->cidade->estado->sigla) ? $usuario->cidade->estado->sigla : '' }}" placeholder="UF">
									</div>
								</div>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id_cidade" value="{{ $usuario->id_cidade }}">
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<label class="control-label" for="interesse">Interesses:</label>
										<select name="interesses[]" class="interesses form-control" data-interesse="{{ route('interesse_usuario.post') }}" data-url="{{ route('retornar.categorias') }}" id="interesse" multiple="multiple">
											<option value="0">Escolha as categorias</option>
											@foreach($interesses as $interesse)
												<optgroup label="{{ $interesse->subCategoria->categoria->categoria }}">
												    <option value="{{ $interesse->subCategoria->id_sub_categoria }}" selected>{{ $interesse->subCategoria->sub_categoria }}</option>
										  		</optgroup>
											@endforeach
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<button type="submit" class="btSalvar btn btn-danger"><i class="fa fa-floppy-o"></i> Salvar dados</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			@if(isset($interessesProdut))
				<!-- COM BASE NOS INTERESSES DO USUÁRIO -->
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-12">
					<div class="formUsuarioAmbiente">
						<div class="row">

							<div class="trocas">
								<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="row text-center">
										<h3 class="text-center"><a href="#"><i class="fa fa-exchange"></i></a></h3>
										<h3 class="text-center">Com base em seus interesses</h3>
						    		</div>
								</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>
					    	</div>

						</div>
					</div>
				</div>
			@endif

			<!-- MINHAS TROCAS -->
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="font text-center"><i class="fa fa-exchange"></i> Trocas</h3>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="trocas">
						    @forelse($trocas as $troca)
						    	@if(isset($troca->propostas[0]))
							    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
							    		<a href="{{ route('editar.produto', $troca->slug) }}">
							    			@if(isset($troca->imagens[0]) && is_file($troca->imagens[0]->minuatura))
							    				<img src="{{ asset($troca->imagens[0]->minuatura) }}" width="200" class="img-rounded" alt="{{ $troca->produto }}">
							    			@else
							    				<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $troca->produto }}">
							    			@endif
							    		</a>

							    		<a href="{{ route('editar.produto', $troca->slug) }}"><h4>{{ $troca->produto }}</h4></a>
							    	</div>
							    	<div class="troca col-xs-1 col-sm-1 col-md-1 col-lg-4">
										<h2 class="text-center">
											<i class="fa fa-exchange"></i>
										</h2>
										<h4 class="text-center">Trocado com:</h4>
										<h4 class="text-center">{{ $troca->propostas[0]->usuario->nome . ' ' .$troca->propostas[0]->usuario->sobrenome }}</h4>
							    	</div>

							    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
							    		<a href="{{ route('produtos.detalhe', $troca->propostas[0]->produtoProposta->produto->slug) }}"><img src="{{ isset($troca->propostas[0]->produtoProposta->produto->imagens[0]) ? asset($troca->propostas[0]->produtoProposta->produto->imagens[0]->miniatura) : asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $troca->propostas[0]->produtoProposta->produto->produto }}"></a>

							    		<a href="{{ route('produtos.detalhe', $troca->slug) }}">
							    			<h4 class="text-center">{{ $troca->propostas[0]->produtoProposta->produto->produto }}</h4>
							    		</a>
							    	</div>
							    @endif

						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    		<div class="row text-center">
					    				<h4><i class="fa fa-exclamation-triangle"></i> Você não fez nenhuma troca. <a href="{{ route('home.index') }}">Vamos realizar sua primeira troca? Confira as novidades e produtos de seu interesse</a></h4>
					    			</div>
					    		</div>
						    @endforelse

					    	@forelse($propostasAceitas as $produto)
					    		@if(count($troca->propostas) > 0)
							    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
							    		<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    			@if(isset($produto->imagens[0]) && isset($produto->imagens[0]->miniatura))
							    				<img src="{{  is_file($produto->imagens[0]->miniatura) ? $produto->imagens[0]->miniatura : $produto->imagens[0]->imagem }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
							    			@else
							    				<img src="{{  asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
							    			@endif
							    		</a>
							    		<a href="{{ route('produtos.detalhe', $produto->slug) }}"><h4>{{ $produto->produto }}</h4></a>
							    	</div>
							    	<div class="troca col-xs-1 col-sm-1 col-md-1 col-lg-4">
										<h2 class="text-center">
											<i class="fa fa-exchange"></i>
										</h2>
										<h4 class="text-center">Trocado com:</h4>
										<h4 class="text-center">{{ $produto->usuario->nome . ' ' .$produto->usuario->sobrenome }}</h4>
							    	</div>

							    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
							    		<a href="{{ route('editar.produto', $produto->propostas[0]->produtoProposta->produto->slug) }}">
			    							@if(isset($produto->propostas[0]->produto->imagens[0]))
							    				<img src="{{ asset($troca->propostas[0]->produtoProposta->produto->imagens[0]->imagem) }}" width="200" class="img-rounded" alt="{{ $produto->propostas[0]->produtoProposta->produto->produto }}">
							    			@else
							    				<img src="{{ asset('assets/imgs/image_facebook.png')  }}" width="200" class="img-rounded" alt="{{ $produto->propostas[0]->produtoProposta->produto->produto }}">
							    			@endif


							    		</a>

							    		<a href="{{ route('editar.produto', $troca->slug) }}"><h4 class="text-center">{{ $produto->propostas[0]->produtoProposta->produto->produto }}</h4></a>
							    	</div>
							    @endif

						    	@empty

						    @endforelse

				    	</div>
					</div>
				</div>
			</div>

			<!-- SUGESTÕES PARA TROCA
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-12">
					<div class="formUsuarioAmbiente">
						<div class="row">

							<div class="trocas">
								<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="row text-center">
										<h3 class="text-center"><a href="#"><i class="fa fa-exchange"></i></a></h3>
										<h3 class="text-center">Com base em seus interesses</h3>
						    		</div>
								</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>
					    	</div>

						</div>
					</div>
				</div>-->

			<!-- Produtos postados - Colocar possíveis trocas abaixo de cada produto. -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="font text-center"><i class="fa fa-bookmark"></i> Minhas postagens</h3>
							</div>

							@if(count($produtos) == 12)
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<a href="{{ route('produto_usuario.novo') }}">
										<h3 class="font text-center"><i class="fa fa-bars"></i> Todos Produtos</h3>
									</a>
								</div>
							@endif

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a href="{{ route('produto_usuario.novo') }}">
									<h3 class="font text-center"><i class="fa fa-plus"></i> Adicionar Produto</h3>
								</a>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="trocas">
					    	@forelse($produtos as $key => $produto)
						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-4">
									<div class="row text-center">
						    			<a href="{{ route('editar.produto', $produto->slug) }}">
						    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
						    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
						    				@else
						    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
						    				@endif
						    			</a>
						    			<a href="{{ route('editar.produto', $produto->slug) }}">
						    				<h3 class="text-center">{{ $produto->produto }}</h3>
						    			</a>

						    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    			<small class="text-center">
							    				Views: {{ $produto->views }}
							    			</small>
						    			</a>

						    			<small>
						    				@if(isset($produto->troca))
						    					Situação:
												{{ $produto->troca->situacao->situacao }}
											@elseif(count($produto->imagens) < 4)
						    					Situação:
												<a href="#">{{'A carregar imagens ('. (4 - count($produto->imagens)).')'}}</a>
											@else
												<a href="{{ route('propostas.produto', $produto->slug) }}" >Propostas ({{ count($produto->propostas) }})</a>
											@endif
						    			</small>
						    		</div>
						    	</div>

						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    			<h4><i class="fa fa-frown-o"></i> Você ainda não postou nenhum produto? <a href="{{ route('produto_usuario.novo') }}">Clique aqui e cadastre um item para trocar com outros usuários.</a></h4>
					    		</div>
						    @endforelse
				    	</div>
					</div>
				</div>
			</div>

		</div>
	@stop

	@section('script')
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepcker.pt.BR.js') }}"></script>
		<script src="{{ asset('assets/js/select2.min.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
    	<script src="{{ asset('assets/js/cep.js')}}"></script>
    	<script src="{{ asset('assets/js/usuario/usuario.js')}}"></script>
    @stop
	