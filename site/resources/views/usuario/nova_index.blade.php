@extends('layout.app')
	@section('conteudo')
		@section('style')
			<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
			<link rel="stylesheet" href="{{ asset('assets/css/datepicker/bootstrap-datepicker.min.css') }}" />
		@stop
		<!--END MENU-->


		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>
			<!--CONTENT-->
		<div class="container" id="conteudo">

			<!-- Minhas informações -->
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-10">
				<div class="formUsuarioAmbiente">
					<form class="form-horizontal formUsuario" action="{{ route('atualizar_informacoes.post') }}" method="POST">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
									<h3>Minhas Informações</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
									<label class="control-label" for="nome">Nome:</label>
									<div class="label"></div>
									<input type="text" name="nome" class="form-control" id="nome" value="{{ $usuario->nome }}" placeholder="Nome">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
									<label class="control-label" for="sobrenome">Sobrenome:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="sobrenome" id="sobrenome" value="{{ $usuario->sobrenome }}" placeholder="Sobrenome">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-2">
									<label class="control-label" for="data_nascimento">Nascimento:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="data_nascimento" id="data_nascimento" value="{{ $usuario->data_nascimento }}" placeholder="20/09/1985">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-2">
									<label class="control-label" for="telefone">Telefone:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="telefone" id="telefone" value="{{ $usuario->telefone }}" placeholder="(00) 9999-9999">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-2">
									<label class="control-label" for="celular">Celular:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="celular" id="celular" value="{{ $usuario->celular }}" placeholder="(00) 9999-9999">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-2">
									<label class="control-label" for="interesse">Sexo:</label>
									<div class="label"></div>
									<select name="sexo" class="sexo form-control" id="sexo">
										<option></option>
										<option value="Masculino" {{ $usuario->sexo == 'Masculino' ? 'selected' : '' }}>Masculino</option>
										<option value="Feminino" {{ $usuario->sexo == 'Feminino' ? 'selected' : '' }}>Feminino</option>
										<option value="Outros" {{ $usuario->sexo == 'Feminino' ? 'Outros' : '' }}>Outros</option>
									</select>
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-4">
									<label class="control-label col-sm-2" for="email">Email:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="email" id="email" value="{{ $usuario->email }}" placeholder="Email">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-2">
									<label class="control-label" for="cep">CEP:</label>
									<div class="label"></div>
									<input type="text" class="form-control" id="cep" name="cep" data-url="{{ route('consultar_localidade.post') }}" id="cep" value="{{ $usuario->cep }}" placeholder="78089-098">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-6">
									<label class="control-label" for="endereco">Endereço:</label>
									<input type="text" readonly class="form-control" id="endereco" name="endereco" value="{{ old('endereco', $usuario->endereco) }}" placeholder="Rua, Avenida..">
								</div>

								<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
									<label class="control-label col-sm-2" for="numero">Número:</label>
									<input type="text" class="form-control" name="numero" id="numero" value="{{ old('numero', $usuario->numero) }}" placeholder="123">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<label class="control-label" for="complemento">Complemento:</label>
									<input type="text" class="form-control" name="complemento" id="complemento" value="{{ $usuario->complemento }}" placeholder="Bloco, apartamento, fundos, casa...">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<label class="control-label" for="bairro">Bairro:</label>
									<input type="text" readonly class="form-control" name="bairro" id="bairro" value="{{ $usuario->bairro }}" placeholder="Bairro">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-4"> 
									<label class="control-label" for="cidade">Cidade:</label>
									<input type="text" readonly class="form-control" id="cidade" value="{{ isset($usuario->cidade->cidade) ? $usuario->cidade->cidade : '' }}" placeholder="Cidade">
								</div>

								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1"> 
									<label class="control-label" for="uf">Estado:</label>
									<input type="text" readonly class="form-control" id="uf" value="{{ isset($usuario->cidade->estado->sigla) ? $usuario->cidade->estado->sigla : '' }}" placeholder="UF">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
									<label class="control-label" for="interesse">Interesses:</label>
									<select name="interesses[]" class="interesses form-control" data-interesse="{{ route('interesse_usuario.post') }}" data-url="{{ route('retornar.categorias') }}" id="interesse" multiple="multiple">
										<option value="0">Escolha as categorias</option>
										@foreach($interesses as $interesse)
											<optgroup label="{{ $interesse->subCategoria->categoria->categoria }}">
											    <option value="{{ $interesse->subCategoria->id_sub_categoria }}" selected>{{ $interesse->subCategoria->sub_categoria }}</option>
									  		</optgroup>
										@endforeach
									</select>
								</div>
							</div>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_cidade" value="{{ $usuario->id_cidade }}">
						</div>

						<div class="row">
							<div class="form-group"> 
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-4"> 
									<button type="submit" class="btSalvar btn btn-default">Salvar dados</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-8 col-md-4 col-lg-12">
								<h3 class="text-center">Com base em seus interesses</h3>
							</div>
						</div>
					</div>

					<div class="trocas">
				    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    			<p>São de teu interesse.</a></p>
			    		</div>
			    	</div>
				</div>
			</div>

			<!-- Minhas Trocas -->
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-10">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
								<h3>Trocas</h3>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="trocas">
					    	@forelse($trocas as $troca)
						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<a href="{{ route('editar.produto', $troca->slug) }}"><img src="{{ isset($troca->imagens[0]) ? asset($troca->imagens[0]->imagem) : 'http://i.imgur.com/Qt0RAmx.jpg?1' }}" width="200" class="img-rounded" alt="{{ $troca->produto }}"></a>

						    		<a href="{{ route('editar.produto', $troca->slug) }}"><span>{{ $troca->produto }}</span></a>
						    		<span style="display: block;">
						    			<strong>
						    			Situação: {{ $troca->troca->situacao->situacao }}
						    			</strong>
						    		</span>

						    		<span>
						    			<strong>
						    			Trocado com: <a href="{{ route('mensagens_produto.index', [$troca->slug, $troca->troca->proposta->id_proposta_produto]) }}">{{ $troca->troca->proposta->usuario->nome . ' ' . $troca->troca->proposta->usuario->sobrenome  }}</a>
						    			</strong>
						    		</span>
						    	</div>
						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    			<p>Você ainda não realizou uma troca. <a href="#">Clique aqui e comece a trocar</a></p>
					    		</div>
						    @endforelse
				    	</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-8 col-md-4 col-lg-12">
								<h3 class="text-center">Novidades</h3>
							</div>
						</div>
					</div>

					<div class="trocas">
				    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    			<p>São de teu interesse.</a></p>
			    		</div>
			    	</div>
				</div>
			</div>


		</div>
	@stop

	@section('script')
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepcker.pt.BR.js') }}"></script>
		<script src="{{ asset('assets/js/select2.min.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
    	<script src="{{ asset('assets/js/cep.js')}}"></script>
    	<script src="{{ asset('assets/js/usuario/usuario.js')}}"></script>
    @stop
	