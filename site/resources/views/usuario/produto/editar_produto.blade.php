@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/fileinput/css/fileinput.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/datepicker/bootstrap-datepicker.min.css') }}" />
	@stop

		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>

		<div class="container" id="conteudo">
			@include('elements.usuario.menu_usuario')


			<!-- DADOS DO PRODUTO -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<!-- Breadcrumbs -->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{!! Breadcrumbs::render('editar.produto', $produto) !!}
						</div>
					</div>
					<form class="form-horizontal formProduto" method="POST" role="form" action="{{ route('produto_usuario.salvar') }}">
						<input type="hidden" name="id_produto" class="form-control" value="{{ $produto->id_produto }}">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h3 class="font text-center"><i class="fa fa-pencil-square-o"></i> Editar - {{ $produto->produto }}</h3>
								</div>
							</div>
						</div>
						<div class="container-fluid">
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
										<label class="control-label" for="produto">Produto*:</label>
										<div class="label"></div>
										<input type="text" name="produto" class="form-control" value="{{ $produto->produto }}" id="produto" placeholder="Nome do produto que deseja trocar" {{ $disabled }}>
									</div>

									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<label class="control-label" for="idSubCategoria">Seção do produto*:</label>

										<div class="label"></div>
										<select name="id_sub_categoria" class="idSubCategoria form-control" id="idSubCategoria" {{ $disabled }}>
											<option></option>
											@foreach($categorias as $categoria)
												<optgroup label="{{ $categoria->categoria }}">
													@foreach($categoria->subCategorias as $subCategoria)
												    	<option value="{{ $subCategoria->id_sub_categoria }}" {{ $produto->id_sub_categoria == $subCategoria->id_sub_categoria ? 'selected' : '' }}>{{ $subCategoria->sub_categoria }}</option>
												    @endforeach
										  		</optgroup>
											@endforeach
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<label class="control-label" for="descricao_produto">Descrição do produto*:</label>
										<div class="label"></div>
										<textarea class="form-control" name="descricao_produto" placeholder="Descreva seu produto de forma bem detalhada para que os usuários possam saber sobre seu produto.." rows="5" id="comment" {{ $disabled }}>{{ $produto->descricao_produto }}</textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
										<label class="control-label" for="data_aquisicao">Data de aquisição*:</label>
										<div class="label"></div>
										<input type="text" class="form-control" name="data_aquisicao" value="{{ $produto->data_aquisicao }}" id="data_aquisicao" placeholder="Data aproximada da aquisição" {{ $disabled }}>
									</div>

									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<label class="control-label" for="garantia">Tempo restante de garantia:</label>
										<div class="label"></div>

										<input type="text" class="form-control" id="tempo_garantia" value="{{ $produto->tempo_garantia }}" name="tempo_garantia" placeholder="Ex.: 8 meses">
									</div>

									<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
										<label class="control-label" for="produtos_interesse" class="interesse_prod_route">Produtos de interesse:</label>
										<select name="produtos_interesse[]" class="produtos_interesse form-control"  data-route="{{ route('interesse_usuario_troca.get') }}" id="produtos_interesse" multiple {{ $disabled }}>
											@foreach($produto->interesses as $interesse)
												<option value="{{ $interesse->produto_interesse }}" selected>{{ $interesse->descricao_produto }}</option>
											@endforeach
										</select>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">

								</div>
							</div>

							@if(count($produto->imagens) >= 3)
								<div class="row">
									<div class="form-group">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="checkbox">
												<label>
													<input type="checkbox" id="publicado" value="1" {{ $produto->publicado ? 'checked' : '' }} name="publicado" placeholder="Publicado">
													<strong>Quero publicar meu produto</strong>
												</label>
											</div>
										</div>
									</div>
								</div>
							@endif

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-1">
										<button type="submit" class="btn btn-danger btPublicarProduto"><i class="fa fa-floppy-o"></i> Salvar</button>
									</div>

									@if($produto->publicado)
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-1">
											<a target="_blank" href="{{ route('produtos.detalhe', [$produto->slug]) }}" class="btn btn-danger btViewProduto">Visualizar ONLINE <i class="fa fa-external-link-square"></i></a>
										</div>
									@endif
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- SUGESTÕES PARA TROCA -->
<!--			@if($produto->publicado)
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-12">
					<div class="formUsuarioAmbiente">
						<div class="row">

							<div class="trocas">
								<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="row text-center">
										<h3 class="text-center"><a href="#"><i class="fa fa-exchange"></i></a></h3>
										<h3 class="text-center">Com base nos interesses deste produto</h3>
						    		</div>
								</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>

						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
						    		<div class="row text-center">
						    			<a href="#"><img src="{{ asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="Teste"></a>

						    			<a href="#"><h3 >Sugestão de troca</h3></a>
						    		</div>
						    	</div>
					    	</div>

						</div>
					</div>
				</div>
			@endif-->

			<!-- IMAGENS DE UM PRODUTO -->
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-12">
				<div class="row">
					<div class="formUsuarioAmbiente">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h3 class="font text-center"><i class="fa fa-picture-o"></i> Imagens </h3>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="message none">
								<div class="alert alert-info">
									  <strong>Aguarde..</strong> Carregando Arquivos
								</div>
							</div>

							<div class="alert alert-info">
								<strong>Caro {{ $produto->usuario->nome . ' ' .$produto->usuario->sobrenome  }}, você pode inserir até 4 imagens de seu produto.
								É obrigatório que insira ao menos 3 imagens de seu produto.</strong>
							</div>


					    	<div class="col-sm-12 col-md-3 col-lg-12">
								@if(count($produto->imagens) < 4)
									<form class="formUploadImagemProduto" method="POST">
										<div class="form-group">
											<div class="col-xs-12 col-sm-8 col-md-5 col-lg-6">
												<span class="btn btn-default btn-file">
													@if(count($produto->imagens) < 3)
												    	Carregar arquivos <input id="fileupload" data-route="{{ route('upload.imagem_produto') }}" name="imagens_produto" class="imagens_produto" type="file" accept="image/jpg, image/jpeg" multiple>
												    @else
												    	Carregar arquivo <input id="fileupload" data-route="{{ route('upload.imagem_produto') }}" name="imagens_produto" class="imagens_produto" type="file" accept="image/jpg, image/jpeg">
												    @endif
												</span>
												<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">

												<div class="form-group">
													<div class="col-xs-12 col-sm-8 col-md-5 col-lg-7">
														<button type="submit" class="btn btn-info btPublicarProduto {{ count($produto->imagens) < 4 ? 'none' : '' }}">Publicar Produto</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								@endif
							</div>
						</div>


					    <div class="trocas col-xs-12 col-sm-12 col-md-3 col-lg-12">
							@forelse($produto->imagens as $key => $imagem)
						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="row">
						    			<a href="{{ route('editar.produto', $imagem->slug) }}">
						    				@if(is_file($imagem->imagem) && is_file($imagem->miniatura))
						    					<img src="{{ asset($imagem->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
						    				@else
						    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
						    				@endif
						    			</a>
						    		</div>
					    			<a href="{{ route('excluir_imagem.produto', $imagem->id_imagem_produto) }}" class="excluirImagem" data-id="{{ $imagem->id_imagem_produto }}">Excluir</a>
						    	</div>

						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    			<h4><i class="fa fa-frown-o"></i> Você ainda não subiu nenhuma imagem para {{ $produto->produto }}? <a href="javascript:void(0)">Clique em carregar arquivos e selecione as fotos de seu item</a></h4>
					    		</div>
						    @endforelse
					    </div>
					</div>
				</div>
			</div>

			@section('script')
				<script type="text/javascript" src="{{ asset('assets/fileinput/js/fileinput.min.js') }}"></script>
				<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
				<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepcker.pt.BR.js') }}"></script>
				<script src="{{ asset('assets/js/select2.min.js') }}"></script>
		    	<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
		    	<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
		    	<script src="{{ asset('assets/js/cep.js')}}"></script>
		    	<script src="{{ asset('assets/js/usuario/produto/produto.js')}}"></script>
		    	<script src="{{ asset('assets/js/usuario/produto/produto_imagem.js')}}"></script>
			@stop
	@stop