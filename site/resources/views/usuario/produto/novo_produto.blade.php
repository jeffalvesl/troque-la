@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/fileinput/css/fileinput.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/datepicker/bootstrap-datepicker.min.css') }}" />
	@stop

		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>

		<div class="container" id="conteudo">

		@include('elements.usuario.menu_usuario')

		<!-- FORMULÁRIO DE ADICIONAR PRODUTO -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="formUsuarioAmbiente">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{!! Breadcrumbs::render('produto_usuario.novo') !!}
					</div>
				</div>
				<form class="form-horizontal formProduto" method="POST" role="form" action="{{ route('produto_usuario.salvar') }}">

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h3 class="font text-center"><i class="fa fa-plus"></i> Adicionar Produto</h3>
							</div>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<label class="control-label" for="produto">Produto*:</label>
									<div class="label"></div>
									<input type="text" name="produto" class="form-control" id="produto" placeholder="Nome do produto que deseja trocar">
								</div>

								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label class="control-label" for="idSubCategoria">Seção do produto*:</label>
									<div class="label"></div>
									<select name="id_sub_categoria" class="idSubCategoria form-control" id="idSubCategoria">
										<option></option>
										@foreach($categorias as $categoria)
											<optgroup label="{{ $categoria->categoria }}">
												@foreach($categoria->subCategorias as $subCategoria)
											    	<option value="{{ $subCategoria->id_sub_categoria }}">{{ $subCategoria->sub_categoria }}</option>
											    @endforeach
									  		</optgroup>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label class="control-label" for="descricao_produto">Descrição do produto*:</label>
									<div class="label"></div>
									<textarea class="form-control" name="descricao_produto" placeholder="Descreva seu produto de forma bem detalhada para que os usuários possam saber sobre seu ele..." rows="5" id="comment"></textarea>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
									<label class="control-label" for="data_aquisicao">Data de aquisição*:</label>
									<div class="label"></div>
									<input type="text" class="form-control" name="data_aquisicao" id="data_aquisicao" placeholder="Data aproximada da aquisição">
								</div>

								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
									<label class="control-label" for="garantia">Tempo restante de garantia:</label>
									<div class="label"></div>
									<input type="text" class="form-control" id="tempo_garantia" name="tempo_garantia" placeholder="Ex.: 8 meses">
								</div>

								<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
									<label class="control-label" for="produtos_interesse" class="interesse_prod_route">Produtos de interesse:</label>
									<select name="produtos_interesse[]" class="produtos_interesse form-control"  data-route="{{ route('interesse_usuario_troca.get') }}" id="produtos_interesse" multiple>

									</select>
								</div>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">

							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<button type="submit" class="btn btn-danger btPublicarProduto"><i class="fa fa-floppy-o"></i> Salvar</button>
								</div>
							</div>
						</div>
					</div>


				</form>
			</div>
		</div>

		@section('script')
			<script type="text/javascript" src="{{ asset('assets/fileinput/js/fileinput.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepcker.pt.BR.js') }}"></script>
			<script src="{{ asset('assets/js/select2.min.js') }}"></script>
	    	<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
	    	<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
	    	<script src="{{ asset('assets/js/cep.js')}}"></script>
	    	<script src="{{ asset('assets/js/usuario/produto/produto.js')}}"></script>

		@stop
	@stop