@extends('layout.app')
	@section('conteudo')

		<div class="container" id="conteudo">

			@include('elements.usuario.menu_usuario')

			<!-- Breadcrumbs -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{!! Breadcrumbs::render('produto_usuario.index') !!}
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h2 class="font text-center"><i class="fa fa-th-large"></i> Produtos Postados</h2>
								<h4 class="font text-center">Você também pode conferir se já recebeu alguma proposta clicando no link "Propostas" logo abaixo do produto.</h4>
							</div>
						</div>
					</div>

					<!-- Listagem -->
					<div class="row">
						<div class="trocas">
					    	@forelse($produtos as $key => $produto)
						    	<div class="troca col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<div class="row text-center">
						    			<a href="{{ route('editar.produto', $produto->slug) }}">
						    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
						    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
						    				@else
						    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
						    				@endif
						    			</a>
						    			<a href="{{ route('editar.produto', $produto->slug) }}">
						    				<h3 class="text-center">{{ $produto->produto }}</h3>
						    			</a>

						    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    			<small class="text-center">
							    				Views: {{ $produto->views }}
							    			</small>
						    			</a>

						    			<small>
						    				@if(isset($produto->troca))
						    					Situação:
												{{ $produto->troca->situacao->situacao }}
											@elseif(count($produto->imagens) < 4)
						    					Situação:
												<a href="#">{{'A carregar imagens ('. (4 - count($produto->imagens)).')'}}</a>
											@elseif(count($produto->propostas) > 0)
												<a href="{{ route('propostas.produto', $produto->slug) }}" >Propostas ({{ count($produto->propostas) }})</a>
											@else
												<small>Recebendo propostas</small>
											@endif
						    			</small>
						    		</div>
						    	</div>

						    	@empty
							    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    			<h4><i class="fa fa-frown-o"></i> Você ainda não postou nenhum produto? <a href="{{ route('produto_usuario.novo') }}">Clique aqui e cadastre um item para trocar com outros usuários.</a></h4>
						    		</div>
						    @endforelse
				    	</div>
					</div>
				</div>
			</div>
	@stop