@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/fileinput/css/fileinput.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
	@stop
	<!--FIM DO BUSCADOR-->
		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>
		<!--CONTENT-->
		<div class="container" id="conteudo">
			<!--NOVIDADES-->

			@include('elements.usuario.menu_usuario')

			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-8">
				<div class="formUsuarioAmbiente">
					<h1>Imagens - {{ $produto->produto }}</h1>
					<div class="row">

						<div class="message none">
							<div class="alert alert-info">
								  <strong>Aguarde..</strong> Carregando Arquivos
							</div>
						</div>

						@if(count($produto->imagens) < 2)
							<div class="alert alert-info">
								<?php 
								$qntImagens = (4 - count($produto->imagens));
								?>
								<strong>Informação!</strong> Caro {{ $produto->usuario->nome . ' ' .$produto->usuario->sobrenome  }}, estamos quase lá! Insira {{ $qntImagens }} {{ $qntImagens > 1 ? 'imagens' : 'imagem' }} de seu produto para concluirmos a publicação.
							</div>
						@endif

						<div class="col-sm-12 col-md-3 col-lg-12">
							@if(count($produto->imagens) < 4)
								<form class="formUploadImagemProduto" method="POST">
									<div class="form-group">
										<div class="col-xs-12 col-sm-8 col-md-5 col-lg-6"> 
											<span class="btn btn-default btn-file">
											    Carregar arquivos <input id="fileupload" data-route="{{ route('upload.imagem_produto') }}" name="imagens_produto" class="imagens_produto" type="file" accept="image/jpg, image/jpeg" multiple>
											</span>
											<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}"> 
											<input type="hidden" name="_token" value="{{ csrf_token() }}">

											<div class="form-group"> 
												<div class="col-xs-12 col-sm-8 col-md-5 col-lg-7">
													<button type="submit" class="btn btn-info btPublicarProduto {{ count($produto->imagens) < 4 ? 'none' : '' }}">Publicar Produto</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							@else

								<div class="alert alert-warning">
								  <strong>Atenção!</strong> Caro(a) {{ $produto->usuario->nome . ' ' .$produto->usuario->sobrenome  }}, você já possuí 4 imagens deste produto, apague alguma imagem se quiser adicionar uma imagem diferente.
								</div>

							@endif
						</div>
					</div>

					@if($produto->publicado != 1 && count($produto->imagens) >= 2)
						<div class="formPublicarProduto col-sm-12 col-md-3 col-lg-12">
							<form class="formPublicarProduto" method="POST" action="{{ route('publicar_produto.post') }}">
								<div class="form-group">
									<div class="col-xs-12 col-sm-8 col-md-5 col-lg-6"> 
										<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="form-group">
											<div class="col-xs-12 col-sm-8 col-md-5 col-lg-12">
												<button type="submit" class="btn btn-info btPublicarProduto">Publicar Produto</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					@endif

				    <div class="trocas">

				    	@if(count($produto->imagens) > 0)
					    	@foreach($produto->imagens as $imagem)
					    		@if(is_file($imagem->imagem))
							    	<div class="row">
							    		<div class="troca col-sm-12 col-md-3 col-lg-3">
							    			<a href="javascript:void(0)">
							    				<img src="{{ asset($imagem->imagem) }}" width="200" class="img-rounded" alt="Rounded Image">
							    			</a>
							    			<a href="{{ route('excluir_imagem.produto', $imagem->id_imagem_produto) }}" class="excluirImagem" data-id="{{ $imagem->id_imagem_produto }}">Excluir</a>
							    		</div>
							    	</div>
						    	@endif
						    @endforeach
					 	@endif

				    </div>
				</div>
			</div>
		</div>

		@section('script')
			<script type="text/javascript" src="{{ asset('assets/fileinput/js/fileinput.min.js') }}"></script>
	    	<script src="{{ asset('assets/js/usuario/produto/produto_imagem.js')}}"></script>
		@stop
	@stop