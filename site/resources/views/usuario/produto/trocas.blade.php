@extends('layout.app')
	@section('conteudo')

		<div class="container" id="conteudo">
			<!--MENU DO USUÁRIO-->
			@include('elements.usuario.menu_usuario')


			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
				<div class="formUsuarioAmbiente">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{!! Breadcrumbs::render('minhas_trocas.index') !!}
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h2 class="font text-center"><i class="fa fa-exchange"></i> Trocas Realizadas</h2>
								<h4 class="font text-center">Confira suas trocas realizadas e também nossas sugestões para realizar sua próxima troca.</h4>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="trocas">
						    @forelse($trocas as $troca)
						    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
						    		<a href="{{ route('editar.produto', $troca->slug) }}"><img src="{{ isset($troca->imagens[0]) ? asset($troca->imagens[0]->miniatura) : asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $troca->produto }}"></a>

						    		<a href="{{ route('editar.produto', $troca->slug) }}"><h4>{{ $troca->produto }}</h4></a>
						    	</div>
						    	<div class="troca col-xs-1 col-sm-1 col-md-1 col-lg-4">
									<h2 class="text-center">
										<i class="fa fa-exchange"></i>
									</h2>
									<h4 class="text-center">Trocado com:</h4>
									<h4 class="text-center">{{ $troca->propostas[0]->usuario->nome . ' ' .$troca->propostas[0]->usuario->sobrenome }}</h4>
						    	</div>

						    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
						    		<a href="{{ route('produtos.detalhe', $troca->propostas[0]->produtoProposta->produto->slug) }}"><img src="{{ isset($troca->propostas[0]->produtoProposta->produto->imagens[0]) ? asset($troca->propostas[0]->produtoProposta->produto->imagens[0]->miniatura) : asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $troca->propostas[0]->produtoProposta->produto->produto }}"></a>

						    		<a href="{{ route('produtos.detalhe', $troca->slug) }}">
						    			<h4 class="text-center">{{ $troca->propostas[0]->produtoProposta->produto->produto }}</h4>
						    		</a>
						    	</div>

						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    		<div class="row text-center">
					    				<h4><i class="fa fa-exclamation-triangle"></i> Você não fez nenhuma troca. <a href="{{ route('home.index') }}">Vamos realizar sua primeira troca? Confira as novidades e produtos de seu interesse</a></h4>
					    			</div>
					    		</div>
						    @endforelse

					    	@forelse($propostasAceitas as $produto)
						    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
						    		<a href="{{ route('produtos.detalhe', $produto->slug) }}">
						    			<img src="{{ isset($produto->imagens[0]) && isset($produto->imagens[0]->miniatura) ? asset($produto->imagens[0]->miniatura) : asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
						    		</a>

						    		<a href="{{ route('produtos.detalhe', $produto->slug) }}"><h4>{{ $produto->produto }}</h4></a>
						    	</div>
						    	<div class="troca col-xs-1 col-sm-1 col-md-1 col-lg-4">
									<h2 class="text-center">
										<i class="fa fa-exchange"></i>
									</h2>
									<h4 class="text-center">Trocado com:</h4>
									<h4 class="text-center">{{ $produto->usuario->nome . ' ' .$produto->usuario->sobrenome }}</h4>
						    	</div>

						    	<div class="troca text-center col-xs-12 col-sm-12 col-md-2 col-lg-4">
						    		<a href="{{ route('editar.produto', $produto->propostas[0]->produtoProposta->produto->slug) }}">
						    			<img src="{{ isset($produto->propostas[0]->produtoProposta->produto->imagens[0]) ? asset($troca->propostas[0]->produtoProposta->produto->imagens[0]->miniatura) : asset('assets/imgs/image_facebook.png') }}" width="200" class="img-rounded" alt="{{ $produto->propostas[0]->produtoProposta->produto->produto }}">
						    		</a>

						    		<a href="{{ route('editar.produto', $troca->slug) }}"><h4 class="text-center">{{ $produto->propostas[0]->produtoProposta->produto->produto }}</h4></a>
						    	</div>
								
						    	@empty
						    	<div class="troca col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    		<div class="row text-center">
					    				<h4><i class="fa fa-exclamation-triangle"></i> Você não fez nenhuma troca. <a href="{{ route('home.index') }}">Confira o que está sendo novidade e troque agora seu primeiro produto! Confira</a></h4>
					    			</div>
					    		</div>
						    @endforelse

				    	</div>
					</div>
				</div>
			</div>
		@stop