@extends('layout.app')
	@section('conteudo')
		@section('style')
			<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
			<link rel="stylesheet" href="{{ asset('assets/css/datepicker/bootstrap-datepicker.min.css') }}" />
		@stop
		<!--END MENU-->


		<div class="mensagem navbar-fixed-top none">
			<div class="sucesso container">
				<span>Dados gravados com sucesso</span>
			</div>
		</div>
			<!--CONTENT-->
		<div class="container" id="conteudo">
			<!--NOVIDADES-->
			@include('elements.usuario.menu_usuario')

			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-8">
				<div class="formUsuarioAmbiente">
					<form class="form-horizontal formUsuario" action="{{ route('atualizar_informacoes.post') }}" method="POST">
							<div class="form-group">
								<label class="control-label col-sm-2" for="nome">Nome:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
									<div class="label"></div>
									<input type="text" name="nome" class="form-control" id="nome" value="{{ $usuario->nome }}" placeholder="Nome">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="sobrenome">Sobrenome:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<div class="label"></div>
									<input type="text" class="form-control" name="sobrenome" id="sobrenome" value="{{ $usuario->sobrenome }}" placeholder="Sobrenome">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="data_nascimento">Data de nascimento:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-3"> 
									<div class="label"></div>
									<input type="text" class="form-control" name="data_nascimento" id="data_nascimento" value="{{ $usuario->data_nascimento }}" placeholder="20/09/1985">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="telefone">Telefone:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-3"> 
									<div class="label"></div>
									<input type="text" class="form-control" name="telefone" id="telefone" value="{{ $usuario->telefone }}" placeholder="(00) 9999-9999">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="celular">Celular:</label>
									<div class="col-xs-12 col-sm-8 col-md-5 col-lg-3"> 
										<div class="label"></div>
										<input type="text" class="form-control" name="celular" id="celular" value="{{ $usuario->celular }}" placeholder="(00) 9999-9999">
									</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="interesse">Sexo:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-7"> 
									<div class="label"></div>
									<select name="sexo" class="sexo form-control" id="sexo">
										<option></option>
										<option value="Masculino" {{ $usuario->sexo == 'Masculino' ? 'selected' : '' }}>Masculino</option>
										<option value="Feminino" {{ $usuario->sexo == 'Feminino' ? 'selected' : '' }}>Feminino</option>
										<option value="Outros" {{ $usuario->sexo == 'Feminino' ? 'Outros' : '' }}>Outros</option>
									</select>
								</div>
							</div>

							
							<div class="form-group">
								<label class="control-label col-sm-2" for="email">Email:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-6">
									<div class="label"></div>
									<input type="text" class="form-control" name="email" id="email" value="{{ $usuario->email }}" placeholder="Email">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="interesse">Interesses:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-7"> 
									<select name="interesses[]" class="interesses form-control" data-interesse="{{ route('interesse_usuario.post') }}" data-url="{{ route('retornar.categorias') }}" id="interesse" multiple="multiple">
										<option value="0">Escolha as categorias</option>
										@foreach($interesses as $interesse)
											<optgroup label="{{ $interesse->subCategoria->categoria->categoria }}">
											    <option value="{{ $interesse->subCategoria->id_sub_categoria }}" selected>{{ $interesse->subCategoria->sub_categoria }}</option>
									  		</optgroup>
										@endforeach
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="cep">CEP:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-3"> 
									<div class="label"></div>
									<input type="text" class="form-control" id="cep" name="cep" data-url="{{ route('consultar_localidade.post') }}" id="cep" value="{{ $usuario->cep }}" placeholder="78089-098">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="endereco">Endereço:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-7"> 
									<input type="text" readonly class="form-control" id="endereco" name="endereco" value="{{ old('endereco', $usuario->endereco) }}" placeholder="Rua, Avenida..">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="numero">Número:</label>
								<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"> 
									<input type="text" class="form-control" name="numero" id="numero" value="{{ old('numero', $usuario->numero) }}" placeholder="123">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="complemento">Complemento:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<input type="text" class="form-control" name="complemento" id="complemento" value="{{ $usuario->complemento }}" placeholder="Bloco, apartamento, fundos, casa...">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="bairro">Bairro:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<input type="text" readonly class="form-control" name="bairro" id="bairro" value="{{ $usuario->bairro }}" placeholder="Bairro">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="cidade">Cidade:</label>
								<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5"> 
									<input type="text" readonly class="form-control" id="cidade" value="{{ isset($usuario->cidade->cidade) ? $usuario->cidade->cidade : '' }}" placeholder="Cidade">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="uf">Estado:</label>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"> 
									<input type="text" readonly class="form-control" id="uf" value="{{ isset($usuario->cidade->estado->sigla) ? $usuario->cidade->estado->sigla : '' }}" placeholder="UF">
								</div>
							</div>
							
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id_cidade" value="{{ $usuario->id_cidade }}">
							
							<div class="form-group"> 
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btSalvar btn btn-default">Salvar dados</button>
								</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	@stop

	@section('script')
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/datepicker/bootstrap-datepcker.pt.BR.js') }}"></script>
		<script src="{{ asset('assets/js/select2.min.js') }}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
    	<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js')}}"></script>
    	<script src="{{ asset('assets/js/cep.js')}}"></script>
    	<script src="{{ asset('assets/js/usuario/usuario.js')}}"></script>
    @stop
