@extends('layout.app')
	@section('conteudo')
		<div class="conteudoPadrao pageMensagem container" id="conteudo">
			<!--CATEGORIA-->
			{!! Breadcrumbs::render('mensagens_produto.index', $produto) !!}

						<!-- CONTEUDO -->

			<div class="container" id="produtoeinfo">
				<div class="col-xs-12 col-sm-6 col-md-8 col-lg-6" id="imagemdoproduto">
					<div class="container-fluid">
						<div id="carouselProduct" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								@foreach($produto->imagens as $key => $imagem)
								<div class="item {{ $key == 0 ? 'active' : '' }}">
									<?php
											list($width, $height) = getimagesize(asset($imagem->imagem));
										?>
										@if($width >= 495)
											<img class="img-responsive" src="{{ asset($imagem->imagem) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
										@else
											<img class="img-responsive" src="{{ asset($imagem->imagem) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}" style="position: relative;left: 50%;margin-left: -97px;">
										@endif
								</div>
								@endforeach
							</div>

							<a class="left carousel-control" href="#carouselProduct" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#carouselProduct" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 col-lg-12">
								<div class="infodoproduto">
									<div class="container-fluid">
										<div class="page-header">
											<h1 class="font">{{ $produto->produto }}</h1>
										</div>
										<div class="product-desc">
											<p class="font"><?php echo $produto->descricao_produto; ?></p>
										</div>

										<div class="product-price">
											<h2>Aceita trocar em:</h2>
										</div>

										<div class="produtosDesejados">
										    <p>
											    @foreach($produto->interesses as $interesse)
						                        	<i class="fa fa-tags wow bounceIn"></i><big><a href="#">{{ $interesse->descricao_produto }}</a></big>
						                        @endforeach
					                    	</p>
					                    </div>
					                </div>
								</div>
							</div>
						</div>
					</div>
				</div>

				@if(Auth::user()->id_usuario == $produto->usuario->id_usuario)
					@include('elements.produto.mensagens.usuario_dono_produto')
				@else
					@include('elements.produto.mensagens.usuario_dono_proposta')
				@endif
		</div>



		@section('script')
		    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
		    <script type="text/javascript" src="{{ asset('assets/js/usuario/produto/mensagem.js')}}"></script>
		    <script type="text/javascript" src="{{ asset('assets/js/usuario/produto/proposta.js')}}"></script>
		@stop
	@stop
