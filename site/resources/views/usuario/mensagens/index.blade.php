@extends('layout.app')
	@section('style')
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/timeline.css') }}">
	@stop
	@section('conteudo')
		<div class="container-fluid">
			<div class="conteudoPadrao container" id="conteudo">
					{!! Breadcrumbs::render('mensagens_produto.index', $produto) !!}

				<!-- TITULO DO PRODUTO -->
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1 class="font text-center">
								<i class="fa fa-book"></i> {{ $produto->produto }}
								<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">
							</h1>
						</div>
					</div>
				</div>

				<!---  Informações do produto  -->
				<div class="container-fluid">
					<div id="produtoeinfo">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="imagemdoproduto">
								<h4 class="text-center"><small><i class="fa fa-search-plus"></i> Clique na imagem para ampliar</small></h4>
								<div class="imagensProduto text-center row">
									@foreach($produto->imagens as $imagem)
										<div class="imagem col-xs-12 col-sm-6 col-md-3 col-lg-3">
											<a href="{{ asset($imagem->imagem) }}" class="fancybox" rel="gallery1" title="{{ $produto->produto }}">
												@if(is_file($imagem->miniatura))
													<img class="img-rounded" src="{{ asset($imagem->miniatura) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@else
													<img class="img-rounded" src="{{ asset($imagem->imagem) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@endif
											</a>
										</div>
									@endforeach
								</div>
								@include('elements.social')
							</div>
						</div>
					</div>

					<div class="container">
						<!---  Proposta  -->
						<div class="row">
							<div class="page-header">
								<h2 class="font">Proposta de {{ $proposta->usuario->nome . ' ' . $proposta->usuario->sobrenome }}</h2>
								<div>
									<p>{{ $proposta->proposta }}</p>

									@if($proposta->troca)
										<p>
											<i class="fa fa-caret-right"></i>
											<strong>Situação:</strong> {{ $proposta->troca->situacao->situacao }}
										</p>
									@endif
								</div>

								@if(Auth::user()->id_usuario == $produto->usuario->id_usuario)
									@include('elements.produto.mensagens.usuario_dono_produto')
								@else
									@include('elements.produto.mensagens.usuario_dono_proposta')
								@endif

								<hr>



								<div>
									<h4 class="font">Produto oferecido: <a href="{{ route('produtos.detalhe', $proposta->produtoProposta->produto->slug) }}">{{ $proposta->produtoProposta->produto->produto }}</a></h4>
									@if(isset($proposta->troca))
										<input type="hidden" name="id_produto_troca" value="{{ $proposta->troca->id_produto_troca }}">
									@endif
								</div>

								<div class="row">
									@foreach($proposta->produtoProposta->produto->imagens as $imagem)
										<div class="col-xs-2 col-sm-3 col-md-3 col-lg-3">
											<a href="{{ asset($imagem->imagem) }}" class="fancybox" rel="gallery1" title="{{ $produto->produto }}">
												@if(is_file($imagem->miniatura))
													<img class="img-rounded" src="{{ asset($imagem->miniatura) }}" width="120" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@else
													<img class="img-rounded" src="{{ asset($imagem->imagem) }}" width="120" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@endif
											</a>
										</div>
									@endforeach
								</div>
							</div>
						</div>

						<ul class="timeline">
							@if(!$produto->trocado)
								<li>
									<div class="timeline-badge info"><i class="fa fa-info"></i></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h3 class="timeline-title font">Antes de continuar, leia essas instruções.</h3>
										</div>
										<div class="timeline-body">
											<h4 class="font"><i class="fa fa-hand-o-right"></i> Antes de aceitar uma proposta, procure saber sobre o produto oferecido e sobre o usuário em questão.</h4>
											<h4 class="font"><i class="fa fa-hand-o-right"></i> Depois que você aceitar uma proposta você não poderá mais aceitar nenhuma outra ou trocar mensagens. Por isso, analise bem, pense e depois troque seu produto.</h4>
											<h4 class="font"><i class="fa fa-hand-o-right"></i> Antes de aceitar a proposta procure saber o endereço de entrega, telefone e email da pessoa com quem está negociando.</h4>
											<h4 class="font"><i class="fa fa-hand-o-right"></i> O recebimento e o envio dos produtos em questão são de inteira responsabilidade dos usuários.</h4>
											<h4 class="font"><i class="fa fa-hand-o-right"></i> Lembramos que o Troque Lá não se responsabiliza por produtos com defeitos e trocas mal sucedidas. O que oferecemos é um lugar para conectar pessoas e fazer com elas troquem seus produtos.</h4>
											<h4 class="font"><i class="fa fa-hand-o-right"></i> Os dados informados são de inteira responsabilidade do usuário.</h4>


											<div class="alert alert-success">
													<h4 class="font"><i class="fa fa-hand-spock-o"></i> Dejamos a você uma boa negociação e estamos dispostos a te ajudar sempre que precisar.</h4>
											</div>
										</div>
									</div>
								</li>
								<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="timeline-badge success"><i class="fa fa-paper-plane"></i></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h4 class="timeline-title">Envie uma mensagem</h4>
										</div>
										<div class="timeline-body">
											<div class="row">
								                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-12">
							                        <form class="form-horizontal" method="POST">
							                            <input type="hidden" name="id_produto" value="{{ $proposta->produtoProposta->produto->id_produto }}">
							                            <input type="hidden" name="id_proposta_produto" data-url="{{ route('verifica.mensagemProposta') }}" value="{{ $proposta->id_proposta_produto }}">

							                            <div class="form-group">
							                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							                                    <div class="label"></div>
							                                    <textarea data-action="{{ route('enviar_mensagem.post') }}" class="form-control" name="mensagem" placeholder="Escreva suas dúvidas sobre esta proposta" rows="5" id="comment"></textarea>
							                                </div>
							                            </div>
							                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
							                            <div class="form-group">
							                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							                                    <button type="submit" class="btn btn-danger btEnviarMensagem">Enviar Mensagem</button>
							                                </div>
							                            </div>
							                        </form>
								                </div>
								            </div>
										</div>
									</div>
								</li>
							@else
								<li>
									<div class="timeline-badge success"><i class="fa fa-check"></i></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<div class="alert alert-success">
												<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 10h</small></p>
												<h4 class="timeline-title font">Parabéns! A proposta foi aceita e troca está realizada!</h4>
											</div>
										</div>
									</div>
								</li>
							@endif


							@foreach($proposta->mensagens as $mensagem)
								<li class="{{ Auth::user()->id_usuario == $mensagem->id_usuario ? '' : 'timeline-inverted'}} col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="timeline-badge warning"><i class="fa fa-align-justify"></i></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h4 class="timeline-title">{{ Auth::user()->id_usuario != $mensagem->id_usuario ? $mensagem->usuario->nome . ' ' . $mensagem->usuario->sobrenome : 'Você' }} disse:</h4>
											<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{ $mensagem->criado_em }}</small></p>
										</div>
										<div class="timeline-body">
											<p><?php echo $mensagem->mensagem; ?></p>
										</div>
									</div>
								</li>
							@endforeach
							<!--
							<li class="timeline-inverted">
								<div class="timeline-badge info"><i class="fa fa-thumbs-up"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Negociar novamente</h4>
									</div>
									<div class="timeline-body">
										<p>Você pode negociar novamente com este usuário. Caso queria iniciar uma nova negociação, basta clicar no botão abaixo.</p>
									</div>
									<hr>
									<div class="btn-group">
							            <button type="button" class="btn btn-primary btn-sm">
							            	Negociar novamente com este usuário
							              	<i class="fa fa-thumbs-o-up"></i>
							            </button>
							          </div>
								</div>
							</li>

							<li class="timeline-inverted">
								<div class="timeline-badge danger"><i class="fa fa-times"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Proposta recusada</h4>
										<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> há 11 minutos atrás</small></p>
									</div>
									<div class="timeline-body">
										<h3 class="font">Rafael Franke recusou a proposta</h3>
									</div>
								</div>
							</li>-->


							<!-- Proposta do usuário -->
							<li class="{{ Auth::user()->id_usuario == $proposta->usuario->id_usuario ? '' : 'timeline-inverted' }} col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="timeline-badge"><i class="fa fa-flag"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">{{ $proposta->usuario->nome . ' ' .$proposta->usuario->sobrenome }} fez uma proposta:</h4>
										<p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Em {{  $proposta->criado_em}}</small></p>
									</div>
									<div class="timeline-body">
										<p>{{ $proposta->proposta }}</p>
									</div>

									<hr>

									<div class="timeline-heading">
										<h4 class="timeline-title">Produto oferecido: {{ $proposta->produtoProposta->produto->produto }}</h4>
									</div>

									<div class="timeline-body">
										@foreach($proposta->produtoProposta->produto->imagens as $imagem)
											<div class="imagem col-xs-2 col-sm-3 col-md-3 col-lg-3">
												<a href="{{ asset($imagem->imagem) }}" class="fancybox" rel="gallery1" title="{{ $produto->produto }}">
													@if(is_file($imagem->miniatura))
														<img class="img-rounded" src="{{ asset($imagem->miniatura) }}" width="100" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
													@else
														<img class="img-rounded" src="{{ asset($imagem->imagem) }}" width="100" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
													@endif
												</a>
											</div>
										@endforeach
									</div>
								</div>
							</li>
						</ul>
					</div>

				</div>

			</div>
		</div>



		@section('script')
		    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
		    <script type="text/javascript" src="{{ asset('assets/js/usuario/produto/mensagem.js')}}"></script>
		    <script type="text/javascript" src="{{ asset('assets/js/usuario/produto/proposta.js')}}"></script>
		@stop
	@stop
