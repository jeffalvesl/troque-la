<!DOCTYPE html>
<html lang="en">

<head>
    @include('elements.institucional.head')
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="sobrePage">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="{{ route('home.index') }}">Troque Lá</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Guia</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#addproduto">Adicionar Produto</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#trocas">Trocas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#excecoes">Exceções</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#social">Social</a>
                    </li>
                    @if(Auth::user())
                        @include('elements.box_menu_logado')
                    @else
                        <li>
                            <a class="visible-xs visible-ms" href="{{ route('login.index') }}">Login</a>
                        </li>
                        <li class="dropdown hidden-xs hidden-md">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                            @include('elements.login')
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header class="sobrenos" id="about">
        <div class="header-content">
            <div class="header-content-inner text-justify">
                <h1 class="text-center">Guia Prático ao Usuário do Troque Lá</h1>
                <hr class="light">
                <p class="text-center">Obrigado por conectar-se conosco! Esse Guia Prático tem como intuito explicar como o nosso site funciona e mostrar o que você pode ou não fazer.</p>
            </div>
            <hr>
            <a href="#addproduto" class="btn btn-info btn-xl page-scroll">Iniciar aprendizado</a>
        </div>
    </header>

    <section class="bg-primary aboutAjuda" id="addproduto">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2 class="section-heading text-center">ADICIONAR PRODUTO</h2>
                    <hr class="light">
                    <p class="text-center">O troque lá é um serviço de trocas online, onde você pode entrar em contato com pessoas da sua cidade ou estado que possuam produtos de seu interesse.</p>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-6 col-sm-6 text-left">
                        <h3>O que você pode fazer:</h3>
                        <br>
                        <ul id="cando" style="margin-left: -40px !important;">
                            <li class="list-unstyled"><p><i class="fa fa-check"></i> Publicar produtos próprios;</p></li>
                            <li class="list-unstyled"><p><i class="fa fa-check"></i> Entrar em contato com outros usuários;</p></li>
                            <li class="list-unstyled"><p><i class="fa fa-check"></i> Dar sugestões de melhorias;</p></li>
                            <li class="list-unstyled"><p><i class="fa fa-check"></i> Completar uma oferta com dinheiro;</p></li>
                            <li class="list-unstyled"><p><i class="fa fa-check"></i> Compartilhar sua oferta com os seus amigos;</p></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-sm-6 text-right">
                        <h3>O que você não pode fazer:</h3>
                        <br>
                        <ul>
                            <li class="list-unstyled"><p> Publicar produtos que não sejam seus; <i class="fa fa-times"></i></p></li>
                            <li class="list-unstyled"><p> Vender produtos ou anunciar serviços; <i class="fa fa-times"></i></p></li>
                            <li class="list-unstyled"><p> Praticar atos descriminatórios ou ofensivos; <i class="fa fa-times"></i></p></li>
                            <li class="list-unstyled"><p> Postar imagens que não condizem com o anúncio; <i class="fa fa-times"></i></p></li>
                            <li class="list-unstyled"><p> Deixar link que remeta a outros serviços; <i class="fa fa-times"></i></p></li>
                        </ul>  
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <section id="trocas">
            <div class="container">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="section-heading text-center">TROCAS</h2>
                    <hr>
                    <p class="text-center">Após adicionar o seu produto, você pode trocá-lo por vários outros anunciados. Veja como é fácil:</p>
                    <br>

                    <div class="row">
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".1s"></i>
                                <h3>Publique</h3>
                                <p class="text-muted">Tem algo que não usa mais? Anuncie!</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-arrow-right wow bounceIn text-primary" data-wow-delay=".2s"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-share-alt wow bounceIn text-primary" data-wow-delay=".3s"></i>
                                <h3>Compartilhe</h3>
                                <p class="text-muted">Talvez os seus amigos tenham interesse no seu produto.</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-arrow-right wow bounceIn text-primary" data-wow-delay=".4s"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-search wow bounceIn text-primary" data-wow-delay=".5s"></i>
                                <h3>Veja produtos</h3>
                                <p class="text-muted">Navegue pelo site e veja todos os produtos cadastrados.</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-arrow-right wow bounceIn text-primary" data-wow-delay=".6s"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-envelope-o wow bounceIn text-primary" data-wow-delay=".7s"></i>
                                <h3>Envie uma proposta</h3>
                                <p class="text-muted">Gostou de um produto? Envie uma proposta.</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-arrow-right wow bounceIn text-primary" data-wow-delay=".8s"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-exchange wow bounceIn text-primary" data-wow-delay=".9s"></i>
                                <h3>Troque</h3>
                                <p class="text-muted">Proposta aceita? Show de bola! Hora de trocar.</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-arrow-right wow bounceIn text-primary" data-wow-delay="1.1s"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-5 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-users wow bounceIn text-primary" data-wow-delay="1.2s"></i>
                                <h3>Indique-nos</h3>
                                <p class="text-muted">Seus amigos também pode ter algo que você quer. Que tal chamá-los para ver?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section id="excecoes">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2 class="section-heading text-center">EXCEÇÕES</h2>
                    <hr>
                    <p class="text-center">Aqui vai uma lista de coisas que você não pode fazer em nosso site:</p>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-home wow bounceIn text-primary" data-wow-delay=".1s"></i>
                                <h3>Imóveis</h3>
                                <p class="text-muted">Você não pode trocar a sua casa</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-car wow bounceIn text-primary" data-wow-delay=".2s"></i>
                                <h3>Automóveis</h3>
                                <p class="text-muted">Você não pode trocar o seu carro ou sua moto</p>
                            </div>
                        </div>
                    </div>
                    <hr class="light">
                    <p class="text-center">Casa, apartamento, carro e moto são coisas que envolvem bastante burocracia para serem trocadas. Nossa ideia é oferecer um serviço simples, logo cremos que não seria uma boa oferecer a troca dos mesmos.</p>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3 text-center">
                            <div class="service-box">
                                <i class="fa fa-4x fa-camera-retro wow bounceIn text-primary" data-wow-delay=".3s"></i>
                                <h3>Posso usar qualquer imagem?</h3>
                                <p class="text-muted">Pode não! Expressamente proibido a utilização de imagens que nada tenha a ver com o produto e qualquer imagem de cunho pornográfico no site.</p>
                            </div>
                            <hr>
                            <h3>Faça um bom uso do Troque Lá!</h3>
                            <div class="service-box">
                                <i class="fa fa-4x fa-smile-o wow bounceIn" data-wow-delay=".4s"></i>
                            </div>
                            <hr class="light">
                            @if(Auth::user())
                                <a href="{{ route('produto_usuario.novo') }}" class="btn btn-success btn-xl page-scroll">Adicionar produto</a>
                            @else
                                <a href="{{ route('cadastro.index') }}" class="btn btn-success btn-xl page-scroll">Cadastre-se</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <aside class="bg-dark">
        @include('elements.institucional.social')
    </aside>

    <section id="contact">
        @include('elements.institucional.rodape')
    </section>

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{ asset('assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.fittext.js')}}"></script>
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    
    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/js/creative.js') }}"></script>
    @yield('script')



</body>

</html>
