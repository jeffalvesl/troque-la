<!--RODAPE-->


<div class="container-fluid">
	<section class="rodape  hidden-sm hidden-xs">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rodapeSocial">
            <div class="row">
                <div class="socialItens socialItensInicio">
                    @include('partials.social')                
                </div>
            </div>
        </div>
	</section>
</div>