
<section id="contact" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="container">
        <div class="row">
            <p class="row-centered">2015© Troque Lá plataforma de trocas onlines. Todos os direitos reservados a J.F Company.</p>
        </div>
        <footer class="row rodapeDesk hidden-xs hidden-md">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('sobre.index') }}">Sobre</a>
                </li>
                <li>
                    <a href="{{ route('termo.index') }}">Termos</a>
                </li>
            </ul>
        </footer>
        <footer class="rodapeMobile visible-xs visible-md">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('sobre.index') }}">Sobre</a>
                </li>
                <li>
                    <a href="{{ route('termo.index') }}">Termos</a>
                </li>
            </ul>
        </footer>
    </div>
</section>
