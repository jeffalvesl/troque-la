<div class="btn-group wishlist col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="proposta">
		<form class="formProposta form-horizontal" method="POST" action="{{ route('proposta_produto.post') }}">

			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<label class="control-label" for="nome">Produto*:</label>
					<select  name="produto_proposta_produto" class="produto_proposta_produto form-control"  data-route="{{ route('interesse_usuario_troca.get') }}" id="produtos_interesse">
						@foreach($produtosUsuarioLogado as $produtoUsuario)
							<option value="{{ $produtoUsuario->id_produto }}">{{ $produtoUsuario->produto }}</option>
						@endforeach
					</select>
					<small>Informe o produto que deseja oferecer em troca deste.</small>
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<label class="control-label" for="nome">Proposta*:</label>
					<textarea class="form-control" name="proposta" placeholder="Descreva sua proposta de troca. Especifique o produto que tem e coloque o link a baixo." rows="10" id="comment"></textarea>
				</div>
			</div>

			<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<button type="submit" class="btn btn-danger btLancarProposta">
			<i class="fa fa-paper-plane-o"></i> Enviar proposta
			</button>

		</form>
	</div>
</div>
