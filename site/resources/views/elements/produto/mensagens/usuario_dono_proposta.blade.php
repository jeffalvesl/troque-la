<!--<div class="col-xs-12 col-sm-6 col-md-8 col-lg-6">
	<div class="row">
		<h3 class="font">Sua proposta</h3>

		<form class="form-horizontal" method="POST">
			<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">
				<div>
					<small>12/09/2015 as 10:20</small>
					<strong> - Proposta</strong>
					<p>{{ $proposta->proposta }}</p>
					<p><strong>Produto oferecido: </strong> <a href="{{ route('produtos.detalhe', $proposta->produtoProposta->produto->slug) }}">{{ $proposta->produtoProposta->produto->produto }}</a></p>

					@if(!isset($proposta->troca) || $proposta->troca->situacao->id_situacao <> 4)
						@if(!$proposta->aceita)
							@include('elements.modal_mais_informacoes', array('proposta' => $proposta))
						@else
							<p>
								

								<!---@if($proposta->troca->situacao->id_situacao <> 4)
									@include('elements.mensagem.modal_confirma', array('id_modal' => 'cancelar_troca', 'mensagem' => 'Deseja mesmo cancelar esta troca?', 'classeBt' => 'btCancelarTroca', 'action' => route('cancelar_troca.post')))
									<a href="#" class="btn btn-info"  data-action="{{ route('cancelar_troca.post') }}" data-proposta="{{ $proposta->id_proposta_produto }}" data-toggle="modal" data-target="#cancelar_troca"  style="background: #ea6464;border: none;">Cancelar Troca</a>
								@endif



								<input type="hidden" name="id_produto_troca" value="{{ $proposta->troca->id_produto_troca }}">
								@if($proposta->troca->situacao->id_situacao == 2)
									<a href="javascript:void(0)" data-action="{{ route('troca_realizada.post') }}" class="btn btn-info btTrocaRealizada">Troca realizada</a>
									<a href="javascript:void(0)" class="btn btn-info btVerEndereco" data-toggle="modal" data-target="#modal_endereco_troca">Endereço de entrega</a>
									@include('elements.modal_endereco_troca', array('usuario' => $produto->usuario))
								@endif

							</p>

						@endif
					@endif


					@if($proposta->troca)
						<p>
							<strong>Situação:</strong> {{ $proposta->troca->situacao->situacao }}
						</p>
					@endif

				</div>

				<hr>
		</form>
	</div>
</div>-->

@if(isset($proposta->troca) && $proposta->troca->situacao->id_situacao == 1)
	@include('elements.mensagem.modal_confirma', array('id_modal' => 'confirmar_troca', 'mensagem' => 'Deseja mesmo realizar a troca?', 'classeBt' => 'btConfirmarTroca', 'action' => route('confirmar_troca.post')))
	<a href="#" data-toggle="modal" data-target="#confirmar_troca" class="btn btn-info">Confirmar troca</a>
@endif