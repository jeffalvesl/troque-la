@foreach($proposta->mensagens as $key => $mensagem)
	<div>
		<a href="#">
			@if(Auth::user()->id_usuario == $mensagem->usuario->id_usuario)
				<h4><strong>Você</strong></h4>
			@else
				<h4><strong>{{ $mensagem->usuario->nome . ' ' . $mensagem->usuario->sobrenome }}</strong></h4>
			@endif
		</a>

		<small>{{ $mensagem->criado_em }}</small>
		<strong> - Mensagem</strong>
		<p class="{{ $key == (count($proposta->mensagens) - 1) && Auth::user()->id_usuario != $mensagem->usuario->id_usuario ? 'bg-warning' : '' }}">{{ $mensagem->mensagem }}</p>
		@if(Auth::user()->id_usuario == $proposta->usuario->id_usuario && Auth::user()->id_usuario !== $mensagem->usuario->id_usuario || Auth::user()->id_usuario == $produto->usuario->id_usuario && Auth::user()->id_usuario != $mensagem->usuario->id_usuario)
			<p>
				<a href="#" class="btn btn-info btResponder" data-toggle="modal" data-mensagem="{{ $mensagem->id_mensagem_proposta_usuario }}" data-target="#enviar_mensagem_proposta{{ $mensagem->id_mensagem_proposta_usuario }}">Responder</a>
			</p>
			@include('elements.modal_enviar_mensagem', array('mensagem' => $mensagem, 'proposta' => $proposta))
		@endif
	</div>

	<hr class="light">
@endforeach