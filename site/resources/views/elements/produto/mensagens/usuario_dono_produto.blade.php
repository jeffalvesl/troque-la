<!---<div class="row">
	<form class="form-horizontal" method="POST">
		<input type="hidden" name="id_produto" value="{{ $produto->id_produto }}">
			<div>

				@if(!isset($proposta->troca) || $proposta->troca->situacao->id_situacao <> 4)
					@if(!$proposta->aceita)
						<p>
							@include('elements.mensagem.modal_confirma', array('id_modal' => 'aceitar_proposta', 'mensagem' => 'Deseja mesmo aceitar essa proposta?', 'classeBt' => 'btAceitarProposta', 'action' => route('aceitar_proposta.post')))
							<a href="#" data-proposta="{{ $proposta->id_proposta_produto }}" data-action="{{ route('aceitar_proposta.post') }}" data-toggle="modal" data-target="#aceitar_proposta" class="btn btn-info">Aceitar proposta</a>
						</p>
					@else
						<p>

							<input type="hidden" name="id_produto_troca" value="{{ $proposta->troca->id_produto_troca }}">

							<!--@if($proposta->troca->situacao->id_situacao <> 4)
								@include('elements.mensagem.modal_confirma', array('id_modal' => 'cancelar_troca', 'mensagem' => 'Deseja mesmo cancelar esta troca?', 'classeBt' => 'btCancelarTroca', 'action' => route('cancelar_troca.post')))
								<a href="#" class="btn btn-info"  data-action="{{ route('cancelar_troca.post') }}" data-proposta="{{ $proposta->id_proposta_produto }}" data-toggle="modal" data-target="#cancelar_troca"  style="background: #ea6464;border: none;">Cancelar Troca</a>
							@endif

							@if($proposta->troca->situacao->id_situacao == 2)
								<a href="javascript:void(0)" data-action="{{ route('troca_realizada.post') }}" class="btn btn-info btTrocaRealizada">Troca realizada</a>
								<a href="javascript:void(0)" class="btn btn-info btVerEndereco" data-toggle="modal" data-target="#modal_endereco_troca">Endereço de entrega</a>
								@include('elements.modal_endereco_troca', array('usuario' => $proposta->usuario))
							@endif


						</p>
					@endif
				@endif

			</div>

			<hr>
	</form>
</div>-->

@if(!$proposta->aceita)
	<p>
		@include('elements.mensagem.modal_confirma', array('id_modal' => 'aceitar_proposta', 'mensagem' => 'Deseja mesmo aceitar essa proposta?', 'classeBt' => 'btAceitarProposta', 'action' => route('aceitar_proposta.post')))
		<a href="#" data-proposta="{{ $proposta->id_proposta_produto }}" data-action="{{ route('aceitar_proposta.post') }}" data-toggle="modal" data-target="#aceitar_proposta" class="btn btn-danger">Aceitar proposta</a>
	</p>
@endif