@if(isset($sobre->texto))
	<div class="container">
	    <div class="row">
	        <div class="col-lg-8 col-lg-offset-2 text-center">
	            <p class="font text-faded" style="font-size: 20px;"><?php echo strip_tags($sobre->texto) ?></p>
	            <a href="{{ route('sobre.index') }}" class="btn btn-default btn-xl">Sobre</a>
	        </div>
	    </div>
	</div>
@endif