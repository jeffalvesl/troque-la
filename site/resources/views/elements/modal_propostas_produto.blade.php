<div id="modal_propostas_produto" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="font">Propostas - {{ $produto->produto }}</h2>
            </div>

            <div class="modal-body">
                <div class="list-group">
                    @if(count($produto->propostas) > 0)
                        @foreach($produto->propostas as $proposta)
                            <a href="{{ route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]) }}" class="list-group-item">{{ $proposta->usuario->nome . ' ' .$proposta->usuario->sobrenome  }} - <small>{{ substr($proposta->proposta, 0, 99) }}...</small></a>
                        @endforeach
                    @else
                        <span>Seu produto ainda não recebeu propostas</span>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>