<div class="compartilhe">
	<div class="row-centered">
		<h3>Compartilhe:</h3>
		<div class="socialItens socialItensInicio">
            <ul class="social-network social-circle">
                <li>
                    <a href="javascript:void(0)" data-href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}" data-layout="button_count" id="fbShare" class="icoFacebook fb-share-button compartilhar" title="Compartilhar no Facebook">
                        <i class="fa fa-facebook"></i>
                    </a>

                </li>
                <li>
                    <a href="javascript:void(0)" data-href="http://twitter.com/home?status={{ $produto->produto . ' - ' . route('produtos.detalhe', $produto->slug) }}" id="ttShare" class="icoTwitter twitter-share-button compartilhar" title="Compartilhar no Twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-href="https://plus.google.com/share?url={{ route('produtos.detalhe', $produto->slug) }}" id="gpShare" class="icoGoogle compartilhar" title="Compartilhar no Google +">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>                
        </div>
	</div>
</div>