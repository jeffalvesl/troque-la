<ul id="login-dp" class="dropdown-menu">
    <li>
         <div class="row">
                <div class="col-md-12">
                    <span>Via</span>
                    <div class="social-buttons">
                        <a href="{{ route('cadastro.social', 'facebook') }}" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                    </div>
                    <span>ou</span>
                     <form class="formLogin form" role="form" method="POST" id="login-nav" action="{{ route('logar.post') }}">
                            <div class="form-group">
                                 <div class="label"></div>
                                 <input type="email" name="email" class="form-control" id="email" placeholder="Seu email">
                            </div>
                            <div class="form-group">
                                 <div class="label" style="margin-top: 65px;"></div>
                                 <input type="password" data-url="{{ route('verifica_credenciais.post') }}" class="form-control" id="password" name="password" placeholder="Senha">
                                 <div class="help-block text-right"><a href="{{ route('login.esqueci') }}">Esqueceu a senha?</a></div>
                            </div>

                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                            <div class="form-group">
                                 <button type="submit" class="btn btn-success btn-block">Entrar</button>
                            </div>

                            <div class="checkbox">
                                 <label>
                                 <input type="checkbox" name="manter_logado" value="1"> Mantenha-me logado
                                 </label>
                            </div>
                     </form>
                </div>
                <div class="bottom text-center">
                    <h4>
                        <a href="{{ route('cadastro.index') }}"><b>Cadastre-se</b></a>
                    </h4>
                </div>
         </div>
    </li>
</ul>
