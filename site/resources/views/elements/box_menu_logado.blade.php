<li>
    <a class="visible-xs visible-ms" href="#">Sair</a>
</li>
 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->nome . ' ' . Auth::user()->sobrenome }} <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li class="row-centered"><a href="{{ route('usuario.index') }}"><i class="icon-cog"></i> Minhas informações</a></li>
        <li class="row-centered">
        	<a href="javascript:void(0)" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
        		<i class="icon-envelope"></i>Mensagens 
        	</a>
        </li>
        <li class="row-centered"><a href="{{ route('reportar_erro.index') }}"><i class="icon-envelope"></i>Reportar erro</a></li>
        <li class="row-centered"><a href="{{ route('logout.get') }}"><i class="icon-envelope"></i>Sair</a></li>
    </ul>
</li>