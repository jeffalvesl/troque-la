<div id="modal_endereco_troca" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="font">Endereço para envio - {{ $usuario->nome . ' ' .$usuario->sobrenome }}</h2>
            </div>

            <div class="modal-body">
                <div class="list-group">
                    <big>Endereço: {{ $usuario->endereco }} - N {{ $usuario->numero }} - Bairro {{ $usuario->bairro }}</big>
                </div>

                <div class="list-group">
                    <big>Cidade: {{ $usuario->cidade->cidade }} - {{ $usuario->cidade->estado->estado }}</big>
                </div>

                <div class="list-group">
                    <big>Complemento: {{ $usuario->complemento != '' ?  $usuario->complemento : 'Sem complemento' }}</big>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>