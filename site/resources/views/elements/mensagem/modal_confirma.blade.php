<div class="modal fade" id="{{ $id_modal }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h2>{{ $mensagem }}</h2>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-default" data-dismiss="modal">Não</button>
                <button type="button" class="btn btn-info btn-default {{ $classeBt }}" data-proposta="{{ $proposta->id_proposta_produto }}" data-action="{{ $action }}">Sim</button>
            </div>
        </div>
    </div>
</div>