<div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
	<div class="row">
  		<ol class="breadcrumb">
			<li><a href="#">Início</a></li>
			<li><a href="#">{{ $produto->subCategoria->categoria->categoria }}</a></li>
			<li><a href="#">{{ $produto->subCategoria->sub_categoria }}</a></li>
			<li class="active">{{ $produto->produto }}</li>
		</ol>
	</div>
</div>