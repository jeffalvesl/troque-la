<div class="col-md-12">
    <h2 class="font text-center">Entrar</h2>
    <div class="social-buttons">
        <a href="{{ route('cadastro.social', 'facebook') }}" class="btn btn-fb">
            <i class="fa fa-facebook"></i> Facebook
        </a>
    </div>
    <h2 class="font text-center">ou</h2>
     <form class="formLogin" method="POST" action="{{ route('logar.post') }}">
        <div class="form-group">
             <div class="label"></div>
             <label>Email:</label>
             <input type="email" name="email" class="form-control" id="email" placeholder="Seu email">
        </div>
        <div class="form-group">
             <div class="label" style="margin-top: 65px;"></div>
             <label>Senha:</label>
             <input type="password" data-url="{{ route('verifica_credenciais.post') }}" class="form-control" id="password" name="password" placeholder="Senha">
             <div class="help-block text-right"><a href="{{ route('login.esqueci') }}">Esqueceu a senha?</a></div>
        </div>

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="form-group">
            <input type="submit" class="btn btn-success btn-block width100" value="Entrar">
        </div>
     </form>
</div>

<div class="bottom text-center">
    <h3>
        <a href="{{ route('cadastro.index') }}"><b>Cadastre-se</b></a>
    </h3>
</div>

<div class="col-md-12">
    <h2 class="font text-center">Quer receber novas ofertas de troca?</h2>
     <form class="form" role="form" method="POST" id="login-nav">
        <div class="form-group">
             <div class="label"></div>
             <input type="email" name="email" class="form-control text-center" id="email" placeholder="Insira seu email e receba diariamente">
        </div>

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="form-group">
            <div class="social-buttons text-center">
                <button type="submit" class="btn btn-success width100">Quero receber ofertas de troca diariamente</button>
            </div>
        </div>
     </form>
</div>