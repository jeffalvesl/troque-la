<div id="enviar_mensagem_proposta{{ $mensagem->id_mensagem_proposta_usuario }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="infoProposta modal-header">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Responder mensagem de {{ $mensagem->usuario->nome . ' ' . $mensagem->usuario->sobrenome }}</h4>
                    
                </div>
                
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-12">
                    <div class="formMaisInfoProposta">
                        <form class="form-horizontal" method="POST">

                            <input type="hidden" name="id_produto" value="{{ $proposta->produtoProposta->produto->id_produto }}">
                            <input type="hidden" name="id_proposta_produto" value="{{ $proposta->id_proposta_produto }}">

                            <div class="form-group">
                                <label class="control-label" for="mensagem">Resposta:</label>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                                    <div class="label"></div>
                                    <textarea data-action="{{ route('enviar_mensagem.post') }}" class="form-control" id="{{ $mensagem->id_mensagem_proposta_usuario }}" name="mensagem" placeholder="Escreva suas dúvidas sobre esta proposta" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                        
        
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
                            <div class="form-group"> 
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <button type="submit" class="btn btn-info btEnviarMensagem">Enviar Mensagem</button>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>