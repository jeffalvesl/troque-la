<nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
	    	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ route('home.index') }}">Troque Lá</a>
		</div>

		<div class="collapse navbar-collapse js-navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown mega-dropdown">
					<a href="javascript:void(0)" class="dropdown-toggle categorias" data-toggle="dropdown">
						<i class="fa fa-clone"></i> Categorias <span class="caret"></span>
					</a>

					<ul class="dropdown-menu mega-dropdown-menu row">
						<li class="col-sm-3">
							<ul>
								<li class="dropdown-header"><h4>Em destaque</h4></li>
	                            @include('partials.destaques_menu')

	                            <li class="divider"></li>

	                            <li>
	                            	<a href="{{ route('sobre.index') }}">
	                            		Sobre o troque.la
	                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	                            	</a>
	                            </li>
	                            <li>
	                            	<a href="{{ route('guia.index') }}">
	                            		Como funciona
	                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	                            	</a>
	                            </li>
	                            <li>
	                            	<a href="{{ route('termo.index') }}">
	                            		Termos de uso
	                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	                            	</a>
	                            </li>

	                            <li>
	                            	<a href="{{ route('privacidade.index') }}">
	                            		Políticas de privacidade
	                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	                            	</a>
	                            </li>

	                            <li>
	                            	<a href="{{ route('sobre.index') }}#help">
	                            		Ajuda
	                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
	                            	</a>
	                            </li>
							</ul>
						</li>
						@include('partials.menu_categorias')
					</ul>
				</li>
				@if(Auth::user())
		            @include('partials.menu_usuario_logado')
				@endif
				<li><a href="{{ route('sobre.index') }}#contato"><i class="fa fa-envelope-o"></i> Contato</a></li>
				@if(!Auth::user())
					<li><a href="{{ route('sobre.index') }}#help"><i class="fa fa-comments-o"></i> Ajuda</a></li>
				@endif
			</ul>

	        <ul class="nav navbar-nav navbar-right">
	            <li class="dropdown hidden-xs hidden-md">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-location-arrow"></i> <b>{{ !is_null(Cookie::get('cidade')) ? Cookie::get('cidade')['cidade'] : 'Cidade' }}</b> <span class="caret"></span></a>
	                @include('elements.cidades')
	            </li>

	        	@if(!Auth::user())
			        <li>
		                <a class="visible-xs visible-ms" href="{{ route('login.index') }}">Login</a>
		            </li>
		            <li class="dropdown hidden-xs hidden-md open">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="entrar"><i class="fa fa-sign-in"></i> <b>Entrar</b> <span class="caret"></span></a>
		                @include('elements.login')
		            </li>

	        	@endif


	            <!--LOGOFF-->
	            @if(Auth::user())
		            <li>
	                    <a class="visible-xs visible-md" href="{{ route('usuario.index') }}">
	                        <i class="fa fa-info-circle"></i> Meus Dados
	                    </a>
	                    <a class="visible-xs visible-md" href="javascript:void(0)" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
	                        <i class="fa fa-inbox"></i> Mensagens
	                    </a>
	                    <li><a class="visible-xs visible-md" href="{{ route('sobre.index') }}#help"><i class="fa fa-comments-o"></i> Ajuda</a></li>
	                    <li>
		                    <a class="visible-xs visible-md" href="{{ route('logout.get') }}">
		                        <i class="fa fa-sign-out"></i> Sair
		                    </a>
	                    </li>
	                </li>
	             	<li class="dropdown hidden-xs hidden-md"><a href="{{ route('usuario.index') }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->nome . ' ' . Auth::user()->sobrenome }} <b class="caret"></b></a>
		                <ul class="dropdown-menu">
		                    <li><a href="{{ route('usuario.index') }}"><i class="fa fa-info-circle"></i> Meus Dados</a></li>
		                    <li>
		                        <a href="javascript:void(0)" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
		                            <i class="fa fa-inbox"></i> Mensagens
		                        </a>
		                    </li>
		                    <li><a href="{{ route('sobre.index') }}#help"><i class="fa fa-comments-o"></i> Ajuda</a></li>
		                    <li><a href="{{ route('reportar_erro.index') }}"><i class="fa fa-bug"></i> Reportar erro</a></li>
		                    <li><a href="{{ route('logout.get') }}"><i class="fa fa-sign-out"></i> Sair</a></li>
		                </ul>
		            </li>
		        @endif

	      	</ul>
		</div>

    </div>

</nav>
