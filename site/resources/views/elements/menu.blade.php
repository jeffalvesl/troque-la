    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="{{ route('home.index') }}">Troque Lá</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="{{ route('sobre.index') }}">Sobre</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('guia.index') }}">Como funciona</a>
                    </li>
                    @if(Auth::user())
                        <li>
                            <a class="visible-xs visible-ms" href="{{ route('usuario.index') }}">
                                <i class="icon-cog"></i> Meus Dados
                            </a>
                            <a class="visible-xs visible-ms" href="javascript:void(0)" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
                                <i class="fa fa-inbox"></i> Mensagens
                            </a>
                            <a class="visible-xs visible-ms" href="{{ route('logout.get') }}">
                                <i class="fa fa-sign-out"></i> Sair
                            </a>
                        </li>
                        <li class="dropdown"><a href="{{ route('usuario.index') }}" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->nome . ' ' . Auth::user()->sobrenome }} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('usuario.index') }}"><i class="fa fa-info-circle"></i> Meus Dados</a></li>
                                <li>
                                    <a href="javascript:void(0)" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
                                        <i class="fa fa-inbox"></i> Mensagens
                                    </a>
                                </li>
                                <li><a href="{{ route('reportar_erro.index') }}"><i class="fa fa-bug"></i> Reportar erro</a></li>
                                <li><a href="{{ route('logout.get') }}"><i class="fa fa-sign-out"></i> Sair</a></li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a class="visible-xs visible-ms" href="{{ route('login.index') }}">Entrar</a>
                        </li>
                        <li class="dropdown hidden-xs hidden-ms">
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><b>Entrar</b> <span class="caret"></span></a>
                            @include('elements.login')
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
