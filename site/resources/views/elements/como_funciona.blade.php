<div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Como funciona</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-pencil-square-o wow bounceIn text-primary"></i>
                        <h3>Cadastre-se</h3>
                        <p class="text-muted">Cadastrando-se você pode interagir com os anúncios</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".1s"></i>
                        <h3>Publique</h3>
                        <p class="text-muted">Tem algo que não usa mais? Que tal anunciar?</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-exchange wow bounceIn text-primary" data-wow-delay=".2s"></i>
                        <h3>Troque</h3>
                        <p class="text-muted">Achou algo que te interessou? Mande uma proposta e troque.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-users wow bounceIn text-primary" data-wow-delay=".3s"></i>
                        <h3>Indique-nos</h3>
                        <p class="text-muted">Seus amigos também pode ter algo que você quer. Que tal chamá-los para ver?</p>
                    </div>
                </div>
            </div>
        </div>