<!--RODAPE-->
<div class="container-fluid">
	<section class="rodape  hidden-sm hidden-xs">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rodapeSocial">
            <div class="row">
                <div class="socialItens socialItensInicio">
                    <ul class="social-network social-circle">
                        <li>
                            <a href="#" class="icoFacebook" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="icoTwitter" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="icoGoogle" title="Google +">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="icoInstagram" title="Intagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	</section>
</div>