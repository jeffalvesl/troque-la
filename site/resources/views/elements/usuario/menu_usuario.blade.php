
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="menuUsuario">
		<div class="list-group">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<a href="{{ route('usuario.index') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('usuario.index') ? 'active' : '' }}">
					<i class="fa fa-info-circle"></i> Meus Dados
				</a>
			</div>
			@if(Auth::user()->ativo)
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('produto_usuario.novo') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('produto_usuario.novo') ? 'active' : '' }}">
						<i class="fa fa-plus"></i> Adicionar Produto
					</a>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('minhas_trocas.index') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('minhas_trocas.index') ? 'active' : '' }}">
						<i class="fa fa-exchange"></i> Minhas trocas
					</a>
				</div>

				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('produto_usuario.index') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('produto_usuario.index') ? 'active' : '' }}">
						<i class="fa fa-th-large"></i> Produtos postados
					</a>
				</div>

				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('reportar_erro.index') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('reportar_erro.index') ? 'active' : '' }}">
						<i class="fa fa-bug"></i> Reportar erro
					</a>
				</div>
			@endif
			@if(Auth::user()->ativo)
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('cancelar.conta') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('cancelar.conta') ? 'active' : '' }}">
						<i class="fa fa-unlock-alt"></i> Cancelar conta
					</a>
				</div>
			@else
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<a href="{{ route('ativar.conta') }}" class="list-group-item {{ URL::to('/').'/'.Route::current()->uri() == route('cancelar.conta') ? 'active' : '' }}">
						<i class="fa fa-tag"></i> Reativar conta
					</a>
				</div>
			@endif
		</div>
	</div>
</div>