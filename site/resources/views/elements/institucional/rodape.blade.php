<div class="container-fluid">
    <div class="container">
        <div class="row">
            <ul class="text-center list-unstyled list-inline">
                <li role="presentation"><a class="page-scroll" href="{{ route('termo.index') }}">Termos</a></li>
                <li role="presentation"><a class="page-scroll" href="{{ route('guia.index') }}">Como funciona</a></li>
                <li role="presentation"><a class="page-scroll" href="{{ route('privacidade.index') }}">Privacidade</a></li>
                <li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}">Sobre</a></li>
                <li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#help">Ajuda</a></li>
                <li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#contato">Contato</a></li>
            </ul>
        </div>
        <div class="row">
            <p class="row-centered">2015© Troque Lá plataforma de trocas online. Todos os direitos reservados.</p>
        </div>
    </div>
</div>