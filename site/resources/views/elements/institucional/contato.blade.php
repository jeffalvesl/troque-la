<section id="contato">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="section-heading text-center">CONTATO</h2>
            <p class="text-center">Dúvidas, sugestões ou reclamações? Favor entrar em contato pelo formulário abaixo.</p>
            <hr>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="formContato form-horizontal" role="form" method="POST" action="{{ route('contato.post') }}">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <label class="control-label" for="nome">Nome:</label>
                        <input type="text" name="nome" class="form-control input-lg" id="name" placeholder="Nome">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <label class="control-label" for="email">Email:</label> 
                        <input type="text" name="email" class="form-control input-lg" id="email" placeholder="exemplo@troque.la">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <label class="control-label" for="mensagem">Mensagem:</label> 
                        <textarea class="form-control input-lg" name="mensagem" rows="5" co id="comment"></textarea>
                    </div>
                </div>
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3"> 
                    <div class="row-centered">
                        <button type="submit" class="btn btn-lg btn-success btEnviarMsg">Enviar Mensagem</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>