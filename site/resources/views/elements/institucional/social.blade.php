<div id="social" class="container text-center">
    <div class="call-to-action">
        <h2>SIGA-NOS NAS REDES SOCIAIS:</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="socialItens socialItensInicio">
                    @include('partials.social')
                </div>
            </div>
        </div>
    </div>
</div>