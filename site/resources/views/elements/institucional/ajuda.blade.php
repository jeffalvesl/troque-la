<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2 class="section-heading text-center">AJUDA</h2>
            <h3 class="text-center">Algum problema com o nosso serviço?</h3>
            <hr class="light">
            <p class="text-center">Verifique se sua pergunta já foi respondida. Caso o contrário, entre em contato conosco na página logo abaixo.</p>
            <hr class="light">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($ajuda as $pergunta)
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{ $pergunta->id_ajuda }}">
                  <h4 class="panel-title text-center">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $pergunta->id_ajuda }}" aria-expanded="false" aria-controls="collapse{{ $pergunta->id_ajuda }}">
                      {{ $pergunta->pergunta }}
                    </a>
                  </h4>
                </div>
                <div id="collapse{{ $pergunta->id_ajuda }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $pergunta->id_ajuda }}">
                  <div class="panel-body">
                    <?php echo $pergunta->resposta; ?>
                  </div>
                </div>
              </div>
              @endforeach
            </div>

            <hr class="light">
            <div class="row-centered">
                <a href="#contato" class="btn btn-primary btn-xl page-scroll">Contato</a>
            </div>
        </div>
    </div>
</div>  