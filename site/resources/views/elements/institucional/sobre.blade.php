<div class="header-content">
    <div class="header-content-inner text-justify">
        <h1 class="text-center">Sobre</h1>
        <hr>
        <p class="text-center">
        	<?php echo strip_tags($sobre->texto) ?>
        </p>
    </div>
    <hr>
    @if(count($ajuda) > 0)
    	<a href="#help" class="btn btn-info btn-xl page-scroll">Ir para ajuda</a>
    @endif
</div>