    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="{{ isset($description) ? $description : 'Troque lá. Site de trocas online. Troque seu celular, smartphone, televisor, troque seus games e consoles. Troque aquele produto que você não quer mais por um produto de seu interesse. Encontre pessoas para realizar sua troca.' }}">
    <meta name="robots" content="index, follow">

	<title>{{ isset($title) ? $title . ' | Troque lá ' : 'Troque lá | Troque seu produto' }}</title>

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/creative.css')}}" type="text/css">
    <link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/messages.css') }}" rel="stylesheet">

    @yield('style')

    <!-- Custom Fonts -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>-->
    <link href="{{ asset('assets/css/font-google.css')}}" rel="stylesheet">

    @include('elements.analyticstracking')