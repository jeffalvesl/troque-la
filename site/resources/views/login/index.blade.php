<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" href="">

		<title>Troque Lá | Entrar</title>

	    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
	    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css')}}" type="text/css">
	    <link rel="stylesheet" href="{{ asset('assets/css/creative.css')}}" type="text/css">
	    <link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="pageCadastro">

		<div class="container-fluid">
			<div class="row">
                <div class="col-md-12">
                	<h1 class="text-center">Entrar</h1>
                    <hr>
                    <div class="social-buttons">
                        <a href="{{ route('cadastro.social', 'facebook') }}" class="btn-lg btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                    </div>
                    <h3 class="row-centered">ou</h3>
                     <form class="form" role="form" method="POST" action="{{ route('logar.post') }}" id="login-nav">
                            <div class="form-group">
                                 <label class="sr-only" for="email">Email</label>
                                 <input type="email" class="form-control" id="email" name="email" placeholder="email@dominio.com">
                            </div>
                            <div class="form-group">
                                 <label class="sr-only" for="password">Senha</label>
                                 <input type="password" class="form-control" id="password" name="password" placeholder="Sua senha">
                                 <div class="help-block text-right"><a href="">Esqueceu a senha?</a></div>
                            </div>
                            <div class="form-group">
                                 <button type="submit" class="btn btn-success btn-block">Entrar</button>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="checkbox">
                                 <label>
                                 <input type="checkbox"> Mantenha-me logado
                                 </label>
                            </div>
                     </form>
                </div>
                <div class="bottom text-center">
                    <h4 class="font">
                        <a href="{{ route('cadastro.index') }}"><b>Cadastre-se</b></a>
                    </h4>
                </div>
         	</div>
		</div>

		<aside class="bg-dark">

	    </aside>

	    <section id="contact">
	        <div class="container">
	            <div class="row">
	                <p class="row-centered">2015© troque lá plataforma de trocas onlines. Todos os direitos reservados a J.F Company.</p>
	            </div>
	        </div>
	    </section>
	    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Plugin JavaScript -->


	</body>
