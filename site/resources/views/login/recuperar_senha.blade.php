<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" href="bootstrap/imgs/troquela.png">

		<title>Esqueci a Senha</title>

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!--<script src="bootstrap/js/ie-emulation-modes-warning.js"></script>-->
        

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="pageCadastro">

		<div class="container-fluid">
		
			<div class="row">
			    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<form role="form" class="formCadastrarSenha" action="{{ route('definir_senha.post') }}" method="POST">
						<h1 class="font row-centered">Nova senha</h1>
						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
			                        <input type="text" name="email" id="email" class="form-control input-lg" value="{{ $usuario->email }}" placeholder="Email" tabindex="1" readonly>
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<div class="label"></div>
			                        <input type="password" name="senha" id="senha" class="form-control input-lg" value="{{ old('senha') }}" placeholder="Nova Senha" tabindex="1">
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<div class="label"></div>
			                        <input type="password" name="confirmacao_senha" id="confirmacao_senha" class="form-control input-lg" value="{{ old('senha') }}" placeholder="Repetir Senha" tabindex="1">
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="hash" value="{{ $hash->hash }}">
						<hr class="colorgraph">
						<div class="row">
							<div class="col-xs-12 col-md-12"><input type="submit" value="Salvar Nova Senha" class="btn btn-success btn-block btn-lg" tabindex="7"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="rodape">
				<div class="menuRodape col-sm-12 col-xs-12 col-lg-12">
					<ul class="col-sm-12 col-xs-12 col-lg-12">
						<li role="presentation"><a class="page-scroll" href="{{ route('home.index') }}">Início</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('termo.index') }}">Termos</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('guia.index') }}">Como funciona</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('privacidade.index') }}">Privacidade</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}">Sobre</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#help">Ajuda</a></li>
						<li role="presentation"><a class="page-scroll" href="{{ route('sobre.index') }}#contato">Contato</a></li>
					</ul>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rodapeSocial">
	                <div class="row">
	                    <div class="socialItens socialItensInicio">
	                        <ul class="social-network social-circle">
	                            <li>
	                                <a href="#" class="icoFacebook" title="Facebook">
	                                    <i class="fa fa-facebook"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoTwitter" title="Twitter">
	                                    <i class="fa fa-twitter"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoGoogle" title="Google +">
	                                    <i class="fa fa-google-plus"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoInstagram" title="Intagram">
	                                    <i class="fa fa-instagram"></i>
	                                </a>
	                            </li>
	                        </ul>                
	                    </div>
	                </div>
	            </div>

				<div class="col-sm-12 col-xs-12 col-lg-12 menuRodape">
					<small>
						2015© Troque Lá plataforma de trocas onlines. Todos os direitos reservados.
					</small>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('assets/js/recuperar.js')}}"></script>

	</body>
</html>