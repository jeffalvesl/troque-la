@extends('layout.app_institucional')
    @section('conteudo')

        <header>
            <div class="header-content">
                <div class="header-content-inner">
                    <h1>Economize trocando</h1>
                    <hr>
                    <p>Troque Lá é a forma mais fácil de você trocar seu produto por outro de seu interesse. É grátis. É simples.</p>
                    <a href="{{ route('home.index') }}" class="btn btn-primary btn-xl page-scroll">Conferir o que está rolando</a>
                </div>
            </div>
        </header>

        <section class="bg-primary" id="about">
            @include('elements.sobre')
        </section>

        <aside class="bg-dark">
            @include('elements.institucional.social')
        </aside>
    
    @stop