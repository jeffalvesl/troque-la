<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" href="bootstrap/imgs/troquela.png">

		<title>{{ $titulo }} | Troque Lá</title>

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="page500">
	
		<div class="container-fluid mobile500 visible-xs visible-sm" id="conteudo500">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="err500">
					<h1>ERRO 500</h1>
					<h3>Nosso servidor teve um mau funcionameto!</h3>
				</div>
				<div class="explicativo500">
					<h3>Esperamos corrigir o problema mais rápido o possível.</h3>
					<button class="btn btn-default" type="button">Recarregar a página</button>
				</div>
			</div>
		</div>
		<div class="container-fluid desk500 hidden-xs hidden-sm" id="conteudo500pc">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="err500">
					<h1>ERRO 500</h1>
					<h3>Nosso servidor teve um mau funcionameto!</h3>
				</div>
				<div class="explicativo500">
					<h3>Esperamos corrigir o problema mais rápido o possível.</h3>
				</div>
			</div>
		</div>
		
		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>	
	</body>
</html>