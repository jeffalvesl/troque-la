<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>{{ $titulo }} | Troque Lá</title>

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="page404">

		<!--CONTENT-->
		<div class="container-fluid" id="conteudo404">
			<div class="col-lg-6 pull-right">
				<div class="houston">
					<h2>404.</h2>
					<h2>Houston,</h2>
					<h3>We Got a Problem!</h3>
				</div>
				<div class="explicativo">
					<a href="{{ URL::previous() }}" class="btErro">Pagina Anterior</a>
					<a href="{{ route('home.index') }}" class="btErro">Ir para Inicio</a>
				</div>
			</div>
		</div>
		
		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>	

	</body>
</html>