@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/fancybox/source/jquery.fancybox.css') }}" />
	@stop

		<div class="container-fluid">
			<div class="conteudoPadrao container" id="conteudo">

				<!-- TITULO DO PRODUTO -->
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1 class="font text-center">
								Propostas feitas em {{ $produto->produto }}
							</h1>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="trocas">
				    	@forelse($produto->propostas as $key => $proposta)
					    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<div class="row text-center">
					    			<h4 class="font">Produto oferecido: </h4>
					    			<a href="{{ route('produtos.detalhe', $proposta->produto->slug) }}">
					    				@if(isset($proposta->produtoProposta->produto->imagens[0]) && is_file($proposta->produtoProposta->produto->imagens[0]->miniatura))
					    					<img src="{{ asset($proposta->produtoProposta->produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $proposta->produtoProposta->produto->produto }}">
					    				@else
					    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $proposta->produtoProposta->produto->produto }}">
					    				@endif
					    			</a>
					    		</div>
					    		<a href="#">
			    					<h3 class="text-center">{{ $proposta->produtoProposta->produto->produto }}</h3>
			    				</a>
					    	</div>

			    			<a href="#">
			    				<h3 class="font">{{ $proposta->usuario->nome . ' ' . $proposta->usuario->sobrenome }}</h3>
			    			</a>
					    	<div class="troca col-xs-12 col-sm-12 col-md-8 col-lg-9">
								<div class="row">
					    			<a href="#">
					    				<p class="font">{{ $proposta->proposta }}</p>
					    			</a>
					    			<a target="_blank" href="{{ route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]) }}" class="btn btn-danger btViewProduto">Visualizar proposta <i class="fa fa-external-link-square"></i></a>
					    		</div>
					    	</div>
				    	@empty

					    @endforelse
			    	</div>
				</div>


				<!-- Banner FULL adsense -->
				<div class="row">
					<div class="container-fluid" id="buscador">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row row-centered">
								<img class="img-responsive" src="{{ asset('assets/imgs/adsense.png') }}" alt="placeholder+image">
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

		@section('script')
			<script src="{{ asset('assets/js/select2.min.js') }}"></script>
			<script src="{{ asset('assets/fancybox/source/jquery.fancybox.pack.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/produto/detalhe_produto.js')}}"></script>
		@stop
	@stop