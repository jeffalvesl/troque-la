@extends('layout.app')
@section('conteudo')
<!--END MENU-->

<div class="conteudoPadrao listagemProduto container-fluid" id="conteudo">

	<div class="row">
		<div class="page-header">
			<h1 class="font">{{ $subCategoria->sub_categoria }}</h1>

			{!! Breadcrumbs::render('subcategoria_produtos.get', $subCategoria) !!}
		</div>
	</div>

	<div class="row">
		<div class="trocas">
	    	@forelse($produtos as $key => $produto)
		    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="row text-center">
		    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
		    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
		    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
		    				@else
		    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
		    				@endif
		    			</a>
		    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
		    				<h3 class="text-center">{{ $produto->produto }}</h3>
		    			</a>

		    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
			    			<small class="font text-center">
			    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
			    			</small>
		    			</a>
		    		</div>
		    	</div>
	    	@empty
				<h4><i class="fa fa-frown-o"></i> Infelizmente não achamos produtos para {{ $subCategoria->sub_categoria }}.
					@if(!Auth::user())
						Seja o primeiro a cadastrar um produto aqui.
						<a href="{{ route('login.login') }}">Faça login e cadastre seu produto</a>
					@else
						<a href="{{ route('produto_usuario.novo') }}">Seja o primeiro a cadastrar um produto aqui.</a>
					@endif
				</h4>
		    @endforelse
    	</div>
	</div>
</div>

@stop