@extends('layout.app')
	@section('conteudo')

	@section('style')
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/fancybox/source/jquery.fancybox.css') }}" />
	@stop

		<div class="container-fluid">
			<div class="conteudoPadrao container" id="conteudo">
				{!! Breadcrumbs::render('produtos.detalhe', $produto) !!}

				<!-- TITULO DO PRODUTO -->
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h1 class="font text-center">
								{{ $produto->produto }}
							</h1>
						</div>
					</div>
				</div>

				<!---  Informações do produto  -->
				<div class="container-fluid">
					<div id="produtoeinfo">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="imagemdoproduto">
								<h4 class="text-center"><small><i class="fa fa-search-plus"></i> Clique na imagem para ampliar</small></h4>
								<div class="imagensProduto text-center row">
									@foreach($produto->imagens as $imagem)
										<div class="imagem col-xs-12 col-sm-6 col-md-3 col-lg-3">
											<a href="{{ asset($imagem->imagem) }}" class="fancybox" rel="gallery1" title="{{ $produto->produto }}">
												@if(is_file($imagem->miniatura))
													<img class="img-rounded" src="{{ asset($imagem->miniatura) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@else
													<img class="img-rounded" src="{{ asset($imagem->imagem) }}" alt="{{ $produto->produto }}" title="{{ $produto->produto }}">
												@endif
											</a>
										</div>
									@endforeach
								</div>
								@include('elements.social')
							</div>
						</div>

						<div class="descricaoProduto row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row">
									<p class="font">{{ $produto->descricao_produto }}</p>
								</div>
							</div>
						</div>

						<div class="row">
							<!-- Produtos de interesse -->
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<div class="text-center aceitetroca">
									<div class="product-price">
										<h3 class="text-center font"><i class="fa fa-exchange"></i> Aceita trocar em:</h3>
									</div>

									<div class="produtosDesejados">
									    <p>
										    @foreach($produto->interesses as $interesse)
					                        	<i class="fa fa-tags wow bounceIn"></i><big><a href="{{ route('home.index') }}/buscar?termo={{ $interesse->descricao_produto }}">{{ $interesse->descricao_produto }}</a></big>
					                        @endforeach
				                    	</p>
				                    </div>
				                </div>
							</div>

							<!-- Informações adicionais do produto -->
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
								<div class="text-center row">
				                    <div class="infoAdicionais">
				                    	<div class="product-price">
											<h3 class="font"><i class="fa fa-info-circle"></i> Informações adicionais:</h3>
										</div>

					                    	<strong>Garantia(tempo restante):</strong>

					                    	<p>
					                    		{{ $produto->tempo_garantia ? $produto->tempo_garantia : 'Não tem garantia' }}
					                    	</p>

					                    	<strong>Garantia extendida(tempo restante):</strong>
					                    	<p>
					                    		{{ $produto->garantia_extendida_tempo ? $produto->garantia_extendida_tempo : 'Não tem garantia extendida' }}
					                    	</p>
				                    </div>
								</div>
							</div>

							@if(Auth::user())
								<!-- Informações do usuário -->
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="text-center row">
										<div class="product-price">
											<h3 class="font"><i class="fa fa-user"></i> {{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome }}</h3>
										</div>
										<div class="col-xs-12 col-sm-12">
							              <ul class="list-group">
							              	@if(!Auth::user())
												<li class="list-group-item text-center"><div class="btn-group cart"><a href="{{ route('login.login') }}" class="btn btn-success">Faça login para ver os dados de contato</a></div></li>
							              	@else
								                <li class="list-group-item text-center"><i class="fa fa-location-arrow"></i> {{ $produto->usuario->bairro }}, {{ $produto->usuario->cidade->cidade }} ({{ $produto->usuario->cidade->estado->estado }})</li>
								                @if($produto->usuario->telefone)
								                	<li class="list-group-item text-center"><i class="fa fa-phone"></i> {{ $produto->usuario->telefone }} </li>
								                @endif

								                @if($produto->usuario->celular)
								                	<li class="list-group-item text-center"><i class="fa fa-mobile"></i> {{ $produto->usuario->celular }} </li>
								                @endif
							                @endif
							              </ul>
							            </div>
									</div>
								</div>
							@else
								<!-- Adsense -->
								<!--<div class="row">
									<div class="container-fluid" id="buscador">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="row row-centered">
												<img src="{{ asset('assets/imgs/adsense3.png') }}" alt="placeholder+image">
											</div>
										</div>
									</div>
								</div>-->

								<!-- Login de Usuário -->
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="text-center row">
										<div class="container-fluid" id="buscador">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="row row-centered">
													<li class="text-center">
														<div class="btn-group cart"><a href="{{ route('login.login') }}" class="btn btn-success">Faça login para ver os dados de contato</a></div>
													</li>
							            		</div>
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>

						<!-- Pessoas interessadas -->
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row">
									<div class="form-group">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											@if(count($pessoasInteressadas) <= 0 && Auth::user() && Auth::user()->id_usuario != $produto->id_usuario)
												<h2 class="font text-center">
													<i class="fa fa-user"></i>
													Seja o primeiro a fazer uma proposta por este produto.

													@if(!Auth::user())
														<a href="{{ route('login.login') }}">Faça login para enviar uma proposta</a>
														ou
														<a href="{{ route('login.login') }}">Cadastre-se</a>
													@endif
												</h2>
											@else
												<h2 class="font text-center">
													@if(count($pessoasInteressadas) > 0)
														<i class="fa fa-users"></i>
														{{ count($pessoasInteressadas) == 1 ? count($pessoasInteressadas) . ' pessoa interessada' : count($pessoasInteressadas) . ' pessoas interessadas'  }} neste produto.
													@endif

													@if(!Auth::user())
														<a href="{{ route('login.login') }}">Faça login e envie uma proposta</a>
														ou
														<a href="{{ route('cadastro.index') }}">Cadastre-se</a>
													@endif
												</h2>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Banner FULL adsense -->
				<!--<div class="row">
					<div class="container-fluid" id="buscador">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row row-centered">
								<img class="img-responsive" src="{{ asset('assets/imgs/adsense.png') }}" alt="placeholder+image">
							</div>
						</div>
					</div>
				</div>-->

				<!-- Formulário de enviar proposta E produtos relacionados -->
				<div class="row">
					@if(Auth::user())

						@if(!in_array(Auth::user()->id_usuario, $pessoasInteressadas) && Auth::user()->id_usuario != $produto->id_usuario)
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="row">
									<div class="form-group">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<h2 class="font text-center">
												<i class="fa fa-envelope-o"></i>
												Envie um proposta
											</h2>
										</div>
									</div>
								</div>
								<div class="page-header"><h3 class="text-center"></div>
								@include('elements.produto.enviar_proposta')
							</div>
						@else
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="text-center row">
									<div class="product-price">
										@if(Auth::user()->id_usuario != $produto->id_usuario)
											<h2 class="font"><i class="fa fa-user"></i> Você já fez uma proposta. Confira os interessados</h2>
										@else
											<h2 class="font"><i class="fa fa-users"></i> Pessoas interessadas em seu produto</h2>
										@endif
									</div>
									<div class="col-xs-12 col-sm-12">
										<ul class="list-group">
											@forelse($produto->propostas as $proposta)
												<li class="list-group-item text-center">
													<i class="fa fa-user"></i> 
													<a target="_blank" href="{{ route('mensagens_produto.index', [$produto->slug, $proposta->id_proposta_produto]) }}">
														{{ $proposta->usuario->nome . ' ' .$proposta->usuario->sobrenome}}
														(<small>Visualizar proposta <i class="fa fa-external-link"></i></small>)
													</a> 
												</li>
											@empty
												@if(Auth::user()->id_usuario == $produto->id_usuario)
													<span>Até agora ninguém fez proposta.</span>
												@endif
											@endforelse
										</ul>
						            </div>
								</div>
							</div>
						@endif
					@else
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h2 class="font text-center">
											<i class="fa fa-newspaper-o"></i>
											Novidades
										</h2>
									</div>
								</div>
							</div>
							<div class="page-header"><h3 class="text-center"></div>
							<div class="trocas">
					    		@forelse($novidades as $key => $produto)
							    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="row text-center">
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				@if(isset($produto->imagens[0]))
							    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="130" class="img-rounded" alt="{{ $produto->produto }}">
							    				@else
							    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
							    				@endif
							    			</a>
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				<h3 class="text-center">{{ $produto->produto }}</h3>
							    			</a>

							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
								    			<small class="font text-center">
								    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
								    			</small>
							    			</a>
							    		</div>
							    	</div>
					    		@empty
									<!--<div class="row">
										<div class="container-fluid" id="buscador">
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
												<div class="row row-centered">
													<img src="{{ asset('assets/imgs/adsense3.png') }}" alt="placeholder+image">
												</div>
											</div>
										</div>
									</div>-->
						    	@endforelse
			    			</div>
						</div>
					</div>
						<!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<!-- Adsense -->
							<!--<div class="row">
								<div class="container-fluid" id="buscador">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="row row-centered">
											<img src="{{ asset('assets/imgs/adsense3.png') }}" alt="placeholder+image">
										</div>
									</div>
								</div>
							</div>
						</div>-->
					@endif

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h2 class="font text-center">
											<i class="fa fa-retweet"></i>
											Produtos Relacionados
										</h2>
									</div>
								</div>
							</div>
							<div class="page-header"><h3 class="text-center"></div>
							<div class="trocas">
					    		@forelse($relacionados as $key => $produto)
							    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="row text-center">
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				@if(isset($produto->imagens[0]))
							    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="130" class="img-rounded" alt="{{ $produto->produto }}">
							    				@else
							    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
							    				@endif
							    			</a>
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				<h3 class="text-center">{{ $produto->produto }}</h3>
							    			</a>

							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
								    			<small class="font text-center">
								    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
								    			</small>
							    			</a>
							    		</div>
							    	</div>
					    		@empty
									<!--<div class="row">
										<div class="container-fluid" id="buscador">
											<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
												<div class="row row-centered">
													<img src="{{ asset('assets/imgs/adsense3.png') }}" alt="placeholder+image">
												</div>
											</div>
										</div>
									</div>-->
						    	@endforelse
			    			</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		@section('script')
			<script src="{{ asset('assets/js/select2.min.js') }}"></script>
			<script src="{{ asset('assets/fancybox/source/jquery.fancybox.pack.js') }}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
			<script type="text/javascript" src="{{ asset('assets/js/produto/detalhe_produto.js')}}"></script>
		@stop
	@stop