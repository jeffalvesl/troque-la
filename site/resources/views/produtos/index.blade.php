@extends('layout.app')
	@section('conteudo')
		<!--END MENU-->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="conteudoPadrao listagemProduto container-fluid" id="conteudo">
				@if(count($novidades) > 0)

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								@if(!Auth::user())
									<h1 class="font text-center">Seja Bem vindo(a) ao Troque lá.</h1>
									<h3 class="font text-center"><a href="{{ route('cadastro.index') }}">Quero me cadastrar</a></h3>
								@else
									<h1 class="font text-center">
										<i class="fa fa-home"></i>
										Olá,
										{{ Auth::user()->nome . ' ' . Auth::user()->sobrenome }}.
									</h1>
									<h3 class="font text-center">
										Seja bem vindo(a) novamente e 

										@if(!Auth::user()->produtoInteresse)
											Confira as novidades e comece a trocar suas inutilidades.
										@else
											<a href="{{ route('produtos.interesse') }}">
												confira o que preparamos pra você!
											</a>
										@endif
									</h3>

								@endif
							</div>
						</div>
					</div>

					<div class="row">
						<div class="trocas">
					    	@forelse($novidades as $key => $produto)
						    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="row text-center">
						    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
						    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
						    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
						    				@else
						    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
						    				@endif
						    			</a>
						    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
						    				<h3 class="text-center">{{ $produto->produto }}</h3>
						    			</a>

						    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    			<small class="font text-center">
							    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
							    			</small>
						    			</a>
						    		</div>
						    	</div>
					    	@empty

						    @endforelse
				    	</div>
					</div>


					<!-- 	<div class="bannersQuadrado row">
							<div class="banner col-md-4">
								<div class="container-fluid">
									<div class="row row-centered">
										<img src="{{ asset('assets/imgs/adsense3.png') }}">
									</div>
								</div>
							</div>

							<div class="banner col-md-4">
								<div class="container-fluid">
									<div class="row row-centered">
										<img src="{{ asset('assets/imgs/adsense3.png') }}">
									</div>
								</div>
							</div>
						</div> -->
				@endif

				@if(count($recomendados) > 0)
					<div class="row page-header">
						@if(Auth::user())
				  			<h2>Recomendado com base em seus produtos e interesses</h2>
				  		@else
				  			<h2>Recomendamos</h2>
				  		@endif
					</div>

					<div id="recomendados" class="row text-center">
			            <div class="row">
							<div class="trocas">
						    	@forelse($recomendados as $key => $produto)
							    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="row text-center">
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
							    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
							    				@else
							    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
							    				@endif
							    			</a>
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				<h3 class="text-center">{{ $produto->produto }}</h3>
							    			</a>

							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
								    			<small class="font text-center">
								    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
								    			</small>
							    			</a>
							    		</div>
							    	</div>
						    	@empty

							    @endforelse
					    	</div>
						</div>
		            </div>
		    	@endif
			</div>
		</div>


		<!-- GOOGLE ADSENSE -->
		<!-- <div class="container-fluid" id="buscador">
			<div class="row row-centered">
				<img src="{{ asset('assets/imgs/adsense.png') }}">
			</div>
		</div> -->


		<!-- TODOS OS PRODUTOS -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="conteudoPadrao listagemProduto container-fluid" id="conteudo">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if(count($maisProdutos) > 0)

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					  				<h2 class="text-center"><i class="fa fa-plus"></i> Veja mais...</h2>
				  				<h5 class="text-center"><i class="fa fa-plus"></i> Confira produtos que podem interessar.</h5>
								</div>
							</div>
						</div>

						<!-- CONTEUDO EM VEJA MAIS -->

				    	<div class="row">
							<div class="trocas">
						    	@forelse($maisProdutos as $key => $produto)
							    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="row text-center">
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
							    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
							    				@else
							    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
							    				@endif
							    			</a>
							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
							    				<h3 class="text-center">{{ $produto->produto }}</h3>
							    			</a>

							    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
								    			<small class="font text-center">
								    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
								    			</small>
							    			</a>
							    		</div>
							    	</div>
						    	@empty

							    @endforelse
					    	</div>
			        	</div>
				    @endif

				   	<nav>
					   	<div class="row-centered">
							<ul class="pagination">
							    {!! paginar($maisProdutos) !!}
						</div>
					</nav>
				</div>
			</div>
		</div>
	@stop