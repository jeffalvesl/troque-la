@extends('layout.app')
@section('conteudo')
<!--END MENU-->

<div class="conteudoPadrao listagemProduto container-fluid" id="conteudo">

	<div class="row">
		<div class="page-header">
			@if(!isset($maisProdutos))
				<h1 class="font text-center"><a href="javascript:void(0)"><i class="fa fa-heart denger"></i></a> {{ $title }}</h1>
			@else
				<h1 class="font text-center">{{ $title }}</h1>
			@endif
		</div>

		@if(isset($maisProdutos))
			<div class="row text-center col-xs-12 col-sm-12 col-md-8 col-lg-8">
				@foreach($maisProdutos as $produto)
					@if(isset($produto->imagens[0]))
						<div class="produto col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="thumbnail">
								<a href="{{ route('produtos.detalhe', $produto->slug) }}">
									<img src="{{ asset($produto->imagens[0]->imagem) }}" title="{{ $produto->produto }}" alt="{{ $produto->produto }}" height="160">
								</a>
								<div class="caption">
									<h3><a href="{{ route('produtos.detalhe', $produto->slug) }}"><?php echo $produto->produto; ?></a></h3>
									@if(isset(Auth::user()->id_usuario) && Auth::user()->id_usuario == $produto->id_usuario)
										<p>Este é seu produto</p>
									@else
										<p>{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome  }}</p>
									@endif	
								</div>
							</div>
						</div>
					@endif
				@endforeach
			</div>
		@else
			<div class="row">
				<div class="trocas">
			    	@forelse($produtos as $key => $produto)
				    	<div class="troca col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="row text-center">
				    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
				    				@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
				    					<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
				    				@else
				    					<img src="{{ asset('assets/imgs/image_facebook.png') }}" width="120" class="img-rounded" alt="{{ $produto->produto }}">
				    				@endif
				    			</a>
				    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
				    				<h3 class="text-center">{{ $produto->produto }}</h3>
				    			</a>

				    			<a href="{{ route('produtos.detalhe', $produto->slug) }}">
					    			<small class="font text-center">
					    				{{ $produto->usuario->nome . ' ' . $produto->usuario->sobrenome}}
					    			</small>
				    			</a>
				    		</div>
				    	</div>
			    	@empty

				    @endforelse
		    	</div>
			</div>
		@endif
	</div>

	@if(isset($maisProdutos))
		<nav>
			<div class="row-centered">
				<ul class="pagination">
				{!! paginar($maisProdutos, Input::get('termo')) !!}

			</div>
		</nav>
	@endif
</div>

@stop