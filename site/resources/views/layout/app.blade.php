<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<title>{{ isset($title) ? $title : 'Troque Lá | Troque e Economize' }}</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="{{ isset($description) ? substr($description, 0, 159) : 'Seu site de trocas online. Troque seus produtos que já não lhe interessam mais por algo que seja de sua necessidade. Busque pessoas que possuem aquilo que é seu interesse.' }}">
		<meta name="robots" content="index, follow">
		<meta name="url" content="{{ URL::current() }}" />

		<meta property="og:url"           content="{{ URL::current() }}" />
	    <meta property="og:type"          content="website" />
	    <meta property="og:title"         content="{{ isset($title) ? $title : 'Troque Lá' }}" />
	    <meta property="og:description"   content="{{ isset($description) ? substr($description, 0, 199) : 'Seu site de trocas online. Troque seus produtos que já não lhe interessam mais por algo que seja de sua necessidade. Busque pessoas que possuem aquilo que é seu interesse.' }}" />
	    <meta property="og:image"         content="{{ isset($imagemProduto) ? asset($imagemProduto) : asset('assets/imgs/image_facebook.png') }}" />

	    <meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}" />
		@yield('style')
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-google.css')}}" rel="stylesheet">



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="pageInicio">
	@include('elements.analyticstracking')

	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=1607268119546921";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

		@include('elements.menu_interno')

		@if(Auth::user())
			@include('elements.modal_mensagens')
		@endif

		<!--END MENU-->

		<!--BUSCADOR
		<div class="container-fluid" id="buscador">
			<div class="row row-centered">

			</div>
		</div>
		-->
		<div class="container-fluid" id="buscador">
			<div class="row row-centered">
				<form action="{{ route('buscar.produto') }}" method="GET">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-centered">
						<div class="input-group input-group-lg">
								<input type="text" class="form-control" name="termo" placeholder="Buscar por...">
								<span class="input-group-btn">
									<button class="btn btn-danger btn-default" type="submit"><i class="fa fa-search"></i></button>
								</span>
						</div>
					</div>
				</form>
			</div>
		</div>

		@yield('conteudo')

        <section id="contact">
            @include('elements.social_interno')
        </section>


		<section id="contact">
            @include('elements.institucional.rodape')
        </section>

		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/menu.js')}}"></script>
		<script src="{{ asset('assets/js/select2.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/trocar_cidade.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/bootstrap-notify.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/app.js')}}"></script>
		@if(!Auth::user())
		    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
		    <script type="text/javascript" src="{{ asset('assets/js/login.js') }}"></script>
		@endif

    	@yield('script')

	</body>
</html>