<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ isset($description) ? $description : 'Seu site de trocas online. Troque seus produtos que já não lhe interessam mais por algo que seja de sua necessidade. Busque pessoas que possuem aquilo que é seu interesse.' }}">
    <meta property="og:image" content="{{ asset('assets/imgs/image_facebook.png') }}">
    <meta name="robots" content="index, follow">

    <title>{{ isset($title) ? $title . ' | Troque lá' : 'Troque Lá | Economize trocando' }}</title>

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/creative.css')}}" type="text/css">
    <link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/messages.css') }}" rel="stylesheet">

    @yield('style')

    <!-- Custom Fonts -->
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>-->
    <link href="{{ asset('assets/css/font-google.css')}}" rel="stylesheet">

    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=1607268119546921";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

    <body id="{{ isset($idPagina) ? $idPagina : '' }}">
        @include('elements.analyticstracking')

        @include('elements.menu')
        @yield('conteudo')

        <section id="contact">
            @include('elements.institucional.rodape')
        </section>

    </body>

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{ asset('assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.fittext.js')}}"></script>
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/js/creative.js') }}"></script>
    @yield('script')

</html>
