<!DOCTYPE html>
<html lang="pt_BR">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" href="bootstrap/imgs/troquela.png">

		<title>Troque Lá</title>

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		@yield("style")
		<link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		
        


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="pageInicio">
		<!-- TOPO DA PÁGINA-->

    		<!--MENU COM CARROSSEL/CATEGORIAS-->

		<nav class="navbar navbar-default navbar-fixed-top">
		    <div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>	
				<big class="navbar-brand" href="#">
					<big class="logo"><a class="navbar-brand font" href="#">Troque Lá</a></big>
					<!--<img src="bootstrap/imgs/logo1.png" alt="">-->
		        </big>
			</div>
				
				
			<div class="collapse navbar-collapse js-navbar-collapse text-center">
				<ul class="nav navbar-nav">
					<li class="dropdown mega-dropdown">
						<a href="#" class="dropdown-toggle font" data-toggle="dropdown">
							Categorias <span class="glyphicon glyphicon-chevron-down pull-right"></span>
						</a>
							
						<ul class="dropdown-menu mega-dropdown-menu row">
							<li class="col-sm-3">
								<ul>
									<li class="dropdown-header">Em destaque</li>                            
		                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
	                              		<div class="carousel-inner">
		                                	<div class="item active">
			                                    <a href="#">
			                                    	<img src="http://placehold.it/254x150/3498db/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 1">
			                                    </a>

			                                    <h4><small>Produto 1</small></h4>                                        
			                                    <button class="btn btn-default" type="button">Ver produto</button>      
			                                </div>

			                                <div class="item">
			                                    <a href="#">
			                                    	<img src="http://placehold.it/254x150/ef5e55/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 2">
			                                    </a>

			                                    <h4><small>Gold sandals with shiny touch</small></h4>                                        
			                                    <button class="btn btn-default" type="button">Ver produto</button>       
		                                	</div>

			                                <div class="item">
			                                    <a href="#">
			                                    	<img src="http://placehold.it/254x150/2ecc71/f5f5f5/&text=New+Collection" class="img-responsive" alt="product 3">
			                                    </a>
			                                    
			                                    <h4><small>Denin jacket stamped</small></h4>                                        
			                                    <button class="btn btn-default" type="button">Ver produto</button>      
		                                	</div>                                
		                              	</div>

		                            </div><!-- /.carousel -->
		                        
		                            <li class="divider"></li>
		                            <li>
		                            	<a href="#">
		                            		Todas as novidades
		                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
		                            	</a>
		                            </li>
		                            <li>
		                            	<a href="#">
		                            		Como funciona
		                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
		                            	</a>
		                            </li>
		                            <li>
		                            	<a href="#">
		                            		Regras de uso
		                            		<span class="glyphicon glyphicon-chevron-right pull-right"></span>
		                            	</a>
		                            </li>
								</ul>
							</li>
							
							<li class="col-sm-2">
								<ul>
									<li class="dropdown-header">Eletrônicos</li>
									<li><a href="#">Desktop</a></li>
									<li><a href="#">Notebook e Ultrabook</a></li>
									<li><a href="#">Acessórios</a></li>
									<li class="divider"></li>
									<li><a href="#">Tablet</a></li>
									<li><a href="#">eBook</a></li>
									<li class="divider"></li>
									<li><a href="#">Câmera e Filmadora</a></li>
									<li><a href="#">Videogames</a></li>
									<li><a href="#">Som e Imagem</a></li>
								</ul>
							</li>

							<li class="col-sm-2">
								<ul>
								    <li class="dropdown-header">Telefonia</li>
								    <li><a href="#">Android</a></li>
								    <li><a href="#">iPhone</a></li>
								    <li><a href="#">Windows Phone</a></li>
								    <li><a href="#">Outros</a></li>
								    <li><a href="#">Telefone Fixo</a></li>
								    <li class="divider"></li>
								    <li><a href="#">Acessórios</a></li>
								    <li class="divider"></li>
								    <li class="dropdown-header">Criança</li>
								    <li><a href="#">Brinquedos</a></li>
						    		<li><a href="#">Carrinhos e Cadeirinhas</a></li>	
								</ul>
							</li>
							<li class="col-sm-2">
								<ul>
									<li class="dropdown-header">Filmes e Músicas</li>
									<li><a href="#">Filmes em DVD</a></li>
								    <li><a href="#">Filmes em Blu-Ray</a></li>
								    <li><a href="#">Séries em DVD</a></li>
								    <li><a href="#">Séries em Blu-Ray</a></li>
								    <li class="divider"></li>
								    <li><a href="#">CD</a></li>
								    <li><a href="#">LP/Vinil</a></li>
								    <li><a href="#">Shows em DVD</a></li>
								    <li><a href="#">Shows em Blu-Ray</a></li>					
		                        </ul>
		                    </li>
		                    
		                    <li class="col-sm-3">
	                    		<ul>
		                    		<li class="dropdown-header">Roupas e Acessórios</li>
		                    		<li><a href="#">Masculino</a></li>
								    <li><a href="#">Feminino</a></li>
								    <li><a href="#">Infantil</a></li>
								    <li><a href="#">Acessórios</a></li>
								    <li><a href="#">Bolsas e Mochilas</a></li>
								    <li><a href="#">Malas</a></li>
								    <li class="divider"></li>
								    <li class="dropdown-header">Games</li>
								    <li><a href="#">Jogos para PC</a></li>
						    		<li><a href="#">Jogos para PlayStation</a></li>	
						    		<li><a href="#">Jogos para XBox</a></li>
						    		<li><a href="#">Outros</a></li>		
		                        </ul>                                                      
							</li>
						</ul>
							
					</li>
				</ul>
				

				<ul class="nav navbar-nav">
					<li><a href="minhas_trocas.html">Minha Área</a></li>
					<li><a href="novo_produto.html">Anunciar</a></li>
					<li><a href="sobre.html">Contato</a></li>
					<li><a href="sobre.html">Ajuda</a></li>
				</ul>
			</div><!-- /.nav-collapse -->
		</nav>

			@yield('conteudo')

		<div class="container">
			<div class="rodape">
				<div class="menuRodape col-sm-12 col-xs-12 col-lg-12">
					<ul class="col-sm-12 col-xs-12 col-lg-12">
						<li>
							<a href="sugestoes.html">Sugestões </a>
						</li>
						<li>
							<a href="depoimentos.html"> Depoimentos</a>
						</li>
						<li>
							<a href="regras.html"> Regras</a>
						</li>
						<li>
							<a href="sobre.html"> Sobre </a>
						</li>
						<li>
							<a href="termos"> Termos </a>
						</li>
						<li>
							<a href="privacidade.html"> Privacidade </a>
						</li>
						<li>
							<a href="reportar_erro.html"> Reportar Erro </a>
						</li>
					</ul>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 rodapeSocial">
	                <div class="row">
	                    <div class="socialItens socialItensInicio">
	                        <ul class="social-network social-circle">
	                            <li>
	                                <a href="#" class="icoFacebook" title="Facebook">
	                                    <i class="fa fa-facebook"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoTwitter" title="Twitter">
	                                    <i class="fa fa-twitter"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoGoogle" title="Google +">
	                                    <i class="fa fa-google-plus"></i>
	                                </a>
	                            </li>
	                            <li>
	                                <a href="#" class="icoInstagram" title="Intagram">
	                                    <i class="fa fa-instagram"></i>
	                                </a>
	                            </li>
	                        </ul>                
	                    </div>
	                </div>
	            </div>

				<div class="col-sm-12 col-xs-12 col-lg-12 menuRodape">
					<small>
							2015© Troque Lá plataforma de trocas onlines. Todos os direitos reservados a J.F Company.
					</small>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/fb.js')}}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/cep.js')}}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/efeitos.js')}}"></script>
		
		@yield('script')
		
	</body>
</html>
