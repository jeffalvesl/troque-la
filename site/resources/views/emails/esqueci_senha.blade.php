<!DOCTYPE html>
<html>
<head>
	<title>Troque lá</title>
</head>
<body>
	<div class="">
		<h1>Olá, Usuário.</h1>
	
		<p>
			<strong><a href="{{ $dados['url'] }}">Clique aqui para recuperar sua senha.</a></strong>
		</p>

		<p>
			Agradecemos a sua preferência em usar nossa plataforma para trocar seus produtos.
			Você pode a qualquer momento entrar em contato com nossa equipe através do email troque.la@gmail.com.
		</p>

		<p>
			Atenciosamente, Equipe Troque lá.
		</p>
	</div>

</body>
</html>