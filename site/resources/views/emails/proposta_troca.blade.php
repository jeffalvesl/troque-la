<!DOCTYPE html>
<html>
<head>
	<title>Troque lá</title>
</head>
<body>
	<div class="">
		<h1>Olá, {{ $dados['dono_produto'] }}.</h1>
	
		
		<p>
			O Usuario {{ $dados['usuario_remetente'] }} fez uma proposta de troca para o produto - {{ $dados['produto'] }}.
		</p>
	
		<p>
			<strong><a href="{{ $dados['url_mensagem'] }}">Clique aqui para vizualizar a proposta.</a></strong>
		</p>

		<p>
			Agradecemos a sua preferência em usar nossa plataforma para trocar seus produtos.
			Você pode a qualquer momento entrar em contato com nossa equipe através do email troque.la@gmail.com.
		</p>

		<p>
			Atenciosamente, Equipe Troque lá.
		</p>
	</div>

</body>
</html>