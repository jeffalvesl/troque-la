@if(Auth::user()->ativo)
	<li><a href="{{ route('usuario.index') }}"><i class="fa fa-info-circle"></i> Meus dados</a></li>
	<li><a href="{{ route('produto_usuario.novo') }}"><i class="fa fa-plus"></i> Adicionar Produto</a></li>
	<li>
		<a href="#" class="mensagens" data-url="{{ route('verifica.notificacao') }}" data-toggle="modal" data-target="#mensagens">
			<i class="fa fa-inbox"></i> Mensagens  <span class="badge total">{{ count($notificacoes) }}</span>
		</a>
	</li>
@endif