<div class="form-group">
    <label class="control-label col-sm-3" for="cidade" class="cidade"></label>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <select name="cidade" class="cidades form-control" id="cidade" data-action="{{ route('trocar.cidade') }}">
        	<option value="">Cidade</option>
        	@foreach($cidades as $cidade)
            	<option value="{{ $cidade->id_cidade }}" {{ $cidade->id_cidade == Cookie::get('cidade')['id_cidade'] ? 'selected' : '' }} >{{ $cidade->cidade  . ' - ' . $cidade->estado->sigla }}</option>
            @endforeach
        </select>
    </div>
</div>