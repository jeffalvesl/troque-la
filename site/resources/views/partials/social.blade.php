@if(isset($rede['facebook']))
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="socialItens socialItensInicio">
                <ul class="social-network social-circle">
                    <li>
                        <a target="_blank" href="{{ $rede['facebook'] }}" class="icoFacebook" title="Facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $rede['twitter'] }}" class="icoTwitter" title="Twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $rede['google'] }}" class="icoGoogle" title="Google +">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $rede['instagram'] }}" class="icoInstagram" title="Intagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endif