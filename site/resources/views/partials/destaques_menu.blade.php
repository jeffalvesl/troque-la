@if(count($destaques) > 0)
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
			
			@foreach($destaques as $key => $destaque)
				@if(isset($destaque->imagens[0]))
			    	<div class="item {{ $key == 0 ? 'active' : '' }}">
			            <a href="{{ route('produtos.detalhe', $destaque->slug) }}">
			            <?php
							$imagem = !is_null($destaque->imagens[0]->miniatura) ? $destaque->imagens[0]->miniatura : $destaque->imagens[0]->imagem;
						?>
			            	<img src="{{ asset($imagem) }}" class="img-responsive" alt="{{ $destaque->produto }}">
			            </a>

			            <h4><small>{{ $destaque->produto }}</small></h4>                                        
			            <a href="{{ route('produtos.detalhe', $destaque->slug) }}" class="btn btn-default">Ver produto</a>      
			        </div>
			    @endif
	       	@endforeach
	  	</div>

	</div>
@endif