@foreach($categorias as $categoria)
	@if(count($categoria->subCategorias) > 0)
		<li class="col-sm-2">
			<ul>
				<li class="dropdown-header">
					<h4>
						<a href="{{ route('produto_categoria.get', $categoria->slug) }}">{{ $categoria->categoria }}</a>
					</h4>
				</li>
				@foreach($categoria->subCategorias as $subCategoria)
					<li><a href="{{ route('subcategoria_produtos.get', [$categoria->slug, $subCategoria->slug]) }}">{{ $subCategoria->sub_categoria }}</a></li>
				@endforeach
			</ul>
		</li>
	@endif
@endforeach