<div class="col-xs-12 col-sm-12 col-md-4 col-lg-12">
	<a href="{{ route('home.index') }}"><h3 class="text-center">{{ $titulo }}</h3></a>
</div>

@foreach($novidades as $produto)
	@if(isset($produto->imagens[0]) && is_file($produto->imagens[0]->miniatura))
		<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
			<div class="row text-center">
    			<a href="{{ route('editar.produto', $produto->slug) }}">
    				<img src="{{ asset($produto->imagens[0]->miniatura) }}" width="200" class="img-rounded" alt="{{ $produto->produto }}">
    			</a>
    			<a href="#">
    				<h5 class="text-center font">{{ $produto->produto }}</h5>
    			</a>

    		</div>
    	</div>
    @endif
@endforeach

