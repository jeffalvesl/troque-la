<!DOCTYPE html>
<html lang="en">

<head>

    @include('elements.institucional.head')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="sobrePage">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="{{ route('home.index') }}">Troque Lá</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Sobre</a>
                    </li>
                    @if(count($ajuda) > 0)
                        <li>
                            <a class="page-scroll" href="#help">Ajuda</a>
                        </li>
                    @endif
                    <li>
                        <a class="page-scroll" href="#contato">Contato</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#social">Social</a>
                    </li>

                    @if(Auth::user())
                        @include('elements.box_menu_logado')
                    @else
                        <li>
                            <a class="visible-xs visible-ms" href="{{ route('login.index') }}">Login</a>
                        </li>
                        <li class="dropdown hidden-xs hidden-md">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                            @include('elements.login')
                        </li>
                    @endif
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header class="sobrenos" id="about">
        @include('elements.institucional.sobre')
    </header>
    
    @if(count($ajuda) > 0)
        <section class="bg-primary aboutAjuda" id="help">
            @include('elements.institucional.ajuda')
        </section>
    @endif
    
    <div class="container-fluid">
        @include('elements.institucional.contato')
    </div>

    <aside class="bg-dark" id="social">
        @include('elements.institucional.social')
    </aside>

    <section id="contact">
        @include('elements.institucional.rodape')
    </section>

    <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{ asset('assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.fittext.js')}}"></script>
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    
    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/js/creative.js') }}"></script>
    @yield('script')
    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/institucional.js') }}"></script>
</body>

</html>
