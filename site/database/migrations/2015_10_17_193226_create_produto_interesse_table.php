<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoInteresseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto_interesse', function($table)
		{
		    $table->increments('id_produto_interesse');
		    $table->string('produto_interesse', 255)->nullable();
		    $table->text('descricao_produto')->nullable();
			$table->integer('id_produto')->unsigned()->nullable();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto_interesse');
	}

}
