<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoInteresseUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto_interesse_usuario', function($table)
		{
		    $table->increments('id_produto_interesse_usuario');
			$table->integer('id_produto')->unsigned();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');
		    $table->integer('id_log_navegacao')->unsigned()->nullable();
		    $table->foreign('id_log_navegacao')->references('id_log_navegacao')->on('log_navegacao');
		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto_interesse_usuario');
	}

}
