<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoTrocaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto_troca', function($table)
		{
		    $table->increments('id_produto_troca');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		    
		    $table->integer('id_produto')->unsigned();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');

		    $table->integer('id_proposta_produto')->unsigned();
		    $table->foreign('id_proposta_produto')->references('id_proposta_produto')->on('proposta_produto');

		    $table->integer('id_situacao')->unsigned();
		    $table->foreign('id_situacao')->references('id_situacao')->on('situacao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto_troca');
	}

}
