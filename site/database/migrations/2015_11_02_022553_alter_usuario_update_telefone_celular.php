<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsuarioUpdateTelefoneCelular extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuario', function($table)
		{
			$table->string('telefone', 20)->nullable()->change();
			$table->string('celular', 20)->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuario', function($table)
		{
		    $table->dropColumn(['telefone', 'celular']);
		});
	}

}
