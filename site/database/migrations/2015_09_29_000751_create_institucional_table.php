<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('institucional', function($table)
		{
		    $table->increments('id_institucional');
		    $table->string('titulo', 100);
		    $table->text('texto');
		    $table->string('slug');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('institucional');
	}

}
