<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_categoria', function($table)
		{
		    $table->increments('id_sub_categoria');
			$table->integer('id_categoria')->unsigned();
		    $table->foreign('id_categoria')->references('id_categoria')->on('categoria');
		    $table->integer('ativo')->nullable();
		    $table->string('slug', 255);
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_categoria');
	}

}
