<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisualizadaToPropostaProduto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('proposta_produto', function($table)
		{
			$table->integer('visualizada')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proposta_produto', function($table)
		{
		    $table->dropColumn(['visualizada']);
		});
	}

}
