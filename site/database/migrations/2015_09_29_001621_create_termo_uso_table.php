<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermoUsoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('termo_uso', function($table)
		{
		    $table->increments('id_termo_uso');
		    $table->string('titulo', 100);
		    $table->string('subtitulo', 100);
		    $table->text('termo');
		    $table->string('slug');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('termo_uso');
	}

}
