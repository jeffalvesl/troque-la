<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto', function($table)
		{
		    $table->increments('id_produto');

		    $table->integer('id_categoria')->unsigned();
		    $table->foreign('id_categoria')->references('id_categoria')->on('categoria');
		    $table->integer('id_sub_categoria')->unsigned();
		    $table->foreign('id_sub_categoria')->references('id_sub_categoria')->on('sub_categoria');
		    
		    $table->string('produto', 255);
		    $table->text('descricao_produto');
		    $table->date('data_aquisicao');
		    
		    $table->integer('garantia')->nullable()->default(0);
		    $table->string('tempo_garantia', 20)->nullable();
		    $table->integer('garantia_extendida')->nullable()->default(0);
		    $table->string('garantia_extendida_tempo', 20)->nullable();

		    $table->integer('aceita_proposta')->nullable()->default(0);
		    $table->integer('trocado')->nullable()->default(0);
		    $table->integer('publicado')->nullable()->default(0);

		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		    
		    $table->string('slug', 255);
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');

		    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto');
	}

}
