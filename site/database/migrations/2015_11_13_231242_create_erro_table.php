<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('erro', function($table)
		{
		    $table->increments('id_erro');
		    $table->string('pagina', 255);
		    $table->string('print_tela', 255);
		    $table->text('descricao_erro');
		    $table->integer('id_usuario');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('erro');
	}

}
