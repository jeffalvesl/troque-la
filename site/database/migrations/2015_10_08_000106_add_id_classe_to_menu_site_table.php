<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdClasseToMenuSiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu_site', function($table)
		{
			$table->string('id', 50)->nullable();
			$table->string('classe', 50)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('menu_site', function($table)
		{
		    $table->dropColumn(['id', 'classe']);
		});
	}

}
