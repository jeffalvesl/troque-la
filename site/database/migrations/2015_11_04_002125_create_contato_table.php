<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contato', function($table)
		{
		    $table->increments('id_contato');
		    $table->string('nome', 255);
		    $table->string('email', 100);
		    $table->text('mensagem');
		    $table->integer('lido')->nullable()->default(0);
		    $table->integer('respondido')->nullable()->default(0);	
		    $table->integer('id_usuario')->nullable();
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contato');
	}

}
