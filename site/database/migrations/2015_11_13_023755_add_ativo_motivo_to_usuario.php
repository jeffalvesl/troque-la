<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtivoMotivoToUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuario', function($table)
		{
			$table->integer('ativo')->nullable()->default(1);
			$table->text('motivo_cancelamento')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuario', function($table)
		{
		    $table->dropColumn(['ativo', 'motivo_cancelamento']);
		});
	}

}
