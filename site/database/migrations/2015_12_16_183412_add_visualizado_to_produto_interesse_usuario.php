<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisualizadoToProdutoInteresseUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produto_interesse_usuario', function($table)
		{
			$table->integer('visualizado')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produto_interesse_usuario', function($table)
		{
		    $table->dropColumn(['visualizado']);
		});
	}

}
