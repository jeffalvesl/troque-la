<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostaProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposta_produto', function($table)
		{
		    $table->increments('id_proposta_produto');
		    $table->text('proposta');
		    $table->integer('aceita')->nullable()->default(0);
			$table->integer('id_produto')->unsigned();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');
		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposta_produto');
	}

}
