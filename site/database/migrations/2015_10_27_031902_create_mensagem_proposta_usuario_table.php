<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagemPropostaUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mensagem_proposta_usuario', function($table)
		{
		    $table->increments('id_mensagem_proposta_usuario');
			$table->text('mensagem');
		    $table->integer('visualizada')->nullable()->default(0);
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		    
		    $table->integer('id_proposta_produto')->unsigned();
		    $table->foreign('id_proposta_produto')->references('id_proposta_produto')->on('proposta_produto');

		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mensagem_proposta_usuario');
	}

}
