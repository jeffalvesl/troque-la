<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjudaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ajuda', function($table)
		{
		    $table->increments('id_ajuda');
		    $table->string('pergunta', 255);
		    $table->text('resposta');
		    $table->string('slug', 255);
		    $table->integer('ativo')->nullable()->default(0);
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ajuda');
	}

}