<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogNavegacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_navegacao', function($table)
		{
		    $table->increments('id_log_navegacao');
		    $table->string('log_navegacao', 255);
		    $table->string('url_conteudo', 255);
		    $table->string('tipo', 150);
		    $table->string('titulo_pagina', 255);
		    $table->string('url_anterior', 255);
		    $table->string('email', 255);
		    $table->string('usuario', 255);
		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schama::drop('log_navegacao');
	}

}
