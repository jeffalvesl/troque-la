<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntesseUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interesse_usuario', function($table)
		{
		    $table->increments('id_interesse_usuario');
			$table->integer('id_sub_categoria')->unsigned();
		    $table->foreign('id_sub_categoria')->references('id_sub_categoria')->on('sub_categoria');
		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interesse_usuario');
	}

}
