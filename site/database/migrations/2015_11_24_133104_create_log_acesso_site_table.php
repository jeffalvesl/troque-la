<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAcessoSiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_acesso_site', function($table)
		{
			$table->increments('id_log_acesso_site');
		    $table->string('url_referencia', 255);
		    $table->string('token', 255);
		    $table->string('session', 255);
		    $table->integer('cadastrou')->default(0);
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_acesso_site');
	}

}
