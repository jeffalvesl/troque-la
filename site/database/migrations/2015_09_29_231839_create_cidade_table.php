<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cidade', function($table)
		{
		    $table->increments('id_cidade');
		    $table->string('cidade', 45);
		    $table->integer('id_estado')->unsigned();
		    $table->foreign('id_estado')->references('id_estado')->on('estado');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cidade');
	}

}
