<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoPropostaProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produto_proposta_produto', function($table)
		{
		    $table->increments('id_produto_proposta_produto');
			$table->integer('id_proposta_produto')->unsigned();
		    $table->foreign('id_proposta_produto')->references('id_proposta_produto')->on('proposta_produto');
		    $table->integer('id_produto')->unsigned();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produto_proposta_produto');
	}

}
