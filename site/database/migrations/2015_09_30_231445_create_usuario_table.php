<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario', function($table)
		{
		    $table->increments('id_usuario');
		    $table->string('usuario', 45)->nullable();
		    $table->string('nome', 50);
		    $table->string('sobrenome', 100);
		    $table->string('telefone', 12)->nullable();
		    $table->string('celular', 12)->nullable();
		    $table->integer('exibir_telefone_celular')->nullable()->default(0);
		    $table->integer('id_cidade')->unsigned()->nullable();
		    $table->foreign('id_cidade')->references('id_cidade')->on('cidade');
		    $table->string('email', 100);
		    $table->integer('confirmou_email')->nullable()->default(0);
		    $table->integer('primeiro_acesso')->nullable()->default(0);
		    $table->string('senha', 255);
		    $table->integer('id_facebook')->nullable();
		    $table->string('cep', 9)->nullable();
		    $table->string('endereco', 255)->nullable();
		    $table->integer('numero')->nullable();
		    $table->string('bairro', 100)->nullable();
		    $table->string('logado_token', 255)->nullable();
		    $table->text('complemento')->nullable();
		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuario');
	}

}
