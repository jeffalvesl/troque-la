<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecuperarSenhaHashTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recuperar_senha_hash', function($table)
		{
			$table->increments('id_recuperar_senha_hash');
		    $table->string('hash', 255);
		    $table->integer('valida')->default(1);

		    $table->timestamp('criado_em');
		    $table->timestamp('atualizado_em');

		    $table->integer('id_usuario')->unsigned();
		    $table->foreign('id_usuario')->references('id_usuario')->on('usuario');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recuperar_senha_hash');
	}

}
