<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagemProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imagem_produto', function($table)
		{
		    $table->increments('id_imagem_produto');
			$table->integer('id_produto')->unsigned();
		    $table->foreign('id_produto')->references('id_produto')->on('produto');
		    $table->string('imagem', 255);
		    $table->string('legenda', 255)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imagem_produto');
	}

}
