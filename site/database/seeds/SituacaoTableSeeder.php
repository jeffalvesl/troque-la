<?php
use Illuminate\Database\Seeder;
use App\Models\Situacao;
  
class SituacaoTableSeeder extends Seeder
{
    public function run()
    {
        // clear table
        DB::table('situacao')->delete();

        $situacoes = [
            [
                'situacao' => 'Aguardando confirmação',
                'slug' => 'aguardando-confirmacao',
            ],
            [
                'situacao' => 'Troca em andamento',
                'slug' => 'troca-em-andamento',
            ],
            [
                'situacao' => 'Realizada',
                'slug' => 'realizada',
            ],
            [
                'situacao' => 'Cancelada',
                'slug' => 'cancelada',
            ]
        ];

        foreach($situacoes as $situacao)
        {
            Situacao::create($situacao);
        }
    }
}