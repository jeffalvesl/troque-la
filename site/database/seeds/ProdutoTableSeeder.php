<?php
use Illuminate\Database\Seeder;
use App\Models\Produto\Produto;
  
class ProdutoTableSeeder extends Seeder
{
    public function run()
    {
        // clear table
        $produtos = Produto::with(['imagens'])->get();

        $produtosCriar = [];

        foreach($produtos as $produto)
        {
            $produtosCriar[] = [
                'id_categoria' => $produto->id_categoria,
                'id_sub_categoria' => $produto->id_sub_categoria,
                'produto' => $produto->produto,
                'descricao_produto' => $produto->descricao_produto,
                'data_aquisicao' => $produto->data_aquisicao,
                'garantia' => $produto->garantia,
                'tempo_garantia' => $produto->tempo_garantia,
                'garantia_extendida' => $produto->garantia_extendida,
                'aceita_proposta' => $produto->aceita_proposta,
                'trocado' => $produto->trocado,
                'publicado' => $produto->trocado,
                'id_usuario' => $produto->id_usuario
            ];
        }

        DB::table('produto')->delete();

        foreach($produtosCriar as $produto)
        {
            Produto::create($produto);
        }
    }
}