<?php
use Illuminate\Database\Seeder;
use App\Models\Usuario\Usuario;
  
class UsuarioTableSeeder extends Seeder
{
    public function run()
    {
        // clear table
        DB::table('usuario')->delete();
        $senha = \Hash::make('123456789');

        $usuarios = [
            [
                'nome' => 'Rafael',
                'sobrenome' => 'Franke',
                'email' => 'rafaelfrankean@gmail.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Jefferson',
                'sobrenome' => 'Alves',
                'email' => 'jeffersonves@gmail.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'João',
                'sobrenome' => 'Silva',
                'email' => 'joaosilva@com.br',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Ana',
                'sobrenome' => 'Flávia',
                'email' => 'anaflavia@gmail.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Jaqueline',
                'sobrenome' => 'Alves',
                'email' => 'jaqueline@alves.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Emerson',
                'sobrenome' => 'Bueno',
                'email' => 'emerson@bueno.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Pedro',
                'sobrenome' => 'Bueno',
                'email' => 'pedro@bueno.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Ricardo',
                'sobrenome' => 'Bueno',
                'email' => 'ricardo@bueno.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Cristiano',
                'sobrenome' => 'Bueno',
                'email' => 'cristiano@bueno.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ],
            [
                'nome' => 'Ronaldo',
                'sobrenome' => 'Bueno',
                'email' => 'ronaldo@bueno.com',
                'primeiro_acesso' => 1,
                'senha' => $senha
            ]
        ];

        foreach($usuarios as $usuario)
        {
            Usuario::create($usuario);
        }
    }
}