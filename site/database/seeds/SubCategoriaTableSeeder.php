<?php
use Illuminate\Database\Seeder;
use App\Models\Produto\Categoria;
use App\Models\Produto\SubCategoria;
  
class SubCategoriaTableSeeder extends Seeder
{
    public function run()
    {

        $categorias = Categoria::all();
        DB::table('sub_categoria')->delete();
        foreach($categorias as $categoria)
        {
            for($i = 1; $i <= 5; $i++)
            {
                $subCategoria = [
                    'id_categoria' => $categoria->id_categoria,
                    'sub_categoria' => 'Sub Categoria ' . $i,
                    'ativo' => 1,
                    'slug' => 'sub-categoria-' . $i
                ];

                SubCategoria::create($subCategoria);
            }

        }
    }
}