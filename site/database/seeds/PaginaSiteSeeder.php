<?php
use Illuminate\Database\Seeder;
use App\Models\Produto\Categoria;
  
class PaginaSiteSeeder extends Seeder
{
    public function run()
    {
        // clear table
        DB::table('pagina_site')->delete();
        
        $paginas = [
            [
                'pagina' => 'Inicio raiz',
                'rota_laravel' => 'home.index',
                'slug' => 'inicio-raiz'
            ],
            [
                'pagina' => 'Cadastre-se',
                'rota_laravel' => 'cadastro.index',
                'slug' => 'cadastre-se'
            ],
            [
                'pagina' => 'Termos de uso',
                'rota_laravel' => 'termo.index',
                'slug' => 'termos-de-uso'
            ],
            [
                'pagina' => 'Sobre',
                'rota_laravel' => 'sobre.index',
                'slug' => 'sobre'
            ],
            [
                'pagina' => 'Privacidade',
                'rota_laravel' => 'privacidade.index',
                'slug' => 'privacidade'
            ],
            [
                'pagina' => 'Produtos',
                'rota_laravel' => 'produtos.index',
                'slug' => 'produtos'
            ]

        ];

        foreach($paginas as $pagina)
        {
            Categoria::create($pagina);
        }
    }
}