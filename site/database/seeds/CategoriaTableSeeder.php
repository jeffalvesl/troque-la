<?php
use Illuminate\Database\Seeder;
use App\Models\Produto\Categoria;
  
class CategoriaTableSeeder extends Seeder
{
    public function run()
    {
        // clear table
        DB::table('categoria')->delete();
        
        $categorias = [
            [
                'categoria' => 'Eletrônicos',
                'ativo' => 1,
                'slug' => 'eletronicos'
            ],
            
            [
                'categoria' => 'Consoles',
                'ativo' => 1,
                'slug' => 'consoles'
            ],

            [
                'categoria' => 'Música',
                'ativo' => 1,
                'slug' => 'musica'
            ],

            [
                'categoria' => 'DVD e Blu-ray',
                'ativo' => 1,
                'slug' => 'dvd-e-blu-ray'
            ],

            [
                'categoria' => 'Informática',
                'ativo' => 1,
                'slug' => 'informatica'
            ],

            [
                'categoria' => 'Celulares',
                'ativo' => 1,
                'slug' => 'celulares'
            ],

            [
                'categoria' => 'Roupas e calçados',
                'ativo' => 1,
                'slug' => 'roupas-e-calçados'
            ],

            [
                'categoria' => 'Informática',
                'ativo' => 1,
                'slug' => 'informatica'
            ],

            [
                'categoria' => 'Livraria',
                'ativo' => 1,
                'slug' => 'livraria'
            ],

            [
                'categoria' => 'Esportes',
                'ativo' => 1,
                'slug' => 'esportes'
            ],

            [
                'categoria' => 'Fotografia',
                'ativo' => 1,
                'slug' => 'fotografia'
            ]
        ];

        foreach($categorias as $categoria)
        {
            Categoria::create($categoria);
        }
    }
}