<?php
use Illuminate\Database\Seeder;
use App\Models\Produto\ImagemProduto;
  
class ImagemTableSeeder extends Seeder
{
    public function run()
    {
        // clear table
        $imagens = ImagemProduto::orderBy('id_imagem_produto', 'ASC')->get();

        $imagensCriar = [];

        foreach($imagens as $imagem)
        {
            $imagensCriar[] = [
                'id_produto' => $imagem->id_produto,
                'imagem' => $imagem->imagem
            ];
        }

        DB::table('imagem_produto')->delete();


        foreach($imagensCriar as $imagem)
        {
            ImagemProduto::create($imagem);
        }
    }
}