$(document).ready(function($) {

	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formErro").validate({
        errorPlacement : function(error, element) {
             formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                $(element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            pagina : { required : true, minlength: 15},
            print_tela : { required: true},
            descricao_erro : { required: true, minlength: 10},
        },
        messages : {
            pagina : { required : 'Informe a url da página', minlength:'O link deve conter 15 caracteres' },
            print_tela : { required : 'Print da tela é requirido'},
            descricao_erro : { required : 'Como foi que chegou a este erro.', minlength: 'A descrição deste erro deve ter no mínimo 10 caracteres' },
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            form.submit();
        }
    });

});