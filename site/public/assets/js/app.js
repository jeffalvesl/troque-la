$(document).ready(function($) {

    $(".compartilhar").click(function(e){
        e.preventDefault();
        var url = $(this).data('href');
        window.open (url, "mywindow","width=700, height=500");
    });

    $('.cidades').select2({
        allowClear: false
	});


    /**
     * Verifica notificações.
     */

    var mensagem = 'vazio';
	setInterval(function(){
		var naoLogado = $('#entrar');

		if(naoLogado.length <= 0)
		{
		    var token = $('meta[name=csrf-token]').attr('content');
		    $.ajax({
		        url: $('.mensagens').data('url'),
		        data: {_token: token},
		        type: 'POST',
		        dataType: 'json',
		        success: function(data) {
		            if(data.notificacoes.total > 0)
		            {
		            	$('.mensagens span').text(data.notificacoes.total);
		            	$('title').text('(Você tem uma notificação. ' + data.notificacoes.total + ')');

		            	$.each(data.notificacoes, function(i, notificacao) {
		            		if(notificacao.notificacao_usuario !== undefined)
		            		{
		            			$('#mensagens .listNotific span').addClass('none');

		            			var idNotificacao = $('#'+notificacao.id_notificacao_usuario).length;
		            			if(idNotificacao == 0)
		            			{
				            		var html = "<a id='"+ notificacao.id_notificacao_usuario +"' href="+ notificacao.url_conteudo +" class='list-group-item'>" + notificacao.notificacao_usuario + " <strong><small>" + notificacao.data_hora + "</small></strong></a>";
				            		$('#mensagens .listNotific').append(html);
		            			}

		            		}
		            	});

		            	return true;
		            }

		            $('#mensagens .listNotific span').text('Sem notificações');
		        },
		        error: function(){
		        	location.reload();
		        }
		    });
		}
    }, 5000);



});