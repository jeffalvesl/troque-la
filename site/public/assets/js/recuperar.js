$(document).ready(function($) {

     /**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formRecuperarSenha").validate({
        errorPlacement : function(error, element) {
            formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                (element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            email : {required: true, email: true},
        },
        messages : {
            email : { required : '', email: ''},
        },

        submitHandler: function(form){
            event.preventDefault();
            var action = $(form).attr('action');

            $('.btRecuperar').prop('disabled', true); 
            $('.btRecuperar').val('Aguarde enquanto validamos o email informado...');

            var dados = $(form).serializeArray();

            $.ajax({
                url: action,
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    $('.btRecuperar').val(data.mensagem);
                },
                error: function(){
                    $('.btRecuperar').val('Infelizmente não foi possível enviar um email para recuperação. Atualize a página e tente novamente');
                }
            });

        }

        //faz ajax para salvar novo usuário.
        // submitHandler: function(form){

        //     }
        // }
    });


    /**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formCadastrarSenha").validate({
        errorPlacement : function(error, element) {
            formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                (element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            email : {required: true, email: true},
            senha: {required: true, minlength: 8},
            confirmacao_senha: {
                required: true,
                equalTo: "#senha"
            },
        },
        messages : {
            email : { required : 'Informe seu email', email: 'Informe corretamente seu email'},
            senha : { required : 'Informe sua senha', minlength: 'Ela deve ter mais que 8 caracteres.' },
            confirmacao_senha: {equalTo: 'Senha não confere', required: 'Confirme sua senha'},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            form.submit();
        }
    });

});