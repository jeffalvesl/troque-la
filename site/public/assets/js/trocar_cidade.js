$(document).ready(function($) {

	var $cidadeTrocar = $('.cidades');

	$cidadeTrocar.on("select2:select", function(e){

        if(!e.params.data.id)
        {
            return false;
        }

		var dados = {'id_cidade': e.params.data.id, '_token': $('meta[name=csrf-token]').attr('content')};
		var action = $('.cidades').data('action');

		$.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.setou)
                {
                    location.reload();
                }
            },
            error: function(){
                console.log('erro');
            }
        });
	});


});