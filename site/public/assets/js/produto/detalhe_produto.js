$(document).ready(function($) {


    if($(".fancybox").length > 0)
    {
        $(".fancybox").fancybox();
    }


	$('.produto_proposta_produto').select2({
		placeholder: 'Informe o produto que deseja oferecer em troca',
        allowClear: false
	});

	/*
		Efeito botão lançar proposta
	*/
	$('.btLancarProposta').click(function(e){

		if($('.proposta').hasClass('none'))
		{
			e.preventDefault();
			$(this).addClass('enviarProposta');
			$(this).text('Enviar Proposta');
			$('.proposta').removeClass('none');
			$('.naoEnviar').removeClass('none');
			return true;
		}
	});

	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formProposta").validate({
        errorPlacement : function(error, element) {
            formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                $(element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            proposta : { required : true, minlength: 10},
            produto_proposta_produto : { required : true},
        },
        messages : {
            proposta : { required : 'Informe sua proposta', minlength:'Elabore melhor sua proposta' },
            produto_proposta_produto : { required : 'Informe o produto que deseja oferecer'},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){

            var action = $(form).attr('action');

            $('.btLancarProposta').prop('disabled', true);
            $('.btLancarProposta').text('Enviando proposta...');

            var dados = $(form).serializeArray();

            $.ajax({
                url: action,
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if(data.salvou)
                    {
                        $('.btLancarProposta').text('Proposta Enviada');

                        setTimeout(function(){
                            location.reload();
                          },
                          3000
                        );
                    }
                },
                error: function(){
                    $('.btLancarProposta').prop('disabled', false);
                    $('.btLancarProposta').text('Proposta Enviada! Aguarde até receber uma resposta do dono deste produto.');
                    setTimeout(function(){
                            $('.proposta').addClass('none');
                          },
                      3000
                    );
                }
            });
        }
	});

	$('.naoEnviar').click(function(e){
		e.preventDefault();

		$(this).removeClass('enviarProposta');
		$('.proposta').addClass('none');
		$('.naoEnviar').addClass('none');
		return true;
	});




});