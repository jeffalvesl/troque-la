$(document).ready(function($) {


	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formContato").validate({
        errorPlacement : function(error, element) {
             formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                $(element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            nome : { required : true, minlength: 3},
            email : {
                required: true, email: true
            },
            mensagem : { required: true, minlength: 8},
        },
        messages : {
            nome : { required : 'Informe seu nome', minlength:'Nome deve ter no mínimo 3 caracteres' },
            email : { required : 'Informe seu email', email: 'Informe corretamente seu email' },
            mensagem : { required : 'Digite sua mensagem', minlength : 'Suagem mensagem deve ter no mínimo 8 caracteres' },
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            event.preventDefault();

            var action = $(form).attr('action');
            var dados = $(form).serialize();
            $('.btEnviarMsg').prop('disabled', true);
            $('.btEnviarMsg').text('Aguarde...');


            $.ajax({
                url: action,
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if(data.enviou)
                    {
                        $('.btEnviarMsg').text('Mensagem enviada com sucesso');
                        setTimeout(function(){
                                $('.btEnviarMsg').text('Enviar Mensagem');
                                $('.btEnviarMsg').prop('disabled', false);
                            },
                            3000
                        );
                    }
                },
                error: function(){
                    $('.btEnviarMsg').text('Erro ao enviar mensagem');
                    $('.btEnviarMsg').prop('disabled', false);
                }
            });

            form.reset();
        }
    });

});