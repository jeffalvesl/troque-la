$(document).ready(function($) {

    var action = $('input[name=password]').data('url');
     /**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formLogin").validate({
        errorPlacement : function(error, element) {
            formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                (element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            email : {required: true, email: true,},
            password: {
                required: true,
                minlength: 8,
                remote: {
                    url: action,
                    type: "POST",
                    data: {
                        email: function(){
                            return $('input[name=email]').val();
                        },
                        password: function(){
                            return  $('input[name=password]').val();
                        },
                        '_token': $('input[name=_token]').val()
                    }
                },
            },
        },
        messages : {
            email : { required : '', email: ''},
            password : { required : '', minlength: '', remote: "Dados não conferem"},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            form.submit();
        }
    });

});