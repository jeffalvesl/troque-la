$(document).ready(function($) {


    var action = $('input[name=email]').data('route');
    var email = $('input[name=email]').val();
	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formCadastro").validate({
        errorPlacement : function(error, element) {
             formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");

                (element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).parent().find('label').html('');
                    }
                });
            }

            return false;
        },

        rules : {
            nome : { required : true, minlength: 3},
            sobrenome : { required : true, minlength: 3},
            email : {
                required: true,
                email: true,
                remote: {
                    url: action,
                    type: "POST",
                    data: {
                        email: function(){
                        return $('input[name=email]').val();
                    },
                        '_token': $('input[name=_token]').val()
                    }
                },
            },
            password: {required: true, minlength: 8},
            confirmacao_senha: {
                required: true,
                equalTo: "#senha"
            },
        },
        messages : {
            nome : { required : 'Informe seu nome', minlength:'Nome deve ter no mínimo 3 caracteres' },
            sobrenome : { required : 'Informe seu sobrenome', minlength : 'Seu sobrenome deve ter no mínimo 3 caracteres' },
            email : { required : 'Informe seu email', email: 'Informe corretamente seu email', remote: "Email já está sendo usado."},
           	password : { required : 'Informe sua senha', minlength: 'Ela deve ter mais que 8 caracteres.' },
           	confirmacao_senha: {equalTo: 'Senha não confere', required: ''},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            form.submit();
        }
    });

});