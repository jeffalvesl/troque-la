$(document).ready(function($) {

    $('#data_nascimento').mask('00/00/0000');
    $('#telefone').mask('(99) 9999-99999');
    $('#celular').mask('(99) 9999-99999');

    $('#data_nascimento').keyup(function(event) {
        $(this).val('');
    });

    $('#data_nascimento').datepicker({
        language: 'pt-BR',
        startDate: '-100y',
        endDate: '+0d'
    });


    $('#cep').mask('00000-000');
    $('.sexo').select2();

    $('.interesses').select2({
        placeholder: 'Informe seus interesses',
        allowClear: false,
        minimumInputLength: 3,

        ajax: {
            url: $('.interesses').data('url'),
            dataType: 'json',
            delay: 250,
            tags: true, 
            method: 'POST',
            data: function (params) {
                console.log(params);
                return {
                    termo: params.term, // search term,
                    _token: $('input[name=_token]').val()
                };
            },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data
                };
            },
            cache: true
        }
    });



	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formUsuario").validate({
        errorPlacement : function(error, element) {
             formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>"+error.html()+"</label>");
            }

            return false;
        },

        rules : {
            nome : { required : true, minlength: 3},
            sobrenome : { required : true, minlength: 3},
            email : {
                required: true, email: true
            },
            cep : { required: true, minlength: 8},
            sexo : { required: true},
            data_nascimento : {required: true},
        },
        messages : {
            nome : { required : 'Informe seu nome', minlength:'Nome deve ter no mínimo 3 caracteres' },
            sobrenome : { required : 'Informe seu sobrenome', minlength : 'Seu sobrenome deve ter no mínimo 3 caracteres' },
            email : { required : 'Informe seu email', email: 'Informe corretamente seu email' },
            cep : { required : 'Informe seu cep', minlength: "" },
            sexo : { required : 'Informe seu sexo'},
            data_nascimento : { required : 'Informe sua data de nascimento'},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){

            event.preventDefault();

            var action = $(form).attr('action');
            var dados = $(form).serialize();
            $('.btSalvar').prop('disabled', true);
            $('.btSalvar').text('Aguarde...');


            $.ajax({
                url: action,
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.atualizou)
                    {
                        $('.btSalvar').text('Salvar dados');
                        $('.btSalvar').prop('disabled', false);
                        $('.mensagem').addClass(data.classe);
                        $('.mensagem').removeClass('none');

                        setTimeout(function(){
                            $('.mensagem').addClass('none');
                            },
                            3000
                        );

                        if(data.redirecionar != false)
                        {
                            window.location = data.redirecionar;
                        }
                    }
                },
                error: function(){
                    $('.btSalvar').text('Salvar dados');
                    alert('Infelizmente não foi possível realizar a ação, tente novamente mais tarde.')
                }
            });

        }
    });

});