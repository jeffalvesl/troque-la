
/**
 * Arquivo responsavel pelas mensagens enviadas em cada proposta do produto.
 * Essa classe não trata as propostas e sim as mensagens relacionadas a cada proposta.
 */
$(document).ready(function($) {

    var id = '';
    $('.btAceitarProposta').click(function(e){

        e.preventDefault();

        var elemento = $(this).text('Aguarde...');

        var action = $(this).data('action');
        var proposta = $(this).data('proposta');
        var dados = {
            id_produto: $('input[name=id_produto]').val(),
            id_proposta_produto: proposta,
            _token: $('meta[name=csrf-token]').attr('content')
        };

        $.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.aceita)
                {
                    location.reload();
                }
            },
            error: function(){
                alert('Não foi possivel realizar a ação. Por favor, tente novamente');
            }
        });

        console.log(dados);
    });

    $('.btConfirmarTroca').click(function(e){
        e.preventDefault();

        var action = $(this).data('action');
        var elemento = $(this);

        elemento.text('Aguarde...');

        var dados = {
            id_produto_troca: $('input[name=id_produto_troca]').val(),
            _token: $('meta[name=csrf-token]').attr('content')
        };


        $.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.confirmou)
                {
                    location.reload();
                }
            },
            error: function(){
                alert('Não foi possivel realizar a ação. Por favor, tente novamente');
            }
        });
    });

    $('.btTrocaRealizada').click(function(e){
        e.preventDefault();

        var action = $(this).data('action');
        var elemento = $(this);

        elemento.text('Aguarde...');

        var dados = {
            id_produto_troca: $('input[name=id_produto_troca]').val(),
            _token: $('meta[name=csrf-token]').attr('content')
        };

        $.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.trocado)
                {
                    location.reload();
                }
            },
            error: function(){
                alert('Não foi possivel realizar a ação. Por favor, tente novamente');
                //location.reload();
            }
        });
    });


    $('.btCancelarTroca').click(function(e) {
        e.preventDefault();

        var action = $(this).data('action');
        console.log(action);
        var elemento = $(this);

        elemento.text('Aguarde...');

        var dados = {
            id_produto_troca: $('input[name=id_produto_troca]').val(),
            _token: $('meta[name=csrf-token]').attr('content')
        };

        $.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.cancelada)
                {
                    location.reload();
                }
            },
            error: function(){
                alert('Não foi possivel realizar a ação. Por favor, tente novamente');
            }
        });
    });
});