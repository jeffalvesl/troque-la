$(document).ready(function($) {

    $('#data_aquisicao').mask('00/00/0000');

    $('#idSubCategoria').select2();

    $('#data_aquisicao').keyup(function(event) {
        $(this).val('');
    });

    $('#data_aquisicao').datepicker({
        language: 'pt-BR',
        startDate: '-150y',
        endDate: '+0d'
    });


	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formProduto").validate({
        errorPlacement : function(error, element) {
             formClass = $(this);
            var label = $(element).parent().find('label');
            if(!$(element).parent().hasClass('error') || error.html())
            {
                $(element).addClass('error');

                $(element).focusout(function(){
                    if($(this).val() != '')
                    {
                        $(this).removeClass('error');
                    }
                });
            }

            return false;
        },

        rules : {
            produto : { required : true, minlength: 5},
            id_sub_categoria : { required : true},
            descricao_produto : { required: true, minlength: 10},
            data_aquisicao : { required: true},
            produto_desejo : {required: true},
            tempo_garantia: {
                required: function(element){
                    if($("#garantiaCheckBox").is(':checked'))
                    {
                        return true;
                    }
                    return false;
                }
            },
        },
        messages : {
            produto : { required : 'Informe o nome do seu produto', minlength:'Nome deve ter no mínimo 5 caracteres' },
            id_sub_categoria : { required : 'Informe uma sessão para o produto'},
            descricao_produto : { required : 'Informe a descrição para seu produto', email: 'A descrição deve ter no mínimo 10 caracteres' },
            data_aquisicao : { required : 'Informe a data de aquisição', minlength: "" },
            produto_desejo : { required : 'Informe o produto que deseja em troca'},
            tempo_garantia : { required : 'Por favor informe a garantia do produto'},
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){

            var action = $(form).attr('action');

            $('.btPublicarProduto').prop('disabled', true);
            $('.btPublicarProduto').text('Aguarde...');

            var dados = $(form).serializeArray();

            $.ajax({
                url: action,
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if(data.url != false)
                    {
                        window.location = data.url;
                    }
                },
                error: function(){
                    $('.btPublicarProduto').prop('disabled', false);
                    $('.btPublicarProduto').text('Públicar produto');
                    alert('Infelizmente não foi possível realizar a ação, tente novamente mais tarde.')
                }
            });
        }
    });

    $('#garantiaCheckBox').click(function(){
        if($(this).is(':checked'))
        {
            $(this).val('1');
            $('input[name=tempo_garantia]').prop('disabled', false);
        }
        else
        {
            $(this).val('0');
            $('input[name=tempo_garantia]').prop('disabled', true);
            $('input[name=tempo_garantia]').removeClass('error');
            $('input[name=tempo_garantia]').parent().find('.label').html('');
            $('input[name=tempo_garantia]').val('');
        }
    });


    $('.produtos_interesse').select2({
        tags: true,
        allowClear: false,
        minimumInputLength: 3,

        ajax: {
            url: $('.produtos_interesse').data('route'),
            dataType: 'json',
            delay: 250,
            method: 'GET',
            data: function (params) {
                console.log(params);
                return {
                    termo: params.term, // search term,
                    _token: $('input[name=_token]').val()
                };
            },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data.interesses
                };
            },
            cache: false
        }
    });
});