
/**
 * Arquivo responsavel pelas mensagens enviadas em cada proposta do produto.
 * Essa classe não trata as propostas e sim as mensagens relacionadas a cada proposta.
 */
$(document).ready(function($) {

    setInterval(function(){
        var token = $('meta[name=csrf-token]').attr('content');
        var dados = {'_token': token, 'id_proposta_produto': $('input[name=id_proposta_produto]').val()};

        if($('input[name=id_proposta_produto]').data('url'))
        {
            $.ajax({
                url
                : $('input[name=id_proposta_produto]').data('url'),
                data: dados,
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if(data.temMensagem)
                    {
                        location.reload();
                    }
                },
                error: function(){
                    alert('Por favor, faça login novamente');
                    location.reload();
                }
            });
        }

    }, 3000);



    var id = '';
    var elementModal = '';
    $('.btResponder').click(function(e){
        id = $(this).data('mensagem');
        elementModal = $(this).data('target');
        console.log(elementModal);
    });

    $('.btEnviarMensagem').click(function(e){
        e.preventDefault();

        if(id)
        {
            var element = $('#' + id);
        }
        else
        {
            var element = $('textarea[name=mensagem]');
        }

        if($(element).val() == '')
        {
            var label = $(element).parent().find('label');
            $(element).addClass('error');
            if(!$(element).parent().hasClass('error'))
            {
                $(element).addClass('error');
                $(element).parent().find('.label').html("<label class='error'>Escreva sua mensagem</label>");
            }
            return false;
        }
        else
        {
            $(element).parent().find('.label').html("<label class='error'></label>");
        }

        var botao = $(this);
        botao.text('Enviando Mensagem...');


        var dados = {
            '_token': $("input[name=_token]").val(),
            'id_produto': $('input[name=id_produto]').val(),
            'id_proposta_produto': $('input[name=id_proposta_produto]').val(),
            'mensagem': $(element).val()
        };

        var action = $('textarea[name=mensagem]').data('action');

        $.ajax({
            url: action,
            data: dados,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if(data.enviou)
                {
                    $('textarea[name=mensagem]').val('');
                    botao.text('Mensagem Enviada!');

                    setTimeout(function(){
                        botao.text('Enviar Mensagem');
                      },
                      3000
                    );

                     $(elementModal).modal('hide');
                     location.reload();
                }
            },
            error: function(){
                $('.btPublicarProduto').prop('disabled', false);
                $('.btPublicarProduto').text('Públicar produto');
                alert('Infelizmente não foi possível realizar a ação, tente novamente mais tarde.')
            }
        });
    });
});