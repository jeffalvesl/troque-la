$(document).ready(function($) {

    $('.imagens_produto').change(function(){
        var arquivos = $('input[name=imagens_produto')[0].files;

        if(arquivos.length > 4)
        {
            alert('Apenas 4 imagens são permitidas.');
            return false;
        }
        else
        {
            $('.message').removeClass('none');

            var action = $(this).data('route');
            var dados = new FormData();
            dados.append('arquivos', arquivos);
            dados.append('id_produto', $('input[name=id_produto]').val());
            dados.append('_token', $('input[name=_token]').val());

            $(arquivos).each(function(index, element) {
                dados.append('arquivo', element);
                $.ajax({
                    url: action,
                    data: dados,
                    type: 'POST',
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if(data.arquivo_salvou)
                        {
                            $('.message').addClass('none');
                            var html = "<div class=troca col-sm-12 col-md-3 col-lg-3><a href=javascript:void(0)><img src="+ data.arquivo +" width=200 class=img-rounded></a></div>";
                            $('.trocas').append(html);

                            location.reload();
                        }
                    },
                    error: function(){
                        alert('Infelizmente não foi possível realizar a ação, tente novamente mais tarde.');
                        return false;
                    }
                });
            });
        }
    });

    $(".excluirImagem").click(function(e){
        e.preventDefault();

        $(this).text('Aguarde..');

        var action = $(this).attr('href');
        $.ajax({
            url: action,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                if(data.excluiu)
                {
                    location.reload();
                }
            },
            error: function(){
                alert('Infelizmente não foi possível realizar a ação, tente novamente mais tarde.');
                $(this).text('Excluir');
                return false;
            }
        });
    });
});