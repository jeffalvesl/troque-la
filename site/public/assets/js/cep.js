$(document).ready(function() {

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#endereco").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {

                        var token = $('input[name=_token]').val();
                        var url = $('input[name=cep]').data('url');
                        var infoLocalidade = {
                            'sigla': dados.uf,
                            'cidade': dados.localidade,
                            '_token': token
                        };


                        $.ajax({
                            url: url,
                            data: infoLocalidade,
                            type: 'POST',
                            dataType: 'json',
                            success: function(retorno) {
                                $('input[name=id_cidade]').val(retorno.id_cidade);
                            },
                            error: function(){

                            }
                        });

                        $("input#cidade").val(dados.localidade);

                        //Atualiza os campos com os valores da consulta.
                        if(dados.logradouro !== '')
                        {
                            $("#endereco").val(dados.logradouro);
                        }
                        else
                        {
                            $("#endereco").val('');
                            $('#endereco').prop('readonly', false);
                        }

                        if(dados.bairro !== '')
                        {
                            $("#bairro").val(dados.bairro);
                        }
                        else
                        {
                            $("#bairro").val('');
                            $("#bairro").prop('readonly', false);
                        }


                        $("#uf").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});