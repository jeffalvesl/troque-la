$(document).ready(function($) {

    /**
     * Arquivo responsável pelos efeitos no menu.
     */
    
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // $('.navbar-collapse ul li a').click(function() {
    //     $('.navbar-toggle:visible').click();
    // });


    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })


    // $(".dropdown").hover(            
    //     function() {
    //         $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
    //         $(this).toggleClass('open');        
    //     },
    //     function() {
    //         $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
    //         $(this).toggleClass('open');       
    //     }
    // );

    $(".categorias").click(function(e){
        $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
    });

});