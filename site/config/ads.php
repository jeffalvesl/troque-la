<?php

return [
    'client_id' => 'ca-pub-6715274494393078', //Your Adsense client ID e.g. ca-pub-9508939161510421
    'ads' => [
        'responsive' => [
            'ad_unit_id' => 3373804947,
            'ad_format' => 'auto'
        ],
        'rectangle' => [
            'ad_unit_id' => 3373804947,
            'ad_style' => 'display:inline-block;width:728px;height:100px',
            'ad_format' => 'auto'
        ]
    ]
];

