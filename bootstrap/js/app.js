$(document).ready(function($) {

    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });
    
    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });
    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });

	/**
     * Form validator to #formSignUp
     * @param  {[type]} error   [description]
     * @param  {[type]} element [description]
     * @return {[type]}         [description]
    */
    var validator = $(".formCadastro").validate({
        errorPlacement : function(error, element) {
            // return false;
        },

        rules : {
            nome : { required : true, minlength: 3},
            sobrenome : { required : true, minlength: 3},
            email : {
                required: true, email: true
            },
            password: {required: true, minlength: 8},
            confirmacao_senha: {
                required: true,
                equalTo: "#senha"
            },
        },
        messages : {
            nome : { required : '', minlength:'' },
            sobrenome : { required : '', email : '' },
            email : { required : '', minlength: '' },
            password : { required : '' }
        },

        //faz ajax para salvar novo usuário.
        submitHandler: function(form){
            form.submit();
        }
    });

    

});