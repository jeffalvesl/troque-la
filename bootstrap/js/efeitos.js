$(document).ready(function($) {

	/*
		Efeito do menu de login na pagina incial do site.
	*/
	$('.loginMenu a').click(function(e){
		e.preventDefault();

		if($(".loginMenu").attr('id') == '')
		{
			$(".loginMenu").attr('id', 'loginCurrent');

			$('#loginCurrent .login').css('display', 'block');
		}
		else
		{
			$('#loginCurrent .login').css('display', 'none');	
			$(".loginMenu").attr('id', '');
		}
	});


	/*
		Efeito botão lançar proposta
	*/
	$('.btLancarProposta').click(function(e){
		e.preventDefault();

		if($('.proposta').hasClass('none'))
		{
			$(this).addClass('enviarProposta');
			$(this).text('Enviar Proposta');
			$('.proposta').removeClass('none');
			$('.naoEnviar').removeClass('none');
			return true;
		}
		else if($(this).hasClass('enviarProposta'))
		{
			alert('aqui');
		}
	});

	$('.naoEnviar').click(function(e){
		e.preventDefault();

		$(this).removeClass('enviarProposta');
		$(this).text('Lançar Proposta');
		$('.proposta').addClass('none');
		$('.naoEnviar').addClass('none');
		return true;
	});

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Fit Text Plugin for Main Header
    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Initialize WOW.js Scrolling Animations
    new WOW().init();



});